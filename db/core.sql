/*
SQLyog Ultimate - MySQL GUI v8.21 
MySQL - 5.6.12-log : Database - core
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`core` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `core`;

/*Table structure for table `annual_budget` */

DROP TABLE IF EXISTS `annual_budget`;

CREATE TABLE `annual_budget` (
  `budget_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL COMMENT 'FK for department',
  `year` int(4) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`budget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `annual_budget` */

insert  into `annual_budget`(`budget_id`,`department_id`,`year`,`amount`) values (1,1,2014,0),(2,2,2014,0),(3,3,2014,0),(4,4,2014,0),(5,5,2014,10000000),(6,6,2014,0);

/*Table structure for table `approval` */

DROP TABLE IF EXISTS `approval`;

CREATE TABLE `approval` (
  `approval_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_number` int(11) NOT NULL COMMENT 'FK for form_request',
  `approved_by` int(11) NOT NULL COMMENT 'FK for user',
  `approved_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`approval_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `approval` */

insert  into `approval`(`approval_id`,`request_number`,`approved_by`,`approved_date`) values (34,19,5,'2014-12-07 07:37:46'),(37,19,2,'2014-12-07 13:35:16'),(46,19,6,'2014-12-07 18:36:57');

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('8107c8032d0f987799423510970ab76f','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',1428236520,'a:3:{s:9:\"user_data\";s:0:\"\";s:4:\"user\";O:8:\"stdClass\":11:{s:7:\"user_id\";s:1:\"1\";s:13:\"user_group_id\";s:1:\"1\";s:10:\"user_email\";s:17:\"if07087@gmail.com\";s:13:\"user_password\";s:40:\"fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe\";s:13:\"user_fullname\";s:20:\"Harry Osmar Sitohang\";s:11:\"user_active\";s:3:\"YES\";s:15:\"user_group_type\";s:9:\"DEVELOPER\";s:15:\"user_group_desc\";s:19:\"Granted all access.\";s:13:\"department_id\";s:1:\"1\";s:15:\"department_name\";s:2:\"IT\";s:15:\"department_desc\";s:32:\"IT : GROUP superadmin, developer\";}s:12:\"sidebar_menu\";s:2131:\"<li><a href=\"javascript:;\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> <span class=\"title\">Access Management</span><span class=\"arrow\"></span></a><ul class=\"sub-menu\"><li><a href=\"http://core.net/backend/menu\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> Menu</a></li><li><a href=\"http://core.net/backend/user_group\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> User Group</a></li><li><a href=\"http://core.net/backend/user\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> User</a></li><li><a href=\"http://core.net/backend/privilege\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> Privilege</a></li></ul><a href=\"http://core.net/backend/dashboard\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> <span class=\"title\">Dashboard</span></a><a href=\"javascript:;\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> <span class=\"title\">Department</span><span class=\"arrow\"></span></a><ul class=\"sub-menu\"><li><a href=\"http://core.net/backend/department\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> List</a></li><li><a href=\"http://core.net/backend/annual_budget\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> Annual Budget</a></li></ul><a href=\"javascript:;\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> <span class=\"title\">COA</span><span class=\"arrow\"></span></a><ul class=\"sub-menu\"><li><a href=\"http://core.net/backend/coa_type\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> Type</a></li></ul><a href=\"javascript:;\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> <span class=\"title\">Request Form</span><span class=\"arrow\"></span></a><ul class=\"sub-menu\"><li><a href=\"http://core.net/backend/request_form\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> My Department</a></li><li><a href=\"http://core.net/backend/request_form_all\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> My Task</a></li></ul><a href=\"http://core.net/backend/supplier\" class=\"sidebar-menu-link\"><i class=\"fa fa-folder-open\"></i> <span class=\"title\">Supplier</span></a></li>\";}');

/*Table structure for table `coa_type` */

DROP TABLE IF EXISTS `coa_type`;

CREATE TABLE `coa_type` (
  `coa_code_number` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) NOT NULL,
  `active` enum('YES','NO') NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`coa_code_number`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `coa_type` */

insert  into `coa_type`(`coa_code_number`,`type_name`,`active`) values (1,'CAPEX','YES'),(2,'Liabilities','NO'),(3,'Capital','NO'),(4,'Sales','NO'),(5,'Expense','YES');

/*Table structure for table `delivery_order` */

DROP TABLE IF EXISTS `delivery_order`;

CREATE TABLE `delivery_order` (
  `do_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_order_number` int(11) NOT NULL COMMENT 'FK for request_order',
  `do_number` varchar(10) NOT NULL,
  `do_file` varchar(500) NOT NULL,
  `received_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `received_by` int(11) NOT NULL COMMENT 'FK for user',
  `delivery_score` float NOT NULL,
  `review_comment` text,
  PRIMARY KEY (`do_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `delivery_order` */

insert  into `delivery_order`(`do_id`,`request_order_number`,`do_number`,`do_file`,`received_date`,`received_by`,`delivery_score`,`review_comment`) values (12,82,'00701','092b73fde2850d15fa4c679ee5e728cd.pdf','2014-12-08 02:33:14',2,1,'asdasdasdasdasd');

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(250) NOT NULL,
  `department_desc` text NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `department` */

insert  into `department`(`department_id`,`department_name`,`department_desc`) values (1,'IT','IT : GROUP superadmin, developer'),(2,'ADMIN','Admin'),(3,'ACCOUNTING','Accounting'),(4,'FINANCE','Finance'),(5,'OPERATIONAL','Operational'),(6,'DIRECTOR','Director');

/*Table structure for table `form_request` */

DROP TABLE IF EXISTS `form_request`;

CREATE TABLE `form_request` (
  `request_number` int(11) NOT NULL AUTO_INCREMENT,
  `request_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `request_by` int(11) NOT NULL COMMENT 'FK user',
  `proceed` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `proceed_date` datetime NOT NULL,
  `proceed_by` int(11) NOT NULL,
  PRIMARY KEY (`request_number`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `form_request` */

insert  into `form_request`(`request_number`,`request_date`,`request_by`,`proceed`,`proceed_date`,`proceed_by`) values (16,'2014-12-07 07:16:30',8,'YES','2014-12-07 00:18:43',8),(19,'2014-12-07 07:34:06',5,'YES','2014-12-07 00:34:19',5);

/*Table structure for table `invoice` */

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_number` int(11) NOT NULL COMMENT 'FK for quotation',
  `invoice_number` varchar(10) NOT NULL,
  `downpayment_step` int(2) NOT NULL DEFAULT '1' COMMENT 'IF ''DOWN_PAYMENT'' => field ''payment_type'' in ''quotation'' table : 1,2,3,...',
  `downpayment_percentage` float NOT NULL DEFAULT '1' COMMENT 'Percentage form total bill, default is 1 : 100%',
  `bill_amount` double NOT NULL,
  `invoice_due_date` date NOT NULL,
  `invoice_file` varchar(150) NOT NULL,
  `received_date` date NOT NULL,
  `received_by` int(11) NOT NULL COMMENT 'FK for user',
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invoice` */

/*Table structure for table `material_request` */

DROP TABLE IF EXISTS `material_request`;

CREATE TABLE `material_request` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_number` int(11) NOT NULL COMMENT 'FK for user',
  `item_description` varchar(500) NOT NULL,
  `item_qty` int(11) NOT NULL DEFAULT '0',
  `item_price` double NOT NULL,
  `item_type` enum('MATERIAL','SERVICE') NOT NULL DEFAULT 'MATERIAL',
  `coa_type_id` int(11) NOT NULL COMMENT 'FK for coa_type',
  `coa_number` varchar(10) NOT NULL DEFAULT '-',
  `asset_number` varchar(10) NOT NULL DEFAULT '-',
  `capex_number` varchar(10) NOT NULL DEFAULT '-',
  `quotation_number` int(11) NOT NULL DEFAULT '0' COMMENT 'FK quotation',
  `choosed_quotation_item_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

/*Data for the table `material_request` */

insert  into `material_request`(`item_id`,`request_number`,`item_description`,`item_qty`,`item_price`,`item_type`,`coa_type_id`,`coa_number`,`asset_number`,`capex_number`,`quotation_number`,`choosed_quotation_item_id`) values (51,16,'Dell Optilex 3010MT',2,0,'MATERIAL',0,'-','-','-',0,0),(52,16,'Switch HP ProCurve 1410-24G',1,0,'MATERIAL',0,'-','-','-',0,0),(59,19,'Meja Staff 1500X600X750',1,300000,'MATERIAL',0,' 5-1','1','-',1,1),(60,19,'Meja Staff 1200X600X750',1,200000,'MATERIAL',0,' 5-2','2','-',1,2),(61,19,'Carpet tile 50cm X 50 cm',1,700000,'MATERIAL',0,' 1-1','3','61',1,3),(62,19,'Pasang pintu kaca dan partisi kaca 12mm',1,1000000,'MATERIAL',0,' 5-3','4','-',1,4),(63,19,'Perbaikan plafon/drop ceiling & pengecetan ulang(cat warna putih)',1,800000,'SERVICE',0,'-','-','-',7,15),(64,19,'Pengecetan dinding/partisi existing',1,1000000,'SERVICE',0,'-','-','-',7,16);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_parent_id` int(11) NOT NULL DEFAULT '0',
  `menu_segment` varchar(250) NOT NULL,
  `menu_name` varchar(250) NOT NULL,
  `menu_desc` varchar(500) NOT NULL,
  `menu_active` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `menu_link` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `menu_action` varchar(500) NOT NULL DEFAULT 'view,edit,add,delete',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`menu_id`,`menu_parent_id`,`menu_segment`,`menu_name`,`menu_desc`,`menu_active`,`menu_link`,`menu_action`) values (1,0,'','Access Management','Setting Menu Access Privilege','YES','NO','view'),(2,1,'menu','Menu','Setting Menu','YES','YES','view,edit,add,remove'),(3,1,'user_group','User Group','Setting User Group','YES','YES','view,edit,add,remove'),(4,1,'user','User','Setting User','YES','YES','view,edit,add,remove'),(5,1,'privilege','Privilege','Setting privilege foreach user group','YES','YES','view'),(9,0,'dashboard','Dashboard','User Dashboard : statistics and more','YES','YES','view'),(10,0,'','Department','Department : List department, manage budget per/department, view budget history','YES','NO','view'),(11,10,'department','List','List Department : CRUD','YES','YES','view,edit,add,remove'),(12,10,'annual_budget','Annual Budget','Setting Budget per/Division','YES','YES','view'),(13,0,'','COA','Chart Of Accounts','YES','NO','view'),(14,13,'coa_type','Type','List of COA Type','YES','YES','view,edit,add,remove'),(15,16,'request_form','My Department','Request Form Specific to Each Department','YES','YES','view,edit,add,remove'),(16,0,'','Request Form','parent Menu Request Form','YES','NO','view'),(17,16,'request_form_all','My Task','All Request Form','YES','YES','view'),(18,0,'supplier','Supplier','List Supplier','YES','YES','view,edit,add,remove');

/*Table structure for table `payment` */

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `payment_method` enum('CHEQUE','CASH','TRANSFER') NOT NULL,
  `payment_ref_number` varchar(150) NOT NULL DEFAULT '-' COMMENT 'Cheque number or payment_reference number for TRANSFER',
  `payment_date` datetime NOT NULL,
  `paid_by` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payment` */

/*Table structure for table `privilege` */

DROP TABLE IF EXISTS `privilege`;

CREATE TABLE `privilege` (
  `privilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `privilege_action` varchar(500) NOT NULL,
  PRIMARY KEY (`privilege_id`),
  UNIQUE KEY `uniq_menu_id_user_group_id` (`menu_id`,`user_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;

/*Data for the table `privilege` */

insert  into `privilege`(`privilege_id`,`menu_id`,`user_group_id`,`privilege_action`) values (1,1,1,'view'),(2,1,2,'view'),(3,2,1,'view,edit,add,remove'),(4,2,2,''),(5,3,1,'view,edit,add,remove'),(6,3,2,'view,edit,add,remove'),(7,4,1,'view,edit,add,remove'),(8,4,2,'view,edit,add,remove'),(9,5,1,'view'),(10,5,2,'view'),(15,1,3,''),(16,2,3,''),(17,3,3,''),(18,4,3,''),(19,5,3,''),(24,9,1,'view'),(25,9,2,'view'),(26,9,3,'view'),(27,10,1,'view'),(28,10,2,'view'),(29,10,3,'view'),(30,11,1,'view,edit,add,remove'),(31,11,2,'view,edit,add,remove'),(32,11,3,'view,edit,add'),(33,1,4,''),(34,2,4,''),(35,3,4,''),(36,4,4,''),(37,5,4,''),(39,9,4,'view'),(40,10,4,''),(41,11,4,''),(42,1,5,''),(43,2,5,''),(44,3,5,''),(45,4,5,''),(46,5,5,''),(48,9,5,'view'),(49,10,5,''),(50,11,5,''),(51,1,6,''),(52,2,6,''),(53,3,6,''),(54,4,6,''),(55,5,6,''),(57,9,6,'view'),(58,10,6,''),(59,11,6,''),(60,1,7,''),(61,2,7,''),(62,3,7,''),(63,4,7,''),(64,5,7,''),(66,9,7,'view'),(67,10,7,'view'),(68,11,7,'view'),(105,12,1,'view'),(106,12,2,'view'),(107,12,3,''),(108,12,4,'view'),(109,12,5,''),(110,12,6,''),(111,12,7,'view'),(112,1,8,''),(113,2,8,''),(114,3,8,''),(115,4,8,''),(116,5,8,''),(117,9,8,'view'),(118,10,8,''),(119,11,8,''),(120,12,8,''),(121,13,1,'view'),(122,13,2,'view'),(123,13,3,''),(124,13,4,'view'),(125,13,5,''),(126,13,6,''),(127,13,7,''),(128,13,8,''),(129,14,1,'view,edit,add,remove'),(130,14,2,'view,edit,add,remove'),(131,14,3,''),(132,14,4,'view,edit,add'),(133,14,5,''),(134,14,6,''),(135,14,7,''),(136,14,8,''),(137,15,1,'view,edit,add,remove'),(138,15,2,'view,edit,add,remove'),(139,15,3,'view,edit,add,remove'),(140,15,4,'view,edit,add,remove'),(141,15,5,'view,edit,add,remove'),(142,15,6,'view,edit,add,remove'),(143,15,7,'view,edit,add,remove'),(144,15,8,'view,edit,add,remove'),(145,16,1,'view'),(146,16,2,'view'),(147,16,3,'view'),(148,16,4,'view'),(149,16,5,'view'),(150,16,6,'view'),(151,16,7,'view'),(152,16,8,'view'),(153,17,1,'view'),(154,17,2,'view'),(155,17,3,'view'),(156,17,4,'view'),(157,17,5,'view'),(158,17,6,'view'),(159,17,7,'view'),(160,17,8,''),(161,18,1,'view,edit,add,remove'),(162,18,2,'view,edit,add,remove'),(163,18,3,'view,edit,add,remove'),(164,18,4,''),(165,18,5,''),(166,18,6,''),(167,18,7,''),(168,18,8,'');

/*Table structure for table `quotation` */

DROP TABLE IF EXISTS `quotation`;

CREATE TABLE `quotation` (
  `quotation_number` int(11) NOT NULL AUTO_INCREMENT,
  `request_number` int(11) NOT NULL COMMENT 'FK for request_form',
  `supplier_id` varchar(500) NOT NULL COMMENT 'FK for supplier',
  `payment_step` enum('FULL_PAYMENT','DOWN_PAYMENT') NOT NULL DEFAULT 'FULL_PAYMENT',
  `quotation_file` varchar(150) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `group_number` double NOT NULL,
  PRIMARY KEY (`quotation_number`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `quotation` */

insert  into `quotation`(`quotation_number`,`request_number`,`supplier_id`,`payment_step`,`quotation_file`,`created_date`,`group_number`) values (1,19,'3','FULL_PAYMENT','7343d50a4ff905b1de8e70b613e1fc4d.pdf','2014-12-07 08:41:22',1),(2,19,'4','FULL_PAYMENT','5d33797644836944f9f922132e00d5cf.pdf','2014-12-07 08:41:23',1),(7,19,'3','FULL_PAYMENT','bc35d2734d42dbed530fabe750cbed49.pdf','2014-12-07 08:59:35',2),(8,19,'4','FULL_PAYMENT','863182c39c3f067cb3ec41e2bfdb119a.pdf','2014-12-07 08:59:35',2);

/*Table structure for table `quotation_item` */

DROP TABLE IF EXISTS `quotation_item`;

CREATE TABLE `quotation_item` (
  `quotation_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_number` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`quotation_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `quotation_item` */

insert  into `quotation_item`(`quotation_item_id`,`quotation_number`,`item_id`,`price`) values (1,1,59,300000),(2,1,60,200000),(3,1,61,700000),(4,1,62,1000000),(5,2,59,350000),(6,2,60,250000),(7,2,61,800000),(8,2,62,1100000),(15,7,63,800000),(16,7,64,1000000),(17,8,63,700000),(18,8,64,900000);

/*Table structure for table `request_order` */

DROP TABLE IF EXISTS `request_order`;

CREATE TABLE `request_order` (
  `request_order_number` int(11) NOT NULL AUTO_INCREMENT,
  `request_number` int(11) NOT NULL COMMENT 'FK for request form',
  `quotation_number` int(11) NOT NULL COMMENT 'FK for quotation',
  `group_number` int(2) NOT NULL,
  `request_type` enum('PO','WO') NOT NULL DEFAULT 'PO' COMMENT 'PO|WO',
  `process_date` datetime NOT NULL COMMENT 'DATE when PO|WO send to supplier',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL COMMENT 'FK for user',
  PRIMARY KEY (`request_order_number`),
  KEY `group_number` (`request_number`,`group_number`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

/*Data for the table `request_order` */

insert  into `request_order`(`request_order_number`,`request_number`,`quotation_number`,`group_number`,`request_type`,`process_date`,`created_date`,`created_by`) values (82,19,1,1,'PO','2014-12-07 11:36:57','2014-12-07 18:36:57',6),(84,19,7,2,'WO','2014-12-07 11:36:57','2014-12-07 18:36:57',6);

/*Table structure for table `supplier` */

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(250) NOT NULL,
  `supplier_business` varchar(250) NOT NULL,
  `supplier_telp` varchar(50) NOT NULL,
  `supplier_fax` varchar(50) NOT NULL,
  `supplier_email` varchar(250) NOT NULL,
  `supplier_address` text NOT NULL,
  `supplier_website_url` varchar(250) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `supplier` */

insert  into `supplier`(`supplier_id`,`supplier_name`,`supplier_business`,`supplier_telp`,`supplier_fax`,`supplier_email`,`supplier_address`,`supplier_website_url`) values (1,'Bhinneka','ecommerce,jual online,komputer','021-2929-2828','-','support@bhinneka.com','Jakarta Pusat, hayam huruk','http://www.bhinneka.com'),(2,'PASIFIC COMPUTER','komputer','+62-778-431075','+62-778-42344','sales@pasific-computer.com','Komplek Penuin Center Blok A NO. 12, Batam 29342-Kepri-Indonesia','-'),(3,'PT. Indodimensi Pratama Sejahtera','interior,display,meja,pintu,kaca,plafon,bangunan,ceiling,cat,carpet','(0776) 451053','(0778) 451006','indodimensi_ps@yahoo.co.id','Komplek Batu Batam Permai Blok 5 NO. 01 & 02 Batam-Indonesia','-'),(4,'PT. Cipta Surya Graha','interior,design,plafon,cat,meja,pintu','0778 428689','0778 421238','orangeinteriordesign@yahoo.com','Komp. Bumi Riau Makmur Block C No. 12-13, Sungai Panas - Batam','-'),(5,'lazada','e-commerce,shopping,online,marketplace,market','021 29490200','021 29490200','support@lazada.com','Jakarta Selatan, kuningan barat','http://www.lazada.co.id/'),(6,'tokopedia','e-commerce,shopping,online,marketplace,market','021 29490200','021 29490200','support@tokopedia.com','Jakarta selatan, grand wijaya center','https://www.tokopedia.com');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `user_password` varchar(500) NOT NULL,
  `user_fullname` varchar(250) NOT NULL,
  `user_active` enum('YES','NO') NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_group_id`,`user_email`,`user_password`,`user_fullname`,`user_active`) values (1,1,'if07087@gmail.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Harry Osmar Sitohang','YES'),(2,3,'admin@sez.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Chaterina','YES'),(3,4,'santi@sez.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Santi','YES'),(4,5,'ani@sez.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Ani','YES'),(5,6,'bambang@sez.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Bambang','YES'),(6,7,'luckyh@sez.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Lucky Hendrati','YES'),(7,2,'qwantest@gmail.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Qwantest Simatupang','YES'),(8,8,'agus@sez.com','fbc12e1e4dbf15ac092000a65e96fd2fb8b08ebe','Agus Widodo','YES');

/*Table structure for table `user_group` */

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `user_group_id` int(20) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL COMMENT 'FK department',
  `user_group_type` varchar(250) NOT NULL,
  `user_group_desc` text NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `user_group` */

insert  into `user_group`(`user_group_id`,`department_id`,`user_group_type`,`user_group_desc`) values (1,1,'DEVELOPER','Granted all access.'),(2,1,'SUPERADMIN','Granted all access, but not for Menu  management edit, add, delete.'),(3,2,'ADMIN','Admin Assistant'),(4,3,'ACCOUNTING','Accounting'),(5,4,'FINANCE','Finance'),(6,5,'OPERATIONAL','Head Operational'),(7,6,'DIRECTOR','Director/General Manager'),(8,1,'IT','IT');

/* Trigger structure for table `form_request` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_delete_form_request` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_delete_form_request` AFTER DELETE ON `form_request` FOR EACH ROW BEGIN
	delete from `material_request` where `request_number` = OLD.request_number;
	DELETE FROM `approval` WHERE `request_number` = OLD.request_number;
	DELETE FROM `quotation` WHERE `request_number` = OLD.request_number;
    END */$$


DELIMITER ;

/* Trigger structure for table `menu` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_insert_menu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_insert_menu` AFTER INSERT ON `menu` FOR EACH ROW BEGIN
	call sp_fix_privilege_per_menu(NEW.menu_id, NEW.menu_action);
    END */$$


DELIMITER ;

/* Trigger structure for table `menu` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_update_menu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_update_menu` AFTER UPDATE ON `menu` FOR EACH ROW BEGIN
	update `privilege` set `privilege_action` = NEW.menu_action WHERE (`user_group_id` = 1 AND `menu_id` = NEW.menu_id) OR (`user_group_id` = 2  AND `menu_id` = NEW.menu_id AND NEW.menu_segment <> 'menu');
    END */$$


DELIMITER ;

/* Trigger structure for table `menu` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_delete_menu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_delete_menu` AFTER DELETE ON `menu` FOR EACH ROW BEGIN
	delete from `privilege` where `menu_id` = OLD.menu_id; #Fix Privilege
	#UPDATE `menu` set menu_parent_id = OLD.menu_parent_id WHERE menu_parent_id = OLD.menu_id; #Fix Menu must exectude in server side
    END */$$


DELIMITER ;

/* Trigger structure for table `quotation` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_delete_quotation` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_delete_quotation` AFTER DELETE ON `quotation` FOR EACH ROW BEGIN
	delete from `quotation_item` where `quotation_number` = OLD.quotation_number;
    END */$$


DELIMITER ;

/* Trigger structure for table `request_order` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_insert_request_order` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_insert_request_order` AFTER INSERT ON `request_order` FOR EACH ROW BEGIN
	call sp_update_choosed_quotation_item_id(NEW.quotation_number);
    END */$$


DELIMITER ;

/* Trigger structure for table `request_order` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_update_request_order` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_update_request_order` AFTER UPDATE ON `request_order` FOR EACH ROW BEGIN
	call sp_update_choosed_quotation_item_id(NEW.quotation_number);
    END */$$


DELIMITER ;

/* Trigger structure for table `request_order` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_delete_request_order` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_delete_request_order` AFTER DELETE ON `request_order` FOR EACH ROW BEGIN
	call sp_update_choosed_quotation_item_id_to_zero(OLD.quotation_number);
	delete from delivery_order where request_order_number = OLD.request_order_number;
	#update material_request set quotation_number =0 where quotation_number = OLD.quotation_number;
    END */$$


DELIMITER ;

/* Trigger structure for table `user_group` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_insert_user_group` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_insert_user_group` AFTER INSERT ON `user_group` FOR EACH ROW BEGIN
	call sp_fix_privilege_per_user_group(NEW.user_group_id, NEW.user_group_type);
    END */$$


DELIMITER ;

/* Trigger structure for table `user_group` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `trigger_by_delete_user_group` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'%' */ /*!50003 TRIGGER `trigger_by_delete_user_group` AFTER DELETE ON `user_group` FOR EACH ROW BEGIN
	delete from `privilege` where `user_group_id` = OLD.user_group_id;
    END */$$


DELIMITER ;

/* Function  structure for function  `fx_count_material_in_request_form` */

/*!50003 DROP FUNCTION IF EXISTS `fx_count_material_in_request_form` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` FUNCTION `fx_count_material_in_request_form`(_request_number INT) RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
BEGIN
	RETURN  (SELECT COUNT(1) AS `count` FROM material_request WHERE request_number = _request_number);
    END */$$
DELIMITER ;

/* Function  structure for function  `fx_get_approval_state` */

/*!50003 DROP FUNCTION IF EXISTS `fx_get_approval_state` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` FUNCTION `fx_get_approval_state`(_request_number INT) RETURNS text CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BLOCK1: BEGIN
		DECLARE done1 INT DEFAULT FALSE;
		declare _department_id, _counter INT;
		declare _result text;
		DECLARE cur1 CURSOR FOR SELECT `approval_department_id` FROM `view_approval` where request_number = _request_number;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
		OPEN cur1;
		set _counter = 0;
		set _result = '';
		read_loop1: LOOP
			FETCH cur1 INTO _department_id;
			IF done1 THEN	
				CLOSE cur1;
				LEAVE read_loop1;
			END IF;	
			set _counter = _counter + 1;
			if _counter = 1 then
				set _result = _department_id;
			else
				set _result = CONCAT(_result, ',', _department_id);
			end if;
		END LOOP read_loop1;
		return _result;
	END BLOCK1 */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fix_budget` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fix_budget` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sp_fix_budget`(_year double)
BLOCK1: BEGIN
	DECLARE done1 INT DEFAULT FALSE;
	DECLARE _division_id, _count INT;
	DECLARE cur1 CURSOR FOR SELECT `division_id` FROM `division`;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	OPEN cur1;
	read_loop1: LOOP
		FETCH cur1 INTO _division_id;
		IF done1 THEN	
			CLOSE cur1;
			LEAVE read_loop1;
		END IF;
		SET _count = (SELECT COUNT(1) AS `count` FROM `budget` WHERE `division_id` = _division_id AND `year` = _year);
		IF _count = 0 then
			INSERT INTO `budget` (`division_id`, `year`) VALUES (_division_id, _year);	
		end if;
	END LOOP read_loop1;
    END BLOCK1 */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fix_privilege` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fix_privilege` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sp_fix_privilege`()
BLOCK1: BEGIN
	DECLARE done1 INT DEFAULT FALSE;
	DECLARE _menu_id INT;
	declare	_menu_segment, _menu_action TEXT;
	DECLARE cur1 CURSOR FOR SELECT `menu_id`, `menu_segment`, `menu_action` FROM `menu`;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	OPEN cur1;
	read_loop1: LOOP
		FETCH cur1 INTO _menu_id, _menu_segment, _menu_action;
		IF done1 THEN	
			CLOSE cur1;
			LEAVE read_loop1;
		END IF;
		BLOCK2: BEGIN
			DECLARE done2 INT DEFAULT FALSE;
			DECLARE _user_group_id, _count_menu INT;
			DECLARE _user_group_type TEXT;
			DECLARE cur2 CURSOR FOR SELECT `user_group_id`, `user_group_type` FROM `user_group`;
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = TRUE;
			OPEN cur2;
			read_loop2: LOOP
				FETCH cur2 INTO _user_group_id, _user_group_type;
				IF done2 THEN	
					CLOSE cur2;
					LEAVE read_loop2;
				END IF;
				SET _count_menu = (SELECT COUNT(1) AS `count` FROM `privilege` WHERE `menu_id` = _menu_id AND `user_group_id` = _user_group_id);
				IF (_count_menu = 0 AND _user_group_type = 'DEVELOPER') OR (_count_menu = 0 AND _user_group_type = 'SUPERADMIN' AND _menu_segment <> 'menu') THEN
					INSERT INTO `privilege` (`menu_id`, `user_group_id`, `privilege_action`) VALUES (_menu_id, _user_group_id, _menu_action);
				ELSEIF (_count_menu <> 0 AND _user_group_type = 'DEVELOPER') OR (_count_menu <> 0 AND _user_group_type = 'SUPERADMIN' AND _menu_segment <> 'menu') THEN
					UPDATE `privilege` SET `menu_id` = _menu_id, `user_group_id` = _user_group_id, `privilege_action` = _menu_action WHERE `menu_id` = _menu_id AND `user_group_id` = _user_group_id;
				ELSEIF _count_menu = 0 THEN
					INSERT INTO `privilege` (`menu_id`, `user_group_id`, `privilege_action`) VALUES (_menu_id, _user_group_id, '');
				END IF;
			END LOOP read_loop2;
		END BLOCK2;
	END LOOP read_loop1;
    END BLOCK1 */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fix_privilege_per_menu` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fix_privilege_per_menu` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sp_fix_privilege_per_menu`(_menu_id INT, _menu_action TEXT)
BLOCK1: BEGIN
	DECLARE done1 INT DEFAULT FALSE;
	DECLARE _user_group_id, _count_menu INT;
	declare	_user_group_type TEXT;
	DECLARE cur1 CURSOR FOR SELECT `user_group_id`, `user_group_type` FROM `user_group`;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	OPEN cur1;
	read_loop1: LOOP
		FETCH cur1 INTO _user_group_id, _user_group_type;
		IF done1 THEN	
			CLOSE cur1;
			LEAVE read_loop1;
		END IF;
		SET _count_menu = (SELECT COUNT(1) AS `count` FROM `privilege` WHERE `menu_id` = _menu_id AND `user_group_id` = _user_group_id);
		IF _count_menu = 0 AND (_user_group_type = 'DEVELOPER' OR _user_group_type = 'SUPERADMIN') then
			INSERT INTO `privilege` (`menu_id`, `user_group_id`, `privilege_action`) VALUES (_menu_id, _user_group_id, _menu_action);
		elseif _count_menu = 0 THEN
			INSERT INTO `privilege` (`menu_id`, `user_group_id`, `privilege_action`) VALUES (_menu_id, _user_group_id, '');		
		end if;
	END LOOP read_loop1;
    END BLOCK1 */$$
DELIMITER ;

/* Procedure structure for procedure `sp_fix_privilege_per_user_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_fix_privilege_per_user_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sp_fix_privilege_per_user_group`(_user_group_id INT, _user_group_type TEXT)
BLOCK1: BEGIN
	DECLARE done1 INT DEFAULT FALSE;
	DECLARE _menu_id, _count_menu INT;
	declare	_menu_segment, _menu_action TEXT;
	DECLARE cur1 CURSOR FOR SELECT `menu_id`, `menu_segment`, `menu_action` FROM `menu`;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	OPEN cur1;
	read_loop1: LOOP
		FETCH cur1 INTO _menu_id, _menu_segment, _menu_action;
		IF done1 THEN	
			CLOSE cur1;
			LEAVE read_loop1;
		END IF;
		SET _count_menu = (SELECT COUNT(1) AS `count` FROM `privilege` WHERE `menu_id` = _menu_id AND `user_group_id` = _user_group_id);
		IF _count_menu = 0 AND (_user_group_type = 'DEVELOPER' OR _user_group_type = 'SUPERADMIN') THEN
			INSERT INTO `privilege` (`menu_id`, `user_group_id`, `privilege_action`) VALUES (_menu_id, _user_group_id, _menu_action);
		elseif  _count_menu = 0 then
			INSERT INTO `privilege` (`menu_id`, `user_group_id`, `privilege_action`) VALUES (_menu_id, _user_group_id, '');		
		end if;
	END LOOP read_loop1;
    END BLOCK1 */$$
DELIMITER ;

/* Procedure structure for procedure `sp_update_choosed_quotation_item_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_update_choosed_quotation_item_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sp_update_choosed_quotation_item_id`(_quotation_number INT)
BLOCK1: BEGIN
	DECLARE done1 INT DEFAULT FALSE;
	DECLARE _quotation_item_id, _item_id INT;
	DECLARE _price DOUBLE;
	DECLARE cur1 CURSOR FOR SELECT `quotation_item_id`, `item_id`, `price` FROM `quotation_item` where quotation_number = _quotation_number;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	OPEN cur1;
	read_loop1: LOOP
		FETCH cur1 INTO _quotation_item_id, _item_id, _price;
		IF done1 THEN	
			CLOSE cur1;
			LEAVE read_loop1;
		END IF;
		UPDATE material_request set choosed_quotation_item_id = _quotation_item_id, item_price = _price, quotation_number = _quotation_number where item_id = _item_id;
	END LOOP read_loop1;
    END BLOCK1 */$$
DELIMITER ;

/* Procedure structure for procedure `sp_update_choosed_quotation_item_id_to_zero` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_update_choosed_quotation_item_id_to_zero` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sp_update_choosed_quotation_item_id_to_zero`(_quotation_number INT)
BLOCK1: BEGIN
	DECLARE done1 INT DEFAULT FALSE;
	DECLARE _quotation_item_id, _item_id INT;
	DECLARE cur1 CURSOR FOR SELECT `quotation_item_id`, `item_id` FROM `quotation_item` WHERE quotation_number = _quotation_number;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	OPEN cur1;
	read_loop1: LOOP
		FETCH cur1 INTO _quotation_item_id, _item_id;
		IF done1 THEN	
			CLOSE cur1;
			LEAVE read_loop1;
		END IF;
		UPDATE material_request set choosed_quotation_item_id = 0, item_price = 0, quotation_number = 0, coa_number = '-', asset_number = '-' where item_id = _item_id;
	END LOOP read_loop1;
    END BLOCK1 */$$
DELIMITER ;

/*Table structure for table `view_approval` */

DROP TABLE IF EXISTS `view_approval`;

/*!50001 DROP VIEW IF EXISTS `view_approval` */;
/*!50001 DROP TABLE IF EXISTS `view_approval` */;

/*!50001 CREATE TABLE  `view_approval`(
 `approval_id` int(11) ,
 `request_number` int(11) ,
 `approved_by` int(11) ,
 `approved_date` timestamp ,
 `user_group_id` int(11) ,
 `user_email` varchar(500) ,
 `user_fullname` varchar(250) ,
 `user_group_type` varchar(250) ,
 `approval_department_id` int(11) ,
 `department_name` varchar(250) 
)*/;

/*Table structure for table `view_quotation` */

DROP TABLE IF EXISTS `view_quotation`;

/*!50001 DROP VIEW IF EXISTS `view_quotation` */;
/*!50001 DROP TABLE IF EXISTS `view_quotation` */;

/*!50001 CREATE TABLE  `view_quotation`(
 `quotation_number` int(11) ,
 `request_number` int(11) ,
 `supplier_id` varchar(500) ,
 `payment_step` enum('FULL_PAYMENT','DOWN_PAYMENT') ,
 `quotation_file` varchar(150) ,
 `created_date` timestamp ,
 `group_number` double ,
 `supplier_name` varchar(250) ,
 `supplier_business` varchar(250) ,
 `supplier_telp` varchar(50) ,
 `supplier_fax` varchar(50) ,
 `supplier_email` varchar(250) ,
 `supplier_address` text ,
 `supplier_website_url` varchar(250) 
)*/;

/*Table structure for table `view_quotation_detail` */

DROP TABLE IF EXISTS `view_quotation_detail`;

/*!50001 DROP VIEW IF EXISTS `view_quotation_detail` */;
/*!50001 DROP TABLE IF EXISTS `view_quotation_detail` */;

/*!50001 CREATE TABLE  `view_quotation_detail`(
 `quotation_number` int(11) ,
 `request_number` int(11) ,
 `supplier_id` varchar(500) ,
 `payment_step` enum('FULL_PAYMENT','DOWN_PAYMENT') ,
 `quotation_file` varchar(150) ,
 `created_date` timestamp ,
 `group_number` double ,
 `supplier_name` varchar(250) ,
 `supplier_business` varchar(250) ,
 `supplier_telp` varchar(50) ,
 `supplier_fax` varchar(50) ,
 `supplier_email` varchar(250) ,
 `supplier_address` text ,
 `supplier_website_url` varchar(250) ,
 `item_id` int(11) ,
 `item_qty` int(11) ,
 `price` double ,
 `item_description` varchar(500) ,
 `item_type` enum('MATERIAL','SERVICE') 
)*/;

/*Table structure for table `view_request_form` */

DROP TABLE IF EXISTS `view_request_form`;

/*!50001 DROP VIEW IF EXISTS `view_request_form` */;
/*!50001 DROP TABLE IF EXISTS `view_request_form` */;

/*!50001 CREATE TABLE  `view_request_form`(
 `request_number` int(11) ,
 `request_date` timestamp ,
 `request_by` int(11) ,
 `proceed` enum('YES','NO') ,
 `proceed_date` datetime ,
 `proceed_by` int(11) ,
 `user_group_id` int(11) ,
 `user_fullname` varchar(250) ,
 `department_id` int(11) ,
 `department_name` varchar(250) ,
 `total_item` int(11) 
)*/;

/*Table structure for table `view_request_form_detail` */

DROP TABLE IF EXISTS `view_request_form_detail`;

/*!50001 DROP VIEW IF EXISTS `view_request_form_detail` */;
/*!50001 DROP TABLE IF EXISTS `view_request_form_detail` */;

/*!50001 CREATE TABLE  `view_request_form_detail`(
 `item_id` int(11) ,
 `request_number` int(11) ,
 `quotation_number` int(11) ,
 `item_description` varchar(500) ,
 `item_qty` int(11) ,
 `item_price` double ,
 `item_type` enum('MATERIAL','SERVICE') ,
 `coa_type_id` int(11) ,
 `coa_number` varchar(10) ,
 `asset_number` varchar(10) ,
 `capex_number` varchar(10) ,
 `request_date` timestamp ,
 `proceed` enum('YES','NO') ,
 `proceed_date` datetime ,
 `proceed_by` int(11) ,
 `user_id` int(11) ,
 `user_group_id` int(11) ,
 `user_fullname` varchar(250) 
)*/;

/*Table structure for table `view_user` */

DROP TABLE IF EXISTS `view_user`;

/*!50001 DROP VIEW IF EXISTS `view_user` */;
/*!50001 DROP TABLE IF EXISTS `view_user` */;

/*!50001 CREATE TABLE  `view_user`(
 `user_id` int(11) ,
 `user_group_id` int(11) ,
 `user_group_type` varchar(250) ,
 `user_email` varchar(500) ,
 `user_password` varchar(500) ,
 `user_fullname` varchar(250) ,
 `user_active` enum('YES','NO') 
)*/;

/*View structure for view view_approval */

/*!50001 DROP TABLE IF EXISTS `view_approval` */;
/*!50001 DROP VIEW IF EXISTS `view_approval` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_approval` AS (select `a`.`approval_id` AS `approval_id`,`a`.`request_number` AS `request_number`,`a`.`approved_by` AS `approved_by`,`a`.`approved_date` AS `approved_date`,`u`.`user_group_id` AS `user_group_id`,`u`.`user_email` AS `user_email`,`u`.`user_fullname` AS `user_fullname`,`g`.`user_group_type` AS `user_group_type`,`d`.`department_id` AS `approval_department_id`,`d`.`department_name` AS `department_name` from (((`approval` `a` join `user` `u` on((`u`.`user_id` = `a`.`approved_by`))) join `user_group` `g` on((`g`.`user_group_id` = `u`.`user_group_id`))) join `department` `d` on((`d`.`department_id` = `g`.`department_id`)))) */;

/*View structure for view view_quotation */

/*!50001 DROP TABLE IF EXISTS `view_quotation` */;
/*!50001 DROP VIEW IF EXISTS `view_quotation` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_quotation` AS (select `q`.`quotation_number` AS `quotation_number`,`q`.`request_number` AS `request_number`,`q`.`supplier_id` AS `supplier_id`,`q`.`payment_step` AS `payment_step`,`q`.`quotation_file` AS `quotation_file`,`q`.`created_date` AS `created_date`,`q`.`group_number` AS `group_number`,`s`.`supplier_name` AS `supplier_name`,`s`.`supplier_business` AS `supplier_business`,`s`.`supplier_telp` AS `supplier_telp`,`s`.`supplier_fax` AS `supplier_fax`,`s`.`supplier_email` AS `supplier_email`,`s`.`supplier_address` AS `supplier_address`,`s`.`supplier_website_url` AS `supplier_website_url` from (`quotation` `q` join `supplier` `s` on((`s`.`supplier_id` = `q`.`supplier_id`)))) */;

/*View structure for view view_quotation_detail */

/*!50001 DROP TABLE IF EXISTS `view_quotation_detail` */;
/*!50001 DROP VIEW IF EXISTS `view_quotation_detail` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_quotation_detail` AS (select `q`.`quotation_number` AS `quotation_number`,`q`.`request_number` AS `request_number`,`q`.`supplier_id` AS `supplier_id`,`q`.`payment_step` AS `payment_step`,`q`.`quotation_file` AS `quotation_file`,`q`.`created_date` AS `created_date`,`q`.`group_number` AS `group_number`,`q`.`supplier_name` AS `supplier_name`,`q`.`supplier_business` AS `supplier_business`,`q`.`supplier_telp` AS `supplier_telp`,`q`.`supplier_fax` AS `supplier_fax`,`q`.`supplier_email` AS `supplier_email`,`q`.`supplier_address` AS `supplier_address`,`q`.`supplier_website_url` AS `supplier_website_url`,`i`.`item_id` AS `item_id`,`mr`.`item_qty` AS `item_qty`,`i`.`price` AS `price`,`mr`.`item_description` AS `item_description`,`mr`.`item_type` AS `item_type` from ((`view_quotation` `q` join `quotation_item` `i` on((`i`.`quotation_number` = `q`.`quotation_number`))) join `material_request` `mr` on((`mr`.`item_id` = `i`.`item_id`)))) */;

/*View structure for view view_request_form */

/*!50001 DROP TABLE IF EXISTS `view_request_form` */;
/*!50001 DROP VIEW IF EXISTS `view_request_form` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_request_form` AS (select `r`.`request_number` AS `request_number`,`r`.`request_date` AS `request_date`,`r`.`request_by` AS `request_by`,`r`.`proceed` AS `proceed`,`r`.`proceed_date` AS `proceed_date`,`r`.`proceed_by` AS `proceed_by`,`u`.`user_group_id` AS `user_group_id`,`u`.`user_fullname` AS `user_fullname`,`d`.`department_id` AS `department_id`,`d`.`department_name` AS `department_name`,`fx_count_material_in_request_form`(`r`.`request_number`) AS `total_item` from (((`form_request` `r` join `user` `u` on((`u`.`user_id` = `r`.`request_by`))) join `user_group` `g` on((`g`.`user_group_id` = `u`.`user_group_id`))) join `department` `d` on((`d`.`department_id` = `g`.`department_id`)))) */;

/*View structure for view view_request_form_detail */

/*!50001 DROP TABLE IF EXISTS `view_request_form_detail` */;
/*!50001 DROP VIEW IF EXISTS `view_request_form_detail` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_request_form_detail` AS (select `i`.`item_id` AS `item_id`,`i`.`request_number` AS `request_number`,`i`.`quotation_number` AS `quotation_number`,`i`.`item_description` AS `item_description`,`i`.`item_qty` AS `item_qty`,`i`.`item_price` AS `item_price`,`i`.`item_type` AS `item_type`,`i`.`coa_type_id` AS `coa_type_id`,`i`.`coa_number` AS `coa_number`,`i`.`asset_number` AS `asset_number`,`i`.`capex_number` AS `capex_number`,`r`.`request_date` AS `request_date`,`r`.`proceed` AS `proceed`,`r`.`proceed_date` AS `proceed_date`,`r`.`proceed_by` AS `proceed_by`,`u`.`user_id` AS `user_id`,`u`.`user_group_id` AS `user_group_id`,`u`.`user_fullname` AS `user_fullname` from ((`material_request` `i` join `form_request` `r` on((`r`.`request_number` = `i`.`request_number`))) join `user` `u` on((`u`.`user_id` = `r`.`request_by`)))) */;

/*View structure for view view_user */

/*!50001 DROP TABLE IF EXISTS `view_user` */;
/*!50001 DROP VIEW IF EXISTS `view_user` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_user` AS (select `u`.`user_id` AS `user_id`,`u`.`user_group_id` AS `user_group_id`,`g`.`user_group_type` AS `user_group_type`,`u`.`user_email` AS `user_email`,`u`.`user_password` AS `user_password`,`u`.`user_fullname` AS `user_fullname`,`u`.`user_active` AS `user_active` from (`user` `u` join `user_group` `g` on((`g`.`user_group_id` = `u`.`user_group_id`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
