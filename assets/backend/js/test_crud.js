var test_crud = {
    test_crud: function() {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        if (typeof (crud) !== "undefined" && enable_crud == true) {
            self.crud();
        }
        /*END INIT PARENT JS*/
    }
};

test_crud = $.extend(crud, test_crud);

$(function() {
    crud.test_crud();
});