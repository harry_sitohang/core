var annual_budget = {
    form_id: "#budget-form",
    btn_submit: '#budget-form [type=submit]',
    annual_budget: function() {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        /*END INIT PARENT JS*/

        //On change dropdown department
        $("select#department_id").bind('change', function(e) {
            e.preventDefault();
            $(self.form_id).find(".alert").remove(); //remove all alert msg in form
            $department_id = $(this).val();
            if ($(this).val() != 0) {
                if ($("select#year").val() != 0) {
                    self.load_amount($department_id, $("select#year").val());
                }
            } else {
                alert("Please Select Department");
            }
        });

        //On change dropdown years
        $("select#year").bind('change', function(e) {
            e.preventDefault();
            $(self.form_id).find(".alert").remove(); //remove all alert msg in form
            $year = $(this).val();
            if ($(this).val() != 0) {
                if ($("select#year").val() != 0) {
                    self.load_amount($("select#department_id").val(), $year);
                }
            } else {
                alert("Please Select Year");
            }
        });

        //On submit form
        $(self.form_id).bind('submit', function(e) {
            e.preventDefault();
            $formData = new FormData($(this)[0]);
            self.save_amount($formData);
        });
    },
    load_amount: function(department_id, year) {
        var self = this;
        $.ajax({
            url: class_url + 'load_amount',
            type: 'post',
            data: {
                department_id: department_id,
                year: year
            },
            dataType: 'json',
            async: true,
            beforeSend: function() {

            },
            error: function(request) {
                console.log(request.responseText);
            },
            success: function(json) {
                self.on_finish_ajax(json); //always call this on the first line
                if (json.status.match(/^success$/i)) {
                    $("#amount").val(json.amount);
                } else {
                    self.alert_msg({type: json.status, dom_container: $(self.form_id), msg: json.msg, fill_type: 'prepend'}); //error msg
                }
            }
        });
    },
    save_amount: function(data) {
        var self = this;

        //process Form POST data
        $.ajax({
            url: class_url + 'save_amount',
            type: 'post',
            data: data,
            dataType: 'json',
            async: true,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                $(self.btn_submit).button('loading'); //loading button submit
                $(self.form_id).find(".alert").remove(); //remove all alert msg in form
            },
            error: function(request) {
                $(self.btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function(json) {
                self.on_finish_ajax(json); //always call this on the first line
                $(self.btn_submit).button('reset');
                self.alert_msg({type: json.status, dom_container: $(self.form_id), msg: json.msg, fill_type: 'prepend'});
                self.scrollTop($(self.form_id), 500, $('html, body'), 45); //scroll to top
            }
        });
    }
};

annual_budget = $.extend(site, annual_budget);

$(function() {
    site.annual_budget();
});