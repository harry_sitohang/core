var request_form_all_datatable = {
    request_form_all_datatable: function() {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        if (typeof (datatable) !== "undefined" && enable_datatable == true) {
            self.datatable();
        }
        /*END INIT PARENT JS*/
    }
};

request_form_all_datatable = $.extend(datatable, request_form_all_datatable);

$(function() {
    datatable.request_form_all_datatable();
});