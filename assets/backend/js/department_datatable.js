var division_datatable = {
    division_datatable: function() {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        if (typeof (datatable) !== "undefined" && enable_datatable == true) {
            self.datatable();
        }
        /*END INIT PARENT JS*/
    }
};

division_datatable = $.extend(datatable, division_datatable);

$(function() {
    datatable.division_datatable();
});