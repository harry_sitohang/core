var backend = {
    backend: function() {
        $("body").delegate(".sidebar-menu-link", "click", function(e) {
            $href = $(this).attr('href');
            if ($(e.target).is('a.sidebar-menu-link') && $href != 'javascript:;') {
                window.location = $href;
            }
        });
    }
};

backend = $.extend(site, backend);

$(function() {
    site.backend();
});