var test_datatable = {
    test_datatable: function() {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        if (typeof (datatable) !== "undefined" && enable_datatable == true) {
            self.datatable();
        }
        /*END INIT PARENT JS*/
    }
};

test_datatable = $.extend(datatable, test_datatable);

$(function() {
    datatable.test_datatable();
});