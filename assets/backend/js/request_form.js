var request_form = {
    form: '#requisition-form',
    btn_submit: '#requisition-form [type=submit]',
    request_form: function () {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        /*END INIT PARENT JS*/

        //On click 'add more item' button
        $("body").on('click', '[data-name=add-more-item]', function (e) {
            e.preventDefault();
            self.load_row_item($(self.form + ' tbody tr').length + 1);
        });
        //On click 'remove item' button
        $("body").on('click', '[data-name=remove-item]', function (e) {
            e.preventDefault();
            $(this).parents('tr').remove(); //remove 'selected row item'

            //reset 'row item counter'
            $(self.form + ' tbody tr').each(function (index) {
                $counter = (parseInt(index) + 1);
                $(this).find('[data-for=item-counter]').html($counter + '.');
            });
        });
        //On change 'type' option
        $("body").on('change', '[name="item_type[]"]', function (e) {
            e.preventDefault();
            $tr = $(this).parents('tr');
            $item_qty = $tr.find('[name="item_qty[]"]');
            //Remove class 'error|success'
            $form_group = $tr.find('.form-group');
            $form_group.removeClass("has-feedback has-success has-warning has-error");
            $form_group.find(".text-danger").remove();
            //SHOW or HIDE 'item_qty'
            if ($(this).val().match(/^MATERIAL$/i)) {
                $item_qty.show();
            } else {
                //$item_qty.hide();
            }
        });
        //On submit 'requisition-form'
        $("body").on('submit', self.form, function (e) {
            e.preventDefault();
            $data = {};
            $(self.form + ' tbody tr').each(function (index) {
                $data[index + 1] = {
                    'item_type': $(this).find('[name="item_type[]"]').val(),
                    'item_description': $(this).find('[name="item_description[]"]').val(),
                    'item_qty': $(this).find('[name="item_qty[]"]').val()
                };
            });
            self.add($data);
        });

        //$("#modal_suggest_after_add_request").modal('show');
        //$("#modal-proceed-with-items").modal('show');
        //self.load_items_table(1);
    },
    load_row_item: function (item_counter) {
        var self = this;
        $.ajax({
            url: class_url + 'load_row_item/' + item_counter,
            type: 'post',
            dataType: 'json',
            async: true,
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                $(self.form + ' tbody').append(json.row_item);
            }
        });
    },
    load_items_table: function (request_number) {
        var self = this;
        $modal_id = '#modal-proceed-with-items';
        $.ajax({
            url: class_url + 'load_items_table',
            type: 'post',
            data: {
                request_number: request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                $($modal_id + ' .modal-body').html('');
                $($modal_id + ' .modal-body').find('.alert').remove();
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                if (json.status.match(/^success$/i)) {
                    $($modal_id + ' .modal-body').html(json.data);
                } else {
                    self.alert_msg({type: 'error', dom_container: $($modal_id + ' .modal-body'), msg: json.msg, fill_type: 'prepend'});
                }
            }
        });
    },
    add: function (data) {
        var self = this;
        //process Form POST data
        $.ajax({
            url: class_url + 'add',
            type: 'post',
            data: {
                items: data
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                $(self.btn_submit).button('loading'); //loading button submit
                $(self.form).find(".alert").remove(); //remove all alert msg in form

                //remove all form-group class
                $form_group = $(self.form).find(".form-group");
                $form_group.removeClass("has-feedback has-success has-warning has-error");
                $form_group.find(".text-danger").remove();
            },
            error: function (request) {
                $(self.btn_submit).button('reset');
                self.alert_msg({type: 'error', dom_container: $(self.form), msg: request.responseText, fill_type: 'prepend'}); //console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line

                $(self.btn_submit).button('reset');
                if (json.status.match(/^success$/i)) {
                    $(self.form + ' tbody tr').remove(); //clear all tr
                    self.load_row_item(1); //add 1 one item

                    //show model PROCEED
                    //$("#modal-proceed-with-items").modal('show');
                    //self.load_items_table(json.request_number);
                    $("[data-name=link-submitted-requisition]").attr('href', class_url + 'detail/' + json.request_number);
                    $("#modal_suggest_after_add_request").modal('show');

                } else if (typeof (json.form_error) !== "undefined") {//Show form error per tr
                    for (var item_key in json.form_error) {
                        $tr = $(self.form + ' tbody tr:nth-child(' + item_key + ')');
                        for (var field_key in json.form_error[item_key]) {
                            $form_group = $tr.find('.form-group[for=' + field_key + ']');
                            if (json.form_error[item_key][field_key] == "") { //if field SUCCESS
                                $form_group.addClass("has-feedback has-success");
                            } else {//if field ERROR
                                $form_group.addClass("has-feedback has-error");
                                $form_group.append('<span class="text-danger">' + json.form_error[item_key][field_key] + '</span>');
                            }
                        }
                    }
                }

                //show message
                if (typeof (json.msg) !== "undefined" && json.status.match(/^error$/i)) {
                    self.alert_msg({type: json.status, dom_container: $(self.form), msg: json.msg, fill_type: 'prepend'}); //error msg
                }
            }
        });
    }
};


request_form = $.extend(site, request_form);
$(function () {
    site.request_form();
});