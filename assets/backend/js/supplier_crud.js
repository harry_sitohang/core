var supplier_crud = {
    supplier_crud: function () {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        if (typeof (crud) !== "undefined" && enable_crud == true) {
            self.crud();
        }
        /*END INIT PARENT JS*/

        //set tag input
        $('.tags').tagsInput({
            width: 'auto',
            //'autocomplete': {'view': 'view', 'edit': 'edit', 'add': 'add', 'remove': 'remove'}
        });

    }
};

supplier_crud = $.extend(crud, supplier_crud);

$(function () {
    crud.supplier_crud();
});