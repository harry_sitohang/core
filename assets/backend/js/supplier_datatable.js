var supplier_datatable = {
    supplier_datatable: function() {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        if (typeof (datatable) !== "undefined" && enable_datatable == true) {
            self.datatable();
        }
        /*END INIT PARENT JS*/
    }
};

supplier_datatable = $.extend(datatable, supplier_datatable);

$(function() {
    datatable.supplier_datatable();
});