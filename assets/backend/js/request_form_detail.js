var request_form_detail = {
    tab_requisition: '#tab_requisition', //tab
    tab_quotation: '#tab_quot', //tab
    tab_po: '#tab_po', //tab
    tab_do: '#tab_do', //tab
    box_table_requsition: '#tab_requisition #box-table-requisition', //tab > portlet-body
    box_table_quotation: '#tab_quot #box-table-quotation', //tab > portlet-body
    box_table_po: '#tab_po #box-table-po', //tab > portlet-body
    box_table_do: '#tab_do #box-table-do', //tab > portlet-body
    table_requisition: '#tab_requisition table#list-items', //tab > portlet-body > table
    table_quotation: '#tab_quot table#table-quotation', //tab > portlet-body > table
    table_po: '#tab_po table#table-po', //tab > portlet-body > table
    table_do: '#tab_do table#table-do', //tab > portlet-body > table
    proceed_requisiton_btn: "#tab_requisition [data-name=proceed-request]", //portlet-body > proceed-request 
    request_form_detail: function () {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        /*END INIT PARENT JS*/

        self.requisition_form_init(); //form requisition tab
        self.quotation_tab_init();
        self.po_tab_init();
        self.do_tab_init();
    },
    requisition_form_init: function () {
        var self = this;

        //On Click tab
        $('body').on('click', "ul.nav-tabs li a[data-toggle=tab]", function () {
            window.location.hash = $(this).attr('href');
        });

        //Check on Load body, if hash exist determine which tab tobe opened
        if (window.location.hash != '') {
            $("ul.nav-tabs li a[data-toggle=tab][href=" + window.location.hash + "]").trigger('click');
        }

        //On change 'type' option
        $("body").on('change', self.tab_requisition + ' select[name=item_type]', function (e) {
            e.preventDefault();
            $tr = $(this).parents('tr');
            $item_qty = $tr.find('[name="item_qty"]');
            //Remove class 'error|success'
            $form_group = $tr.find('.form-group');
            $form_group.removeClass("has-feedback has-success has-warning has-error");
            $form_group.find(".text-danger").remove();
            //SHOW or HIDE 'item_qty'
            if ($(this).val().match(/^MATERIAL$/i)) {
                $item_qty.show();
            } else {
                //$item_qty.hide();
            }
        });

        //On Click update item
        $("body").on('click', self.tab_requisition + ' [data-name=update-item]', function (e) {
            e.preventDefault();
            $tr = $(this).parents('tr');
            self.update_per_item($tr);
        });

        //On keyup 13 input qty
        $("body").on('keyup', self.tab_requisition + ' [name=item_qty]', function (e) {
            e.preventDefault();
            if (e.keyCode == 13) {
                $tr = $(this).parents('tr');
                if ($tr.find('[data-name=update-item]').is(':visible') == true) {
                    self.update_per_item($tr);
                } else if ($tr.find('[data-name=add-item]').is(':visible') == true) {
                    self.add_per_item($tr);
                }
            }
        });

        //On click remove item
        $("body").on('click', self.tab_requisition + ' [data-name=remove-item]', function (e) {
            e.preventDefault();
            if (confirm("Are you sure want to delete this item ?")) {
                $tr = $(this).parents('tr');
                self.delete_per_item($tr);
            }
        });

        //On Click add more item
        $("body").on('click', self.tab_requisition + ' [data-name=add-more-item]', function (e) {
            e.preventDefault();
            $count_items = $(self.tab_requisition + ' tbody tr').length;
            $(self.tab_requisition + ' tbody').append(''.concat('<tr>',
                    '<td data-for="item-counter">' + ($count_items + 1) + '.</td>',
                    '<td>',
                    '<div class="form-group" for="item_type">',
                    '<select name="item_type" class="form-control">',
                    '<option value="0" selected="selected">SELECT</option>',
                    '<option value="MATERIAL" >MATERIAL</option>',
                    '<option value="SERVICE">SERVICE</option>',
                    '</select>',
                    '</div>',
                    '</td>',
                    '<td>',
                    '<div class="form-group" for="item_description">',
                    '<textarea name="item_description" class="form-control"></textarea>',
                    '</div>',
                    '</td>',
                    '<td>',
                    '<div class="form-group" for="item_qty">',
                    '<input type="text" class="form-control input-small" name="item_qty" value="">',
                    '</div>',
                    '</td>',
                    '<td><a href="javascript:;">Quotation has not been set</a></td>',
                    '<td><a href="javascript:;">Price has not been set</a></td>',
                    '<td>',
                    '<a href="javascript:;" class="btn blue btn-sm" data-name="add-item" data-loading-text="loading..."><i class="fa fa-plus"></i> Add</a>',
                    '<a href="javascript:;" class="btn green btn-sm" data-name="update-item" data-loading-text="loading..." style="display:none;"><i class="fa fa-check"></i> Save</a>',
                    '<a href="javascript:;" class="btn red btn-sm" data-name="remove-item" data-loading-text="loading..." style="display:none;"><i class="fa fa-remove"></i> Remove</a>',
                    '</td>',
                    '</tr>'));
        });

        //On click add item
        $("body").on('click', self.tab_requisition + ' [data-name=add-item]', function (e) {
            e.preventDefault();
            $tr = $(this).parents('tr');
            self.add_per_item($tr);
        });

        //On click proceed request
        $("body").on('click', self.proceed_requisiton_btn, function (e) {
            e.preventDefault();
            if (confirm('Are you sure want to proceed this requisition ?')) {
                self.approved_requisition_by_department($(this));
            }
        });

        //On click approve request
        $("body").on('click', self.tab_requisition + ' [data-name=approve-request]', function (e) {
            e.preventDefault();
            if (confirm('Are you sure want to sign/approve this requisition ?')) {
                self.approved_requisition($(this));
            }
        });

        //On click unlock request
        $("body").on('click', self.tab_requisition + ' [data-name=unlock-request]', function (e) {
            e.preventDefault();
            if (confirm('Are you sure want to unlock this requisition ?  This requisition will not be avaliable for you.')) {
                self.unlock_requisition($(this));
            }
        });
    },
    update_per_item: function (tr) {
        var self = this;
        form = tr;
        btn_submit = tr.find('[data-name=update-item]');

        //process Form POST data
        $.ajax({
            url: class_url + 'update_per_item',
            type: 'post',
            data: {
                item_id: tr.find('[data-name=update-item]').attr('data-item-id'),
                item_type: tr.find('[name=item_type]').val(),
                item_description: tr.find('[name=item_description]').val(),
                item_qty: tr.find('[name=item_qty]').val()
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                $(btn_submit).button('loading'); //loading button submit
                $(form).find(".alert").remove(); //remove all alert msg in form

                //remove all form-group class
                $form_group = $(form).find(".form-group");
                $form_group.removeClass("has-feedback has-success has-warning has-error");
                $form_group.find(".text-danger").remove();
            },
            error: function (request) {
                $(btn_submit).button('reset');
                self.alert_msg({type: 'error', dom_container: $(tr).find('td:last'), msg: request.responseText, fill_type: 'append'}); //console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');

                if (json.status.match(/^success$/i)) {
                    self.alert_msg({type: json.status, dom_container: $(tr).find('td:last'), msg: json.msg, fill_type: 'append'}); //success msg
                } else if (typeof (json.form_error) !== "undefined") {//Show form error per tr
                    for (var field_key in json.form_error) {
                        $form_group = tr.find('.form-group[for=' + field_key + ']');
                        if (json.form_error[field_key] == "") { //if field SUCCESS
                            $form_group.addClass("has-feedback has-success");
                        } else {//if field ERROR
                            $form_group.addClass("has-feedback has-error");
                            $form_group.append(json.form_error[field_key]);
                        }
                    }
                }
            }
        });
    },
    delete_per_item: function (tr) {
        var self = this;
        form = tr;
        btn_submit = tr.find('[data-name=remove-item]');

        //process Form POST data
        $.ajax({
            url: class_url + 'delete_per_item',
            type: 'post',
            data: {
                item_id: tr.find('[data-name=update-item]').attr('data-item-id')
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                $(btn_submit).button('loading'); //loading button submit
                $(form).find(".alert").remove(); //remove all alert msg in form
            },
            error: function (request) {
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');
                if (json.status.match(/^success$/i)) {
                    //hide button
                    tr.find('[data-name=update-item], [data-name=remove-item]').hide();

                    setTimeout(function () {
                        tr.remove(); //remove selected tr

                        //badge length
                        $('[data-name=count-items]').html($(self.tab_requisition + " tbody tr").length);

                        //reset 'row item counter'
                        $(self.table_requisition + ' tbody tr').each(function (index) {
                            $counter = (parseInt(index) + 1);
                            $(this).find('[data-for=item-counter]').html($counter + '.');
                        });
                    }, 500);
                }
                self.alert_msg({type: json.status, dom_container: $(tr).find('td:last'), msg: json.msg, fill_type: 'append'}); //error|success msg
            }
        });
    },
    add_per_item: function (tr) {
        var self = this;
        form = tr;
        btn_submit = tr.find('[data-name=add-item]');

        //process Form POST data
        $.ajax({
            url: class_url + 'add_per_item',
            type: 'post',
            data: {
                request_number: $(self.table_requisition).attr('data-request-number'),
                item_type: tr.find('[name=item_type]').val(),
                item_description: tr.find('[name=item_description]').val(),
                item_qty: tr.find('[name=item_qty]').val()
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                $(btn_submit).button('loading'); //loading button submit
                $(form).find(".alert").remove(); //remove all alert msg in form

                //remove all form-group class
                $form_group = $(form).find(".form-group");
                $form_group.removeClass("has-feedback has-success has-warning has-error");
                $form_group.find(".text-danger").remove();
            },
            error: function (request) {
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');

                if (json.status.match(/^success$/i)) {
                    self.alert_msg({type: json.status, dom_container: $(tr).find('td:last'), msg: json.msg, fill_type: 'append'}); //success msg

                    //set badge length
                    $('[data-name=count-items]').html($(self.tab_requisition + " tbody tr").length);

                    //hide|show button
                    $(btn_submit).hide();
                    tr.find('[data-name=update-item], [data-name=remove-item]').attr('data-item-id', json.item_id);
                    tr.find('[data-name=update-item], [data-name=remove-item]').show();
                } else if (typeof (json.form_error) !== "undefined") {//Show form error per tr
                    for (var field_key in json.form_error) {
                        $form_group = tr.find('.form-group[for=' + field_key + ']');
                        if (json.form_error[field_key] == "") { //if field SUCCESS
                            $form_group.addClass("has-feedback has-success");
                        } else {//if field ERROR
                            $form_group.addClass("has-feedback has-error");
                            $form_group.append(json.form_error[field_key]);
                        }
                    }
                }
            }
        });
    },
    approved_requisition_by_department: function (btn_submit) {
        var self = this;
        $request_number = btn_submit.attr('data-request-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'approved_requisition_by_department',
            type: 'post',
            data: {
                request_number: $request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.box_table_requsition));
                $(btn_submit).button('loading'); //loading button submit
                $(self.box_table_requsition).find(".alert").remove(); //remove all alert msg in form
            },
            error: function (request) {
                self.unblockUI_elm($(self.box_table_requsition));
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.unblockUI_elm($(self.box_table_requsition));
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');
                self.alert_msg({type: json.status, dom_container: $(self.box_table_requsition), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    //remove approve btn
                    btn_submit.remove();
                    $("[data-name=add-more-item]").remove();

                    //$(".actions-requisition").html(json.btn_approved);
                    $(self.tab_requisition + " [data-name=box-current-lock-position]").html(json.current_lock_position); //update lock position info in tab requisition, in request details box
                    //
                    //remove td for the last column 
                    $(self.table_requisition + ' thead tr th:last').remove();
                    $(self.table_requisition + ' tbody tr').each(function () {
                        //remove the last column
                        $(this).find('td:last').remove();
                        //disable input
                        $(this).find(".form-group").each(function () {
                            $td = $(this).parent('td');
                            $field_val = $('[name=' + $(this).attr("for") + ']').val();
                            $td.html($field_val);
                        });
                    });
                }
            }
        });
    },
    approved_requisition: function (btn_submit) {
        var self = this;
        $request_number = btn_submit.attr('data-request-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'approved_requisition',
            type: 'post',
            data: {
                request_number: $request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.box_table_requsition));
                $(btn_submit).button('loading'); //loading button submit
                $(self.box_table_requsition).find(".alert").remove(); //remove all alert msg in form
            },
            error: function (request) {
                self.unblockUI_elm($(self.box_table_requsition));
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.unblockUI_elm($(self.box_table_requsition));
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');
                self.alert_msg({type: json.status, dom_container: $(self.box_table_requsition), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    $(self.tab_requisition + " [data-name=box-current-lock-position]").html(json.current_lock_position); //update lock position info in tab requisition, in request details box
                    $(".actions-requisition").html(json.btn_approved);

                    //add button to view pdf "MATERIAL REQUISITION FORM"
                    $(".actions-requisition").html('<a href="' + class_url + 'pdf/material_request/' + $request_number + '" class="btn btn-info btn-sm" target="_blank">View Requisition Form</a>');
                }
            }
        });
    },
    unlock_requisition: function (btn_submit) {
        var self = this;
        $request_number = btn_submit.attr('data-request-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'unlock_requisition',
            type: 'post',
            data: {
                request_number: $request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.box_table_requsition));
                $(btn_submit).button('loading'); //loading button submit
                $(self.box_table_requsition).find(".alert").remove(); //remove all alert msg in form
            },
            error: function (request) {
                self.unblockUI_elm($(self.box_table_requsition));
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.unblockUI_elm($(self.box_table_requsition));
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');
                self.alert_msg({type: json.status, dom_container: $(self.box_table_requsition), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    $(self.box_table_requsition).html('<h4 class="alert alert-warning text-center">You will be redirect to page : List Request</h4>');
                    setTimeout(function () {
                        document.location = class_url;
                    }, 500);
                }
            }
        });
    },
    quotation_tab_init: function () {
        var self = this;

        //set tag input
        $('.tags').tagsInput({
            width: 'auto',
            //'autocomplete': {'view': 'view', 'edit': 'edit', 'add': 'add', 'remove': 'remove'}
        });

        //$("#modal-list-supplier").modal('show');

        //On click link 'approve-quotation'
        $("body").on('click', self.tab_quotation + ' [data-name=approve-quotation]', function (e) {
            e.preventDefault();
            self.approved_quotation($(this));
        });

        //On click link 'select-supplier'
        $("body").on('click', self.tab_quotation + ' [data-name=select-supplier]', function (e) {
            e.preventDefault();
            request_form_detail.selected_select_supplier_elm = $(this);
            //self.hide_selected_supplier();
            $("#modal-list-supplier").modal('show');
        });

        //On submit #form-search-supplier in modal #modal-list-supplier
        $("body").on('submit', self.tab_quotation + ' #form-search-supplier', function (e) {
            e.preventDefault();
            $formData = new FormData($(this)[0]);
            self.search_supplier($formData);
        });

        //On click 'set-supplier' in modal #modal-list-supplier
        $("#modal-list-supplier").on('click', '[data-name=set-supplier]', function (e) {
            e.preventDefault();
            //var modal_supplier = '#modal-list-supplier';
            $supplier_id = $(this).attr('data-supplier-id');
            $supplier_name = $(this).attr('data-supplier-name');
            //$tr = $(self.table_quotation + " tbody tr:nth-child(" + $(modal_supplier).attr('data-row') + ")");
            $form_group = request_form_detail.selected_select_supplier_elm.parent('.form-group');

            //reset form search supplier
            //var form = '#form-search-supplier';
            //$(form + ' select[name=search_supplier_by]').val(0);
            //$(form + ' input[name=search_supplier_keyword]').val('');

            //set select_supplier elm data
            $form_group.find('input[name="supplier_id[]"]').val($supplier_id);//$tr.find('input[name=supplier_id]').val($supplier_id);
            $form_group.find('[data-name=select-supplier]').html($supplier_name);//$tr.find('[data-name=select-supplier]').html($supplier_name);

            //hide modal
            $("#modal-list-supplier").modal('hide');

            self.hide_selected_supplier();
        });

        //On submit add supplier form in modal : #modal-list-supplier
        $("#modal-list-supplier").on('submit', 'form#add-supplier-form', function (e) {
            e.preventDefault();
            $formData = new FormData($(this)[0]);
            self.add_supplier($formData);
        });


        //On Click add supplier to group quotation
        $(self.tab_quotation).on('click', '[data-name=add-supplier-to-group-quotation]', function (e) {
            e.preventDefault();

            //update td column supplier in quotation list table
            $(this).parent('td').find('.box-list-supplier').append(''.concat('<div class="form-group" data-for="supplier">',
                    '<a href="javascript:;" class="btn btn-sm" data-name="select-supplier">Select Supplier</a>',
                    '<input type="hidden" name="supplier_id[]">',
                    //'<input type="file" class="" name="quotation_file[]" style="display: inline-block;">',
                    ' <a href="javascript:;" class="btn btn-sm btn-default" data-name="remove-supplier-from-group-quotation"><i class="fa fa-remove"></i></a>',
                    '</div>'));

        });

        //On Click remove supplier from group quotation
        $(self.tab_quotation).on('click', '[data-name=remove-supplier-from-group-quotation]', function (e) {
            e.preventDefault();
            $(this).parent('.form-group').remove();
        });

        //on click add group quotation
        $(self.tab_quotation).on('click', '[name=add-quotation]', function (e) {
            e.preventDefault();
            $err_found = false;

            //First clear table
            $("#table-add-group-quotation tbody").html('');

            //add row in #table-add-group-quotation
            $tr = $(this).parents('tr');
            request_form_detail.selected_row = $tr;

            $form_group = $tr.find('.box-list-supplier .form-group[data-for=supplier]');
            $form_group.each(function () {
                if (typeof ($(this).find('input[name="supplier_id[]"]')) === "undefined" || !$(this).find('input[name="supplier_id[]"]').val()) {
                    $err_found = true;
                    alert("Please provide supplier data");
                    return false;
                } else {

                    //loop for supplier
                    $supplier_id = $(this).find('input[name="supplier_id[]"]').val();
                    $supplier_name = $(this).find('[data-name=select-supplier]').html();
                    $("#table-add-group-quotation tbody").append(''.concat('<tr data-supplier-id="' + $supplier_id + '">',
                            '<td data-for="supplier_id">',
                            $supplier_name + '<input type="hidden" name="supplier_id[]" value="' + $supplier_id + '">',
                            '</td>',
                            '<td data-for="supplier-quotation-file">',
                            '<input type="file" class="" name="quotation_file_' + $supplier_id + '" style="display: inline-block;">',
                            '</td>',
                            '<td data-for="items">',
                            '</td>',
                            '</tr>'));

                    //loop for items here
                    $items = $tr.find('td[data-for="items"] input[type=checkbox][name="set-items"]:checked');
                    if ($items.length > 0) {
                        //generate ul selected items
                        $ul = '<ul class="list-unstyled">';
                        $items.each(function () {
                            $ul = $ul.concat('<li>',
                                    $(this).attr('data-item-description'),
                                    '<input type="text" class="input-small form-control" name="item_price[' + $supplier_id + '][' + $(this).val() + ']" data-item-id="' + $(this).val() + '" placeholder="IDR">',
                                    '</li>');
                        });
                        $ul += '</ul>';

                        $('#table-add-group-quotation tbody tr[data-supplier-id="' + $supplier_id + '"] td:last').html($ul);

                    } else {
                        alert('Please select items');
                        $err_found = true;
                    }
                }
            });


            if ($err_found == false) {
                $("#modal-add-grooup-quotation").modal('show');
            } else {
                $("#modal-add-grooup-quotation").modal('hide');
                $("#table-add-group-quotation tbody").html(''); //Clear table
            }

        });

        //On Submit Form #form-add-group-quotation
        $(self.tab_quotation).on('submit', '#form-add-group-quotation', function (e) {
            e.preventDefault();
            $formData = new FormData($(this)[0]);
            e.preventDefault();
            self.add_group_quotation($formData);
        });

        //On click save 'group quotation' modal
        $(self.tab_quotation).on('click', "[data-name=save-group-quotation]", function (e) {
            $(self.tab_quotation + " #form-add-group-quotation").trigger('submit');
        });

        //On click btn 'add group quotation'
        $(self.tab_quotation).on('click', "[data-name=add-group-quotation]", function (e) {
            e.preventDefault();
            self.add_group_quotation_row($(this).attr('data-request-number'));
        });

        //On click checkbox 'set-items' on quotation table
        $(self.tab_quotation).on('click', "[name=set-items]", function (e) {
            $elm = $(this);
            $checked = $(this).is(':checked');
            $val = $(this).attr('value');
            $desc = $(this).attr('data-item-description');
            $type = $(this).attr('data-item-type');
            $current_ul = $(this).parents('ul');

            $tbody = $(self.tab_quotation + ' table#table-quotation tbody');
            $ul = $tbody.find('tr td[data-for=items] ul');


            //check in group quot must be contain only material or only service

            if ($checked && ($type == 'MATERIAL' && $current_ul.find('[name=set-items][data-item-type="SERVICE"]:checked').length != 0) || ($type == 'SERVICE' && $current_ul.find('[name=set-items][data-item-type="MATERIAL"]:checked').length != 0)) {
                alert("You cannot combine SERVICE and MATERIAL in one group");
                $elm.parent("span.checked").removeClass('checked');
                $elm.prop('checked', false);
            } else {
                //hide show checkbox in another tr
                $ul.each(function () {
                    $tr = $(this).parents('tr');
                    if ($tr.attr('data-status') != 'added') { //not saved in database yet
                        $checkbox = $(this).find('input[type=checkbox][value=' + $val + ']');
                        if ($checked && !$checkbox.is(':checked')) {
                            $checkbox.parents('li').remove();
                        } else if (!$checked && $checkbox.length == 0) {
                            $(this).append('<li><input type="checkbox" value="' + $val + '" name="set-items" data-item-description="' + $desc + '" data-item-type="' + $type + '"> ' + $desc + ' : ' + $type + '</li>');
                        }

                        if ($(this).find('li').length == 0) {
                            $(this).parents('tr').remove();
                        }
                    }
                });
            }
        });

        //on click button remove group quotation 
        $(self.tab_quotation).on('click', "[name=remove-quotation]", function (e) {
            e.preventDefault();
            if ($(this).attr('data-status') != 'added') {
                self.remove_tr_from_table_quot($(this)); //Only remove tr, not saved in DB yet
            } else {
                self.remove_group_quotation_row($(this).attr('data-request-number'), $(this).attr('data-group-number'), $(this)); //Remove data from DB then remove tr
            }
        });

        //On click unlock quotation, will be delete all quotation and CAPEX number
        $(self.tab_quotation).on('click', '[data-name=unlock-quotation]', function (e) {
            e.preventDefault();
            if (confirm('Are you sure want to unlock this requisition ?  All quotation and CAPEX data will be deleted.')) {
                self.unlock_quotation($(this));
            }
        });

        //Onclick 'view-capex' show modal capex
        $(self.tab_quotation).on('click', '[data-name=view-capex]', function (e) {
            e.preventDefault();
            $("#modal-capex").modal('show');
        });

        //Onchange dropdown 'set-capex' YES|NO
        $(self.tab_quotation).on('change', 'select[name=set-capex]', function (e) {
            e.preventDefault();
            self.update_capex_data($(this));
        });
    },
    approved_quotation: function (btn_submit) {
        var self = this;
        $request_number = btn_submit.attr('data-request-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'approved_requisition',
            type: 'post',
            data: {
                request_number: $request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.box_table_quotation));
                $(btn_submit).button('loading'); //loading button submit
                $(self.box_table_quotation).find('.alert').remove();
            },
            error: function (request) {
                self.unblockUI_elm($(self.box_table_quotation));
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.unblockUI_elm($(self.box_table_quotation));
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');

                self.alert_msg({type: json.status, dom_container: $(self.box_table_quotation), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    //reload quotation table
                    $(self.tab_requisition + " [data-name=box-current-lock-position]").html(json.current_lock_position); //update lock position info in tab requisition, in request details box

                    //update table quotation
                    $(self.box_table_quotation + ' thead tr').each(function () {
                        $(this).find('th:last-child').remove();
                    });
                    $(self.box_table_quotation + ' tbody tr').each(function () {
                        $(this).find('td:last-child').remove(); //remove last td
                        if ($(this).attr('data-status') != 'added') {
                            $(this).remove();
                        }
                    });

                    //remove btn 'add-group-quotation' & Approved & Lock
                    $(self.tab_quotation + " [data-name=add-group-quotation]").remove();
                    $(self.tab_quotation + " [data-name=approve-quotation]").remove();
                    $(self.tab_quotation + " [data-name=unlock-quotation]").remove();

                    //Update CAPEX modal here
                    $("[data-name=view-capex]").html('View Capex');
                    $("#modal-capex .modal-body table tbody tr").each(function (index) {
                        $td = $(this).find('td:last-child');
                        $td.find("select[name=set-capex]").remove();
                        if ($td.find('[data-name=view-capex-attachment]').length == 0) {
                            $td.html('-');
                        }
                    });
                }
            }
        });
    },
    remove_tr_from_table_quot: function (elm) {
        var self = this;
        $removal_tr = elm.parents('tr');
        $items = $removal_tr.find('td[data-for=items] input[type=checkbox][name=set-items]');
        $items.each(function () {
            if ($(this).is(':checked')) {
                $val = $(this).attr('value');
                $desc = $(this).attr('data-item-description');

                $tbody = $(self.tab_quotation + ' table#table-quotation tbody');
                $ul = $tbody.find('tr td[data-for=items] ul');

                //Add checked checkbox to every row except 'row with attr data-status:added'
                $ul.each(function () {
                    $tr = $(this).parents('tr');
                    if ($tr.attr('data-status') != 'added') { //not saved in database yet
                        $checkbox = $(this).find('input[type=checkbox][value=' + $val + ']');
                        if ($checkbox.length == 0) {
                            $(this).append('<li><input type="checkbox" value="' + $val + '" name="set-items" data-item-description="' + $desc + '"> ' + $desc + '</li>');
                        }
                    }
                });
            }
        });

        $removal_tr.remove(); //remove tr
        self.reset_tr_number(); //reset tr number
    },
    reset_tr_number: function () {
        var self = this;
        $tr = $(self.tab_quotation + ' table#table-quotation tbody tr[data-name=quotion-group]');
        $tr.each(function (index) {
            //console.log(index);
            $td = $(this).find('td:first-child');
            $td.html((index + 1) + '.');
        });
    },
    remove_group_quotation_row: function (request_number, group_number, elm) {
        var self = this;
        $.ajax({
            url: class_url + 'remove_group_quotation_row',
            type: 'post',
            data: {
                request_number: request_number,
                group_number: group_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                if (json.status.match(/^success$/i)) {
                    self.remove_tr_from_table_quot(elm); //remove tr from quot table
                    $('[data-name=count-quots]').html($(self.table_quotation + ' tbody tr[data-status=added]').length); //update quonts quotations
                } else {
                    alert(json.msg);
                }
            }
        });
    },
    add_group_quotation_row: function (request_number) {
        var self = this;
        $.ajax({
            url: class_url + 'get_list_request_items',
            type: 'post',
            data: {
                request_number: request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                $tbody = $(self.tab_quotation + ' table#table-quotation tbody');
                $tr = $tbody.find('tr');
                $tr_length = $tr.length + 1;
                $list_li = '';
                for (var key in json) {
                    $checked_item = $tr.find('input[type=checkbox][value=' + key + ']:checked');
                    if ($checked_item.length == 0) {
                        $list_li += '<li><input type="checkbox" value="' + key + '" name="set-items" data-item-description="' + json[key]['desc'] + '" data-item-type="' + json[key]['type'] + '"> ' + json[key]['desc'] + ' : ' + json[key]['type'] + '</li>';
                    }
                }

                if (!$list_li) {
                    alert('There is no item left.');
                } else {
                    $($tbody).append(''.concat('<tr data-name="quotion-group" data-group-number="">',
                            '<td>' + $tr_length + '.</td>',
                            '<td data-for="list-supplier">',
                            '<div class="box-list-supplier">',
                            '<div class="form-group" data-for="supplier">',
                            '<a href="javascript:;" class="btn btn-sm" data-name="select-supplier">Select Supplier</a>',
                            '<input type="hidden" name="supplier_id[]">',
                            '<a href="javascript:;" class="btn btn-sm btn-default" data-name="remove-supplier-from-group-quotation"><i class="fa fa-remove"></i></a>',
                            '</div>',
                            '</div>',
                            '<a href="javascript:;" class="btn btn-sm btn-default" data-name="add-supplier-to-group-quotation"><i class="fa fa-plus-square-o"></i> add supplier</a>',
                            '</td>',
                            '<td data-for="items">',
                            '<ul class="list-unstyled">' + $list_li + '</ul>',
                            '</td>',
                            '<td>',
                            '<a href="javascript:;" data-loading-text="Loading..." name="add-quotation" class="btn btn-success btn-sm"><i class="fa fa-save"></i>&nbsp;&nbsp;save&nbsp;</a> ',
                            '<a href="javascript:;" data-loading-text="Loading..." name="remove-quotation" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp;&nbsp;remove&nbsp;</a>',
                            '</td>',
                            '</tr>'));
                }
            }
        });
    },
    add_group_quotation: function ($formData) {
        var self = this;
        $.ajax({
            url: class_url + 'add_group_quotation',
            type: 'post',
            data: $formData,
            dataType: 'json',
            async: true,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function () {
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                if (json.status.match(/^error$/i)) {
                    alert("Please provide all data.");
                } else {
                    $("#modal-add-grooup-quotation").modal('hide');
                    $("#table-add-group-quotation tbody").html(''); //Clear table
                    request_form_detail.selected_row.attr('data-group-number', json.group_number);
                    request_form_detail.selected_row.attr('data-status', 'added');
                    request_form_detail.selected_row.html(json.tr);

                    self.reset_tr_number(); //reset tr number

                    $('[data-name=count-quots]').html($(self.table_quotation + ' tbody tr[data-status=added]').length); //update quonts quotations
                }
            }
        });
    },
    update_tr_quot_to_status_added: function () {

    },
    search_supplier: function (data) {
        var self = this;
        var form = '#form-search-supplier';
        var modal_body = self.tab_quotation + ' #modal-list-supplier .modal-body';

        //process Form POST data
        $.ajax({
            url: class_url + 'search_supplier',
            type: 'post',
            data: data,
            dataType: 'json',
            async: true,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function () {
                $(form).find(".alert").remove(); //remove all alert msg in form
                self.blockUI_elm($(modal_body));

                //remove all form-group class
                $form_group = $(form).find(".form-group");
                $form_group.removeClass("has-feedback has-success has-warning has-error");
                $form_group.find(".form-control-feedback").remove();
                $controls = $form_group.find(".controls");
                $controls.find(".text-danger").remove();
            },
            error: function (request) {
                self.unblockUI_elm($(modal_body));
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line

                self.unblockUI_elm($(modal_body));
                if (json.status.match(/^success$/i)) {
                    $("#search-supplier-result").html(json.result);
                    self.hide_selected_supplier();
                } else {
                    //self.alert_msg({type: json.status, dom_container: $(form), msg: json.msg, fill_type: 'prepend'}); //error msg
                    for (var field in json.form_error) {
                        $form_group = $(form).find(".form-group[for=" + field + "]");
                        $controls = $form_group.find(".controls");
                        if (json.form_error[field] == "") { //if field SUCCESS
                            $form_group.addClass("has-feedback has-success");
                            $form_group.append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
                        } else {//if field ERROR
                            $form_group.addClass("has-feedback has-error");
                            $form_group.append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                            $controls.append(json.form_error[field]);
                        }
                    }
                }
            }
        });
    },
    hide_selected_supplier: function () {
//        $("#search-supplier-result tbody tr").show();
//        $('.box-list-supplier [data-for=supplier] input[name="supplier_id[]"]').each(function () {
//            if ($(this).val() == true) {
//                $hide_supplier = $("#search-supplier-result [data-name=set-supplier][data-supplier-id=" + $(this).val() + "]");
//                if (typeof ($hide_supplier) !== "undefined") {
//                    $hide_supplier.parents('tr').hide();
//                }
//            }
//        });
//        $("#search-supplier-result .alert").html($("#search-supplier-result tbody tr:visible").length + ' supplier founds');
    },
    add_supplier: function (data) {
        var self = this;
        var crud_form = self.tab_quotation + ' form#add-supplier-form';
        var crud_btn_submit = crud_form + ' [type=submit]';

        //process Form POST data
        $.ajax({
            url: template_url + 'supplier/add',
            type: 'post',
            data: data,
            dataType: 'json',
            async: true,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function () {
                $(crud_btn_submit).button('loading'); //loading button submit
                $(crud_form).find(".alert").remove(); //remove all alert msg in form

                //remove all form-group class
                $form_group = $(crud_form).find(".form-group");
                $form_group.removeClass("has-feedback has-success has-warning has-error");
                $form_group.find(".form-control-feedback").remove();
                $controls = $form_group.find(".controls");
                $controls.find(".text-danger").remove();
            },
            error: function (request) {
                $(crud_btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line

                $(crud_btn_submit).button('reset');
                if (json.status.match(/^success$/i)) {
                    self.alert_msg({type: json.status, dom_container: $(crud_form), msg: json.msg, fill_type: 'prepend'}); //success msg
                } else {
                    //self.alert_msg({type: json.status, dom_container: $(crud_form), msg: json.msg, fill_type: 'prepend'}); //error msg
                    for (var field in json.form_error) {
                        $form_group = $(crud_form).find(".form-group[for=" + field + "]");
                        $controls = $form_group.find(".controls");
                        if (json.form_error[field] == "") { //if field SUCCESS
                            $form_group.addClass("has-feedback has-success");
                            $form_group.append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
                        } else {//if field ERROR
                            $form_group.addClass("has-feedback has-error");
                            $form_group.append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                            $controls.append(json.form_error[field]);
                        }
                    }
                }
            }
        });
    },
    unlock_quotation: function (btn_submit) {
        var self = this;
        $request_number = btn_submit.attr('data-request-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'unlock_requisition',
            type: 'post',
            data: {
                request_number: $request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.box_table_quotation));
                $(btn_submit).button('loading'); //loading button submit
                $(self.box_table_quotation).find(".alert").remove(); //remove all alert msg in form
            },
            error: function (request) {
                self.unblockUI_elm($(self.box_table_quotation));
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                self.unblockUI_elm($(self.box_table_quotation));

                $(btn_submit).button('reset');
                self.alert_msg({type: json.status, dom_container: $(self.box_table_quotation), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    $(self.box_table_quotation).html('<h4 class="alert alert-warning text-center">You will be redirect to page : List Request</h4>');
                    setTimeout(function () {
                        document.location = class_url;
                    }, 500);
                }
            }
        });
    },
    update_capex_data: function (btn_elm) {
        var self = this;
        $request_number = btn_elm.attr('data-request-number');
        $item_id = btn_elm.attr('data-item');
        $state = btn_elm.val();

        $td = btn_elm.parent('td');
        $tr = $td.parent('tr');
        $table = $tr.parents('table');
        $modal_body = $table.parent('.modal-body');

        //process Form POST data
        $.ajax({
            url: class_url + 'update_capex_data',
            type: 'post',
            data: {
                request_number: $request_number,
                item_id: $item_id,
                state: $state
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($table);
                $modal_body.find(".alert").remove(); //remove all alert msg in form
            },
            error: function (request) {
                self.unblockUI_elm($table);
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                self.unblockUI_elm($table);
                self.alert_msg({type: json.status, dom_container: $modal_body, msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    //Update Td
                    if ($state.match(/^YES$/i)) {
                        $td.append(''.concat('<a data-name="view-capex-attachment" style="margin-top: 5px; display: inline-block;" href="' + class_url + 'pdf/capex/' + $item_id + '" target="_blank" title="view capex attachment">',
                                '<span class="glyphicon glyphicon-paperclip"></span> view capex attachment',
                                '</a>'));
                    } else {
                        $td.find('[data-name=view-capex-attachment]').remove();
                    }
                }
            }
        });
    },
    po_tab_init: function () {
        var self = this;

        //On click btn 'select-quot' => 'Select this supplier'
        $(self.tab_po).on('click', "[data-name=select-quot]", function (e) {
            e.preventDefault();
            self.generate_po($(this));
        });

        //On click unlock po, will be delete all 'request order' and update 'price & choosed_quotation_item' to zero
        $(self.tab_po).on('click', '[data-name=unlock-po]', function (e) {
            e.preventDefault();
            if (confirm('Are you sure want to unlock this PO ?  All PO data will be deleted.')) {
                self.unlock_po($(this));
            }
        });

        //On click link 'approve-po'
        $("body").on('click', self.tab_po + ' [data-name=approve-po]', function (e) {
            e.preventDefault();
            self.approved_po($(this));
        });
    },
    generate_po: function (btn_elm) {
        var self = this;
        $request_number = btn_elm.attr('data-request-number');
        $quotation_number = btn_elm.attr('data-quotation-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'generate_po',
            type: 'post',
            data: {
                request_number: $request_number,
                quotation_number: $quotation_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.table_po));
                $(self.box_table_po).find(".alert").remove(); //remove all alert msg in form
                btn_elm.button('loading'); //loading button submit
            },
            error: function (request) {
                self.unblockUI_elm($(self.table_po));
                btn_elm.button('reset'); //loading button submit
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                self.unblockUI_elm($(self.table_po));
                btn_elm.button('reset'); //loading button submit
                self.alert_msg({type: json.status, dom_container: $(self.box_table_po), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    //reload table requisition
                    self.reload_requisition_table($request_number);

                    //Update all button in the same group + remove button po
                    btn_elm.parents("td[data-for=quots]").find('div.group-quot').each(function (index) {
                        $_btn_elm = $(this).find('.btn-sm');

                        //update button attribute
                        $_btn_elm.removeClass('btn-primary');
                        if (!$_btn_elm.hasClass('btn-default')) {
                            $_btn_elm.addClass('btn-default');
                        }
                        $_btn_elm.html('Select this supplier');
                        $_btn_elm.attr('data-name', 'select-quot');

                        //remove po link
                        $po_link = $(this).find('[data-name=link-po]');
                        if ($po_link) {
                            $po_link.remove();
                        }
                    });

                    //Update selected button attribute
                    btn_elm.removeClass('btn-default');
                    btn_elm.addClass('btn-primary');
                    if (!btn_elm.hasClass('btn-primary')) {
                        btn_elm.addClass('btn-primary');
                    }
                    btn_elm.html('<span aria-hidden="true" class="glyphicon glyphicon-ok-sign"></span> Selected');
                    btn_elm.attr('data-name', 'selected-quot');

                    //add link po|wo
                    btn_elm.parent('div.col-md-4').prepend(json.request_order_link);

                    //update count-po
                    $("[data-name=count-po]").html(json.count_po);
                }
            }
        });
    },
    reload_requisition_table: function (request_number) {
        var self = this;
        $.ajax({
            url: class_url + 'reload_requisition_table',
            type: 'post',
            data: {
                request_number: request_number,
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                $(self.box_table_requsition + " .table-responsive").html(json);
            }
        });
    },
    unlock_po: function (btn_submit) {
        var self = this;
        $request_number = btn_submit.attr('data-request-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'unlock_requisition',
            type: 'post',
            data: {
                request_number: $request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.box_table_po));
                $(btn_submit).button('loading'); //loading button submit
                $(self.box_table_po).find(".alert").remove(); //remove all alert msg in form
            },
            error: function (request) {
                self.unblockUI_elm($(self.box_table_po));
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                self.unblockUI_elm($(self.box_table_po));

                $(btn_submit).button('reset');
                self.alert_msg({type: json.status, dom_container: $(self.box_table_po), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    //self.reload_requisition_table($request_number);
                    $(self.box_table_po).html('<h4 class="alert alert-warning text-center">You will be redirect to page : List Request</h4>');
                    setTimeout(function () {
                        document.location = class_url;
                    }, 500);
                }
            }
        });
    },
    approved_po: function (btn_submit) {
        var self = this;
        $request_number = btn_submit.attr('data-request-number');

        //process Form POST data
        $.ajax({
            url: class_url + 'approved_requisition',
            type: 'post',
            data: {
                request_number: $request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
                self.blockUI_elm($(self.box_table_po));
                $(btn_submit).button('loading'); //loading button submit
                $(self.box_table_po).find('.alert').remove();
            },
            error: function (request) {
                self.unblockUI_elm($(self.box_table_po));
                $(btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.unblockUI_elm($(self.box_table_po));
                self.on_finish_ajax(json); //always call this on the first line

                $(btn_submit).button('reset');

                self.alert_msg({type: json.status, dom_container: $(self.box_table_po), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    //hide button 'Select this supplier'
                    $(self.table_po + ' [data-name=select-quot]').remove();

                    //remove approve, unapprove po button
                    $(self.tab_po + ' [data-name=approve-po]').remove();
                    $(self.tab_po + ' [data-name=unlock-po]').remove();

                    //reload table
                    self.reload_do_table($request_number);
                }
            }
        });
    },
    reload_do_table: function (request_number) {
        var self = this;
        $.ajax({
            url: class_url + 'reload_do_table',
            type: 'post',
            data: {
                request_number: request_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                $(self.tab_do + " #box-table-do").html(json);
            }
        });
    },
    do_tab_init: function () {
        var self = this;
        
        //Set count do
        $("[data-name=count-do]").html($(self.tab_do + ' [data-name=view-do]').length);

        //On click input do
        $(self.tab_do).on('click', '[data-name=input-do]', function (e) {
            e.preventDefault();
            $request_order_number = $(this).attr('data-request-order-number');
            $("#form-input-do [name=request_order_number]").val($request_order_number);
            self.get_do_items($request_order_number);
        });


        //On click view do
        $(self.tab_do).on('click', '[data-name=view-do]', function (e) {
            e.preventDefault();
            self.view_do($(this).attr('data-request-order-number'));
        });

        //set slider for delivery score
        $("#delivery_score_range").slider({
            range: "max",
            min: 0,
            max: 100,
            value: 0,
            slide: function (event, ui) {
                $("#delivery_score").val(ui.value);
            }
        });
        $("#delivery_score").val($("#delivery_score_range").slider("value"));

        //On submit  form#form-input-do
        $(self.tab_do).on('submit', "form#form-input-do", function (e) {
            e.preventDefault();
            $formData = new FormData($(this)[0]);
            self.input_do($formData);
        });

        //On click btn 'save-input-do' trigger submit form#form-input-do
        $(self.tab_do).on('click', '[data-name=save-input-do]', function (e) {
            e.preventDefault();
            $("form#form-input-do").trigger('submit');
        });
    },
    get_do_items: function (request_order_number) {
        var self = this;
        $.ajax({
            url: class_url + 'get_do_items',
            type: 'post',
            data: {
                request_order_number: request_order_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                $("#modal-input-do .modal-body .items-container").html(json);
                $("#modal-input-do").modal('show');
            }
        });
    },
    input_do: function ($formData) {
        var self = this;
        $btn_submit = self.tab_do + ' [data-name=save-input-do]';
        $modal_body = self.tab_do + ' #modal-input-do .modal-body';
        $btn_input_do = self.tab_do + ' [data-name=input-do][data-request-order-number=' + $($modal_body).find("[name=request_order_number]").val() + ']';
        $btn_count_do = '[data-name=count-do]';
        $btn_view_do = self.tab_do + ' [data-name=view-do]';

        $.ajax({
            url: class_url + 'input_do',
            type: 'post',
            data: $formData,
            dataType: 'json',
            async: true,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function () {
                $($btn_submit).button('loading'); //loading button submit
                $($modal_body).find('.alert').remove();
            },
            error: function (request) {
                $($btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line

                $($btn_submit).button('reset');

                self.alert_msg({type: json.status, dom_container: $($modal_body), msg: json.msg, fill_type: 'prepend'}); //msg

                if (json.status.match(/^success$/i)) {
                    $($btn_input_do).attr('data-do-id', json.do_id); //add attr 'data-do-id'
                    $($btn_input_do).html('<span aria-hidden="true" class="glyphicon glyphicon-ok-sign"></span> View DO ' + json.do_id);
                    $($btn_input_do).attr('data-name', 'view-do'); //change data-name="input-do" to data-name="view-do" PLACE THIS ON THE LAST 

                    //update count-po
                    $($btn_count_do).html($($btn_view_do).length);
                }
            }
        });
    },
    view_do: function (request_order_number) {
        var self = this;
        $.ajax({
            url: class_url + 'view_do',
            type: 'post',
            data: {
                request_order_number: request_order_number
            },
            dataType: 'json',
            async: true,
            beforeSend: function () {
            },
            error: function (request) {
                console.log(request.responseText);
            },
            success: function (json) {
                self.on_finish_ajax(json); //always call this on the first line
                $("#modal-view-do .modal-body").html(json);
                $("#modal-view-do").modal('show');
            }
        });
    }
};


request_form_detail = $.extend(site, request_form_detail);
$(function () {
    site.request_form_detail();
});