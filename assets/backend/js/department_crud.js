var division_crud = {
    division_crud: function() {
        /*START INIT PARENT JS*/
        var self = this;
        self.init();
        if (typeof (crud) !== "undefined" && enable_crud == true) {
            self.crud();
        }
        /*END INIT PARENT JS*/
    }
};

division_crud = $.extend(crud, division_crud);

$(function() {
    crud.division_crud();
});