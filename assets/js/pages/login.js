var Login = function() {
    var handleLogin = function() {
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: "Email is required."
                },
                password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                $data = new FormData($(".login-form")[0]);
                process_login($data);//form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.login-form').submit(function(e) {
            e.preventDefault();
        });


        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

    };

    var process_login = function(data) {
        $form = '.login-form';
        $btn_submit = '.login-form button[type=submit]';

        $.ajax({
            url: class_url,
            type: 'post',
            data: data,
            dataType: 'json',
            async: true,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
                $($btn_submit).button('loading'); //loading button submit
                $($form).find(".alert").remove(); //remove all alert msg in form

                //remove all form-group class
                $form_group = $($form).find(".form-group");
                $form_group.removeClass("has-feedback has-success has-warning has-error");
                $controls = $form_group.find(".controls");
                $controls.find(".text-danger").remove();
            },
            error: function(request) {
                $($btn_submit).button('reset');
                console.log(request.responseText);
            },
            success: function(json) {
                $($btn_submit).button('reset');
                Login.alert_msg({type: json.status, dom_container: $($form+' .error-msg-container'), msg: json.msg, fill_type: 'replace'}); //error msg
                if (json.status.match(/^success$/i)) {
                    $GET = Login.searchToObject();
                    document.location = typeof($GET['urlredirect']) !== "undefined" ?  $GET['urlredirect'] : base_url + 'backend';
                } else {
                    for (var field in json.form_error) {
                        $form_group = $($form).find(".form-group[for=" + field + "]");
                        $controls = $form_group.find(".controls");
                        if (json.form_error[field] == "") { //if field SUCCESS
                            $form_group.addClass("has-feedback has-success");
                        } else {//if field ERROR
                            $form_group.addClass("has-feedback has-error");
                            $controls.append(json.form_error[field]);
                        }
                    }
                }
                Login.scrollTop($($form + ' .alert'), 500, $('html, body')); //scroll to top
            }
        });
    };

    var handleForgetPassword = function() {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required: "Email is required."
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   

            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        $('.forget-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function() {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function() {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    };

    return {
        //main function to initiate the module
        init: function() {
            handleLogin();
            handleForgetPassword();
        }

    };

}();

Login = $.extend(site, Login);