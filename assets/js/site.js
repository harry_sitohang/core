var site = {
    init: function() {
        var self = this;
        GET = self.searchToObject();
        query_string = $.isEmptyObject(GET) ? '' : '?'+$.param(GET);
        $("body").delegate(".prevent-default", "click", function(e) {
            e.preventDefault();
        });
    },
    /*  Format number */
    formatNumber: function(number, decimal_digit, thousand_separator, decimal_separator) {
        decimal_digit = (typeof (decimal_digit) === "undefined") ? 0 : decimal_digit;
        thousand_separator = (typeof (thousand_separator) === "undefined") ? "," : thousand_separator;
        decimal_separator = (typeof (decimal_separator) === "undefined") ? "." : decimal_separator;
        return (typeof (accounting) === "undefined") ? number : accounting.formatNumber(parseInt(number), decimal_digit, thousand_separator, decimal_separator);
    },
    searchToObject: function(search) {
        search = typeof (search) !== "undefined" ? search : window.location.search;
        var pairs = search.substring(1).split("&"), obj = {}, pair, index;
        for (index in pairs) {
            if (pairs[index] === "")
                continue;
            pair = pairs[index].split("=");
            obj[ decodeURIComponent(pair[0]) ] = decodeURIComponent(pair[1]);
        }
        return obj;
    },
    reload: function() {
        location.reload();
    },
    forbidden_access : function(){
        alert('Sorry, but you have no access for this page.');
        location.reload();
    },
    on_finish_ajax: function(json) {
        if (typeof (json.action) !== "undefined") {
            this[json.action]();
        }
    },
    show_loading_animation: function(options) {
        var default_options = {
            css_background: {
                'width': '100%',
                'height': '100%',
                'background-color': '#000',
                'opacity': '0.8',
                'z-index': '9999',
                'position': 'fixed'
            },
            css_loading_box: {
                'position': 'absolute',
                'top': '50%',
                'left': '50%',
                'margin-left': '-15px'
            },
            css_loading_text: {
                'color': '#fff',
                'font-size': '18px',
                'font-family': 'sans-serif',
                'width': '150px',
                'letter-spacing': '1px',
                'display': 'block',
                'margin-left': '-60px',
                'text-align': 'center'
            },
            img: '<img src="' + _assets + 'img/loading.gif">',
            text_loading: 'Please wait'
        };

        options = (typeof (options) !== "undefined") ? $.extend(default_options, options) : default_options;

        //Generate loading HTML
        var loading_html = '<div id="bg-loading-animation">';
        loading_html = loading_html.concat(
                '<div class="loading-box">',
                options.img,
                '<br/>',
                '<span>' + options.text_loading + '</span>',
                '</div>',
                '</div>'
                );

        $('body').prepend(loading_html);

        $('body').find('#bg-loading-animation').css(options.css_background);
        $('body').find('#bg-loading-animation .loading-box').css(options.css_loading_box);
        $('body').find('#bg-loading-animation .loading-box span').css(options.css_loading_text);
    },
    hide_loading_animation: function() {
        $('body').find('#bg-loading-animation').remove();
    },
    alert_msg: function(options) {
        $.extend({
            type: null,
            dom_container: null,
            title: '',
            msg: '',
            fill_type: 'replace',
            show_title: false,
            auto_close: false,
            time_to_close: 5000
        }, options);

        var alert_class;
        switch (options.type) {
            case 'warning':
                alert_class = 'alert';
                break;
            case 'success' :
                alert_class = 'alert alert-success';
                break;
            case 'error' :
                alert_class = 'alert alert-danger';
                break;
            case 'info' :
                alert_class = 'alert alert-info';
                break;
            default :
                alert_class = 'alert alert-block';
                break;
        }

        var alert_title = (typeof (options.title) === "undefined" || options.show_title == false) ? '' : '<strong>' + options.title + '</strong>';
        switch (options.fill_type) {
            case 'append':
                if (options.dom_container.children('div.alert').length > 0) {
                    options.dom_container.children('div.alert').remove();
                }
                options.dom_container.append('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + alert_title + ' ' + options.msg + '</div>');
                break;
            case 'prepend' :
                if (options.dom_container.children('div.alert').length > 0) {
                    options.dom_container.children('div.alert').remove();
                }
                options.dom_container.prepend('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + alert_title + ' ' + options.msg + '</div>');
                break;
            case 'replace' :
                options.dom_container.html('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + alert_title + ' ' + options.msg + '</div>');
                break;
            default :
                options.dom_container.html('<div class="' + alert_class + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + options.title + ' ' + options.msg + '</div>');
                break;
        }

        if (options.auto_close === true) {
            setTimeout(function() {
                options.dom_container.find('.alert').remove();
            }, options.time_to_close);
        }
    },
    build_clean_url: function(url) {
        return url.replace(/&/g, "and").replace(/[^a-zA-Z0-9 _-]+/g, '').replace(/\s/gi, '-').replace(/--+/g, '-').toLowerCase();
    },
    scrollTop: function(dom_container, speed, parent_container, offset) {
        dom_container = (typeof (dom_container) === "undefined") ? $("body") : dom_container;
        speed = (typeof (speed) === "undefined") ? 500 : speed;
        parent_container = (typeof (parent_container) === "undefined") ? $('html, body') : parent_container;
        offset = (typeof (offset) === "undefined") ? 0 : offset;
        parent_container.animate({
            scrollTop: dom_container.offset().top - offset
        }, speed);
    },
    blockUI_elm: function(elm) {
        elm.block({
            message: '<img src="' + _assets + 'img/loading-spinner-grey.gif">',
            css: {
                border: '0',
                padding: '0',
                background: 'none'
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.2,
                cursor: 'wait'
            }
        });
    },
    // function to  un-block element(finish loading)
    unblockUI_elm: function(elm) {
        setInterval(function() {
            elm.unblock();
        }, 1000);
    }
};
