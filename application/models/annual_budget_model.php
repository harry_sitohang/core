<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

/**
 * @author Harry <if07087@gmail.com>
 * @copyright (c) 2014, Harry
 */
class Annual_budget_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "budget_id";
    public $_table = "annual_budget";
    public $_view = "annual_budget";

}

/* End of file annual_budget_model.php */
/* Location: ./application/models/annual_budget_model.php */