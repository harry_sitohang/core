<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Test_model extends Core_model {
    //Datatables Attribute
    public $primary_key = "member_address_id";
    public $_table = "member_address";
    public $_view = "view_member_address";

}

/* End of file test_model.php */
/* Location: ./application/models/test_model.php */