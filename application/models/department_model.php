<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Department_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "department_id";
    public $_table = "department";
    public $_view = "department";

    public function get_enum_department() {
        $this->db->select("*");
        $this->db->from($this->_table);
        $result = $this->db->get()->result();
        $return = array("Select..." => "");
        foreach ($result as $row) {
            $return[$row->department_name] = $row->department_id;
        }
        return $return;
    }

}

/* End of file department_model.php */
/* Location: ./application/models/department_model.php */