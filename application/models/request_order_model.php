<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Request_order_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "request_order_number";
    public $_table = "request_order";
    public $_view = "request_order";

    public function generate_request_order_pdf_result($request_order_number) {
        $this->db->select('ro.request_order_number, ro.created_date, mr.*, s.supplier_name, s.supplier_address, u.user_fullname');
        $this->db->from('request_order ro');
        $this->db->join('quotation q', 'q.quotation_number = ro.quotation_number');
        $this->db->join('supplier s', 's.supplier_id = q.supplier_id');
        $this->db->join('user u', 'u.user_id = ro.created_by');
        $this->db->join('quotation_item i', 'i.quotation_number = q.quotation_number');
        $this->db->join('material_request mr', 'mr.choosed_quotation_item_id = i.quotation_item_id');
        $this->db->where(array('ro.request_order_number' => $request_order_number));
        return $this->db->get();
    }

    public function is_complete_po($request_number) {
        $this->db->select("*");
        $this->db->from("quotation");
        $this->db->where(array("request_number" => $request_number));
        $this->db->group_by("group_number"); 
        $quots = $this->db->get()->result();

        foreach ($quots as $quot) {
            $this->db->select("COUNT(1) AS `count`");
            $this->db->from("request_order o");
            $this->db->where(array("o.request_number" => $request_number, "o.group_number" => $quot->group_number));
            if ($this->db->get()->row()->count == 0) {
                return FALSE;
            }
        }

        return TRUE;
    }

}

/* End of file request_order.php */
/* Location: ./application/models/request_order_model.php */