<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Delivery_order_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "do_id";
    public $_table = "delivery_order";
    public $_view = "delivery_order";

    public function get_do($request_number) {
        $this->db->select("do.*, o.request_order_number, o.quotation_number, o.request_type, o.created_date, o.process_date, q.quotation_file, s.supplier_name, s.supplier_address");
        $this->db->from("request_order o");
        $this->db->join("delivery_order do", "do.request_order_number = o.request_order_number", "left");
        $this->db->join("quotation q", "q.quotation_number = o.quotation_number");
        $this->db->join("supplier s", "s.supplier_id = q.supplier_id");
        $this->db->where(array("o.request_number" => $request_number));
        $data = $this->db->get()->result_array();
        foreach ($data as $key => $row) {
            $this->db->select("mr.*");
            $this->db->from("quotation_item i");
            $this->db->join("material_request mr", "mr.item_id = i.item_id");
            $this->db->where(array("i.quotation_number" => $row['quotation_number']));
            $data[$key]['items'] = $this->db->get()->result_array();
        }
        return $data;
    }

    public function get_result_detail($field_where = '', $val_where = '') {
        $this->db->select("o.*, u.user_fullname");
        $this->db->from("{$this->_view} o");
        $this->db->join("user u", "u.user_id = o.received_by");
        if (is_array($field_where)) {
            $this->db->where($field_where);
        } else if (!empty($field_where) && !empty($val_where)) {
            $this->db->where($field_where, $val_where);
        }
        return $this->db->get();
    }

    public function get_do_items($request_order_number) {
        $this->db->select("mr.*");
        $this->db->from("request_order ro");
        $this->db->join("material_request mr", "mr.quotation_number = ro.quotation_number");
        $this->db->where(array("ro.request_order_number" => $request_order_number));
        
        return $this->db->get();
    }

}

/* End of file delivery_order_model.php */
/* Location: ./application/models/delivery_order_model.php */