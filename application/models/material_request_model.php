<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Material_request_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "item_id";
    public $_table = "material_request";
    public $_view = "view_request_form_detail";

    /**
     * 
     * @todo check if item_id is valid for current 'user_group'
     */
    public function check_valid_item_id_for_current_user_group($item_id, $user_group_id) {
        $this->db->select('COUNT(1) AS `count`');
        $this->db->from('material_request m');
        $this->db->join('form_request r', 'r.request_number = m.request_number');
        $this->db->join('user u', 'u.user_id = r.request_by');
        $this->db->where(array('m.item_id' => $item_id, 'u.user_group_id' => $user_group_id));
        if ($this->db->get()->row()->count == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    /**
     * @todo Get Unselected item to display in quotation table
     */
    public function get_unselected_items($request_number) {
        return $this->db->query("SELECT * FROM {$this->_table} WHERE request_number = ? AND item_id NOT IN(SELECT item_id FROM quotation_item WHERE request_number = ?)", array($request_number, $request_number));
    }
    
    public function get_result_detail($field_where = '', $val_where = '') {
        $this->db->select("*");
        $this->db->from("{$this->_view}");
        if (is_array($field_where)) {
            $this->db->where($field_where);
        } else if (!empty($field_where) && !empty($val_where)) {
            $this->db->where($field_where, $val_where);
        }
        return $this->db->get();
    }
}

/* End of file material_request_model.php */
/* Location: ./application/models/material_request_model.php */