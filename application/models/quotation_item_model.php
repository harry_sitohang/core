<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Quotation_item_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "quotation_item_id";
    public $_table = "quotation_item";
    public $_view = "quotation_item";
}

/* End of file quotation_item_model.php */
/* Location: ./application/models/quotation_item_model.php */