<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Request_form_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "request_number";
    public $_table = "form_request";
    public $_view = "view_request_form";

    public function get_enum_item_type() {
        $enums = $this->get_enum_values('item_type', 'material_request');
        $result['0'] = 'Please Select';
        foreach ($enums as $enum) {
            $result[$enum] = $enum;
        }
        return $result;
    }

    public function get_current_lock_position($request_number) {
        $this->db->select("*");
        $this->db->from("form_request");
        $this->db->where(array('request_number' => $request_number));
        $row = $this->db->get()->row();
        if (empty($row)) {
            return FALSE;
        }

        $proceed = $row->proceed;
        if ($proceed == 'NO') {
            $this->db->select("u.*");
            $this->db->from("view_user u");
            $this->db->where(array("u.user_id" => $row->request_by));
            return array('status' => 'CLOSED', 'department' => $this->db->get()->row()->user_group_type, 'label' => '<span class="label label-success"><i class="fa fa-lock"></i> CLOSED</span>');
        } else {
            $approval_state = $this->db->query("SELECT fx_get_approval_state(?) AS `approval_state`", array($request_number))->row()->approval_state;
            if (preg_match("/(6,|6$)/i", $approval_state)) { //DIRECTOR
                return array('status' => 'CLOSED', 'department' => '-', 'label' => '<span class="label label-success"><i class="fa fa-lock"></i> CLOSED</span>');
            } else if (preg_match("/(2,|2$)/i", $approval_state)) { //ADMIN
                return array('status' => 'PROCESSING', 'department' => 'DIRECTOR', 'label' => '<span class="label label-danger"><i class="fa fa-lock"></i> Lock by DIRECTOR</span>');
            } else if (preg_match("/(5,|5$)/i", $approval_state)) { //OPERATIONAL MANAGER
                return array('status' => 'PROCESSING', 'department' => 'ADMIN', 'label' => '<span class="label label-danger"><i class="fa fa-lock"></i> Lock by ADMIN</span>');
            }
        }
    }

    public function check_valid_request_for_current_user_group($request_number, $user_group_id) {
        $this->db->select('COUNT(1) AS `count`');
        $this->db->from('form_request r');
        $this->db->join('user u', 'u.user_id = r.request_by');
        $this->db->where(array('r.request_number' => $request_number, 'u.user_group_id' => $user_group_id));
        if ($this->db->get()->row()->count == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_valid_request_for_current_department($request_number, $department_id) {
        $this->db->select('COUNT(1) AS `count`');
        $this->db->from('form_request r');
        $this->db->join('user u', 'r.request_by = u.user_id');
        $this->db->join('user_group g', 'g.user_group_id = u.user_group_id');
        $this->db->where(array("r.request_number" => $request_number, 'g.department_id' => $department_id));
        if ($this->db->get()->row()->count == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_valid_proceed_request($request_number, $department_id) {
        if ($this->check_valid_request_for_current_department($request_number, $department_id)) {
            $this->db->select('proceed');
            $this->db->from('form_request');
            $this->db->where(array('request_number' => $request_number));
            if ($this->db->get()->row()->proceed == 'YES') {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    public function get_request_state($request_number) {
        return $this->db->query('select fx_get_approval_state(?) AS `state`', array($request_number))->row()->state;
    }

    public function check_valid_approved_requisition($request_number, $department_id) {
        $this->db->select('COUNT(1) AS `count`');
        $this->db->from('view_approval a');
        $this->db->where(array('a.request_number' => $request_number, 'a.approval_department_id' => $department_id));
        if ($this->db->get()->row()->count == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_approval($request_number, $department_id) {
        $this->db->select('*');
        $this->db->from('view_approval a');
        $this->db->where(array('a.request_number' => $request_number, 'a.approval_department_id' => $department_id));
        return $this->db->get();
    }

    public function check_item_id_for_request_number($item_id, $request_number) {
        $this->db->select("COUNT(1) AS `count`");
        $this->db->from("quotation_item i");
        $this->db->join("quotation q", "q.quotation_number = i.quotation_number");
        $this->db->where(array("i.item_id" => $item_id, "q.request_number" => $request_number));
        return $this->db->get()->row()->count == 0 ? FALSE : TRUE;
    }

}

/* End of file request_form_model.php */
/* Location: ./application/models/request_form_model.php */