<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Supplier_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "supplier_id";
    public $_table = "supplier";
    public $_view = "supplier";

}

/* End of file supplier_model.php */
/* Location: ./application/models/supplier_model.php */