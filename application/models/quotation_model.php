<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'models/core_model.php';

class Quotation_model extends Core_model {

    //Datatables Attribute
    public $primary_key = "quotation_number";
    public $_table = "quotation";
    public $_view = "quotation";

    public function get_result($field_where = '', $val_where = '') {
        $this->db->select("*");
        $this->db->from("view_quotation");
        if (is_array($field_where)) {
            $this->db->where($field_where);
        } else if (!empty($field_where) && !empty($val_where)) {
            $this->db->where($field_where, $val_where);
        }
        return $this->db->get();
    }

    public function get_result_per_group_number($field_where = '', $val_where = '') {
        $this->db->select("*");
        $this->db->from("view_quotation");
        if (is_array($field_where)) {
            $this->db->where($field_where);
        } else if (!empty($field_where) && !empty($val_where)) {
            $this->db->where($field_where, $val_where);
        }
        $this->db->group_by('group_number');
        return $this->db->get();
    }

    public function get_result_per_group_number_detailed($request_number) {
        $this->db->select('q.*, s.supplier_name, o.request_order_number, o.request_type');
        $this->db->from("{$this->_table} q");
        $this->db->join('supplier s', 's.supplier_id = q.supplier_id');
        $this->db->join('request_order o', 'o.quotation_number = q.quotation_number', 'LEFT');
        $this->db->where(array('q.request_number' => $request_number));
        $this->db->order_by("q.quotation_number", "asc"); 
        $quotations = $this->db->get()->result_array();
        $data = array();
        foreach ($quotations as $quotation) {
            $data[$quotation['group_number']][$quotation['quotation_number']] = $quotation;
            $this->db->select('i.*, mr.item_description, mr.choosed_quotation_item_id, mr.capex_number, mr.item_qty, mr.item_type');
            $this->db->from('quotation_item i');
            $this->db->join('material_request mr', 'mr.item_id = i.item_id');
            $this->db->where(array('i.quotation_number' => $quotation['quotation_number']));
            $items = $this->db->get()->result_array();
            $data[$quotation['group_number']][$quotation['quotation_number']]['items'] = $items;
        }
        //echo '<pre>'; print_r($data); die;
        return $data;
    }

    public function get_row_per_group_number_detailed($request_number, $group_number) {
        $this->db->select('q.*, s.supplier_name');
        $this->db->from("{$this->_table} q");
        $this->db->join('supplier s', 's.supplier_id = q.supplier_id');
        $this->db->where(array('request_number' => $request_number, 'group_number' => $group_number));
        $quotations = $this->db->get()->result_array();
        $data = array();
        foreach ($quotations as $quotation) {
            $data[$quotation['quotation_number']] = $quotation;
            $this->db->select('i.*, mr.item_description, mr.choosed_quotation_item_id, mr.capex_number, mr.item_qty, mr.item_type');
            $this->db->from('quotation_item i');
            $this->db->join('material_request mr', 'mr.item_id = i.item_id');
            $this->db->where(array('i.quotation_number' => $quotation['quotation_number']));
            $items = $this->db->get()->result_array();
            $data[$quotation['quotation_number']]['items'] = $items;
        }
        //echo '<pre>'; print_r($data); die;
        return $data;
    }

    public function get_next_group_number($request_number) {
        $this->db->select('MAX(group_number) AS `next_group_number`');
        $this->db->from($this->_table);
        $this->db->where(array(
            'request_number' => $request_number
        ));
        $row = $this->db->get()->row();
        return empty($row) ? 1 : (!$row->next_group_number) ? 1 : $row->next_group_number + 1;
    }

    public function get_result_detail($field_where = '', $val_where = '') {
        $this->db->select("*");
        $this->db->from("view_quotation_detail");
        if (is_array($field_where)) {
            $this->db->where($field_where);
        } else if (!empty($field_where) && !empty($val_where)) {
            $this->db->where($field_where, $val_where);
        }
        return $this->db->get();
    }

    public function get_result_detail_per_group_number($field_where = '', $val_where = '') {
        $this->db->select("*");
        $this->db->from("view_quotation_detail");
        if (is_array($field_where)) {
            $this->db->where($field_where);
        } else if (!empty($field_where) && !empty($val_where)) {
            $this->db->where($field_where, $val_where);
        }
        $this->db->group_by("title");
        return $this->db->get();
    }

    public function check_group_number_for_current_user($request_number, $group_number) {
        $this->db->select("COUNT(1) AS `count`");
        $this->db->from($this->_table);
        $this->db->where(array(
            'request_number' => $request_number,
            'group_number' => $group_number
        ));
        return $this->db->get()->row()->count ? TRUE : FALSE;
    }

    public function is_complete_quotation_item($request_number) {
        $this->db->select("*");
        $this->db->from("material_request");
        $this->db->where(array("request_number" => $request_number));
        $items = $this->db->get()->result();

        foreach ($items as $item) {
            $this->db->select("COUNT(1) AS `count`");
            $this->db->from("quotation_item i");
            $this->db->join("quotation q", "q.quotation_number = i.quotation_number");
            $this->db->where(array("q.request_number" => $request_number, "i.item_id" => $item->item_id));
            if ($this->db->get()->row()->count == 0) {
                return FALSE;
            }
        }

        return TRUE;
    }

    public function get_selected_quotation($item_id) {
        $this->db->select('q.*, mr.item_qty, i.price');
        $this->db->from('material_request mr');
        $this->db->join("quotation_item i", "i.quotation_item_id = mr.choosed_quotation_item_id");
        $this->db->join("view_quotation q", "q.quotation_number = i.quotation_number");
        $this->db->where(array('mr.capex_number' => $item_id));
        return $this->db->get();
    }

    public function get_po_wo_number($quotation_numbers) {
        $this->db->select('*');
        $this->db->from('request_order');
        $this->db->where(array('quotation_number' => $quotation_numbers));
        return $this->db->get();
    }

    public function check_quotation_number_for_request_number($quotation_number, $request_number) {
        $this->db->select("COUNT(1) AS `count`");
        $this->db->from("quotation q");
        //$this->db->join("request_order o", "o.quotation_number = o.quotation_number", "left");
        $this->db->where(array(
            'q.quotation_number' => $quotation_number,
            'q.request_number' => $request_number,
        ));
        //$this->db->where('o.quotation_number IS NULL', NULL, FALSE);
        return $this->db->get()->row()->count ? TRUE : FALSE;
    }

    public function get_request_type($quotation_number) {
        $this->db->select("COUNT(1) AS `count`");
        $this->db->from("quotation_item i");
        $this->db->join("material_request mr", "mr.item_id = i.item_id");
        $this->db->where(array("i.quotation_number" => $quotation_number, "mr.item_type" => "MATERIAL"));
        return $this->db->get()->row()->count ? 'PO' : 'WO';
    }

}

/* End of file quotation_model.php */
/* Location: ./application/models/quotation_model.php */