<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/crud.php';

/**
 * @author Harry <if07087@gmail.com>
 * @since 10 Oct 2014 
 * @todo Supplier management
 */
class Supplier extends Crud {

    public function __construct() {
        parent::__construct();
    }

    protected function datatable_customize_columns() {
        $this->columns[0]["visible"] = FALSE;
        return $this->columns;
    }

    protected function set_form_validation($field, $action) {
        if ($field['db'] == 'supplier_email') {
            return "xss_clean|required|valid_email";
        } else {
            return parent::set_form_validation($field, $action);
        }
    }

    protected function set_crud_asset() {
        parent::set_crud_asset();

        //set asset css & js
        $this->page_js[] = "{$this->_general_assets}plugins/jquery-tags-input/jquery.tagsinput.min.js";
        $this->page_css[] = "{$this->_general_assets}plugins/jquery-tags-input/jquery.tagsinput.css";
        $this->view->set("pages_css", $this->page_css);
        $this->view->set("pages_js", $this->page_js);
    }

    protected function set_input_form($row, $field) {
        $field_name = $field["db"];
        $field_val = $row && isset($row->{$field_name}) ? $row->{$field_name} : $field['field_data']['default'];
        $placeholder = $field["label"];

        if ($field_name == 'supplier_business') {
            return sprintf('<input id="supplier_business" name="supplier_business" type="text" class="form-control tags" value="%s" placeholder="%s"/>', $field_val, $placeholder);
        } else {
            return parent::set_input_form($row, $field);
        }
    }

}

/* End of file supplier.php */
/* Location: ./application/controllers/backend/supplier.php */