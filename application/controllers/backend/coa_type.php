<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/crud.php';

/**
 * @author Harry <if07087@gmail.com>
 * @since 10 Oct 2014 
 * @todo Coa Type management
 */
class Coa_type extends Crud {

    public function __construct() {
        parent::__construct();
    }

    protected function datatable_customize_columns() {
        $this->columns[0]["visible"] = FALSE;
        return $this->columns;
    }

}

/* End of file coa_type.php */
/* Location: ./application/controllers/backend/coa_type.php */