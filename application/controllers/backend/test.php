<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/crud.php';

class Test extends Crud {

    public function __construct() {
        parent::__construct();
    }

    protected function set_input_form($row, $field) {
        if (!$row) {
            $row = new stdClass();
            $row->country_id = isset($row->country_id) ? $row->country_id : 1;
            $row->province_id = isset($row->province_id) ? $row->province_id : 1;
            $row->city_id = isset($row->city_id) ? $row->city_id : 1;
        }
        if (preg_match("/^country_id$/", $field["db"])) {
            return form_dropdown($field['db'], create_form_dropdown_options($this->db->query("SELECT * FROM location_country")->result_array(), 'country_id', 'country_name'), $row->{$field['db']}, 'id="' . $field['db'] . '" class="form-control" placeholder="' . ucwords(preg_replace("/_/", " ", $field['db'])) . '"');
        } else if (preg_match("/^province_id$/", $field["db"])) {
            return form_dropdown($field['db'], create_form_dropdown_options($this->db->query("SELECT * FROM location_province WHERE country_id = ?", array($row->country_id))->result_array(), 'province_id', 'province_name'), $row->{$field['db']}, 'id="' . $field['db'] . '" class="form-control" placeholder="' . ucwords(preg_replace("/_/", " ", $field['db'])) . '"');
        } else if (preg_match("/^city_id$/", $field["db"])) {
            return form_dropdown($field['db'], create_form_dropdown_options($this->db->query("SELECT * FROM location_city WHERE province_id = ?", array($row->province_id))->result_array(), 'city_id', 'city_name'), $row->{$field['db']}, 'id="' . $field['db'] . '" class="form-control" placeholder="' . ucwords(preg_replace("/_/", " ", $field['db'])) . '"');
        } else if (preg_match("/^address_detail$/", $field["db"])) {
            return form_textarea(array('name' => $field['db'], 'id' => $field['db'], 'value' => $row && isset($row->{$field['db']}) ? $row->{$field['db']} : $field['field_data']['default'], 'class' => 'form-control tinymce', 'placeholder' => $field['db']));
        } else {
            return parent::set_input_form($row, $field);
        }
    }

    protected function set_datatable_setting_parameter() {
        $this->view->set("datatable_setting", array(
            'columnDefs' => $this->columns,
            'visible_actions' => $this->visible_actions,
            'dom' => '<"top clearfix"pl>rt<"bottom"ip><"clear">',
            'stateSave' => FALSE,
            'lengthMenu' => array(array(5, 10, 25, 50, -1), array(5, 10, 25, 50, "All")),
            'iDisplayLength' => 10,
            'oLanguage' => array(
                "lengthMenu" => "Display _MENU_ records per page",
                "zeroRecords" => "Nothing found - sorry",
                "info" => "Showing page _PAGE_ of _PAGES_",
                "infoEmpty" => "No records available",
                "infoFiltered" => "(filtered from _MAX_ total records)",
                "sProcessing" => "<img src='{$this->view->get("_assets")}img/loading-spinner-grey.gif'/><span>&nbsp;&nbsp;Loading...</span>",
            ),
            "order" => array(
                array(0, "desc")
            )
        ));
    }

    protected function datatable_customize_columns() {
        $this->columns[0]["visible"] = FALSE;
        $this->columns[1]["visible"] = FALSE;
        $this->columns[2]["visible"] = FALSE;
        $this->columns[8]["visible"] = FALSE;
        $this->columns[12]["type"] = "enum";
        $this->columns[12]["enums"] = array("Select..." => "", "YES" => "YES", "NO" => "NO");
        //echo "<pre>"; print_r($this->columns); die;
        return $this->columns;
    }

}

/* End of file test.php */
/* Location: ./application/controllers/backend/test.php */