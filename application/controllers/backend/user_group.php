<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/crud.php';

/**
 * @author Harry <if07087@gmail.com>
 * @since 10 Oct 2014 
 * @todo Menu management : User Group CRUD
 */
class User_group extends Crud {

    public function __construct() {
        parent::__construct();
    }

    public function init() {
        $this->load->model('Department_model');
        parent::init();
    }

    protected function set_input_form($row, $field) {
        if (!$row) {
            $row = new stdClass();
            $row->department_id = isset($row->department_id) ? $row->department_id : 0;
        }
        if ($field["db"] == 'department_id') {
            return form_dropdown($field['db'], create_form_dropdown_options($this->db->query("SELECT * FROM department")->result_array(), 'department_id', 'department_name'), $row->{$field['db']}, 'id="' . $field['db'] . '" class="form-control" placeholder="' . ucwords(preg_replace("/_/", " ", $field['db'])) . '"');
        } else {
            return parent::set_input_form($row, $field);
        }
    }

    protected function set_form_validation($field, $action) {
        if ($field['db'] == 'department_id') {
            return "xss_clean|required|callback_check_department_id";
        } else {
            return parent::set_form_validation($field, $action);
        }
    }

    protected function datatable_customize_columns() {
        $this->columns[0]["visible"] = FALSE;
        $this->columns[1]["type"] = 'enum';
        $this->columns[1]["label"] = 'Department';
        $this->columns[1]["enums"] = $this->Department_model->get_enum_department();

        //echo '<pre>'; print_r($this->columns); die;       
        return $this->columns;
    }

    protected function datatable_field_record_formatter($field, $val, $column_index) {
        //echo "<pre>"; print_r($this->columns); die;
        if ($field == 'department_id') {
            return $this->Department_model->get_row_by_primary_key($val)->row()->department_name;
        } else {
            return parent::datatable_field_record_formatter($field, $val, $column_index);
        }
    }

    public function check_department_id($department_id) {
        if ($department_id == 0) {
            $this->form_validation->set_message('check_department_id', 'Please Select %s field');
            return FALSE;
        }

        $department = $this->Department_model->get_row_by_primary_key($department_id)->row();
        if (empty($department)) {
            $this->form_validation->set_message('check_department_id', 'The %s field is not valid');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

/* End of file user_group.php */
/* Location: ./application/controllers/backend/user_group.php */