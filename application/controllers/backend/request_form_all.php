<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'controllers/backend/request_form.php';

/**
 * @author Harry <if07087@gmail.com>
 * @since 11 Nov 2014 
 * @todo Request Form per/Department
 */
class Request_form_all extends Request_form {

    protected function datatable_total_rows() {
        switch ($this->session_user->department_id) {
            case '5': //OPERATIONAL MANAGER
                return $this->db->query("SELECT COUNT(1) AS `count` FROM {$this->_view} r WHERE r.proceed = \"YES\"")->row()->count; //return $this->db->query("SELECT COUNT(1) AS `count` FROM {$this->_view} r JOIN approval a ON a.request_number = r.request_number")->row()->count;
                break;
            case '2': //ADMIN
                return $this->db->query("SELECT COUNT(1) AS `count` FROM {$this->_view} r JOIN view_approval a ON a.request_number = r.request_number AND a.approval_department_id = \"5\"")->row()->count;
                break;
            case '6': //DIRECTOR
                return $this->db->query("SELECT COUNT(1) AS `count` FROM {$this->_view} r JOIN view_approval a ON a.request_number = r.request_number AND a.approval_department_id = \"2\"")->row()->count;
                break;
            default:
                return $this->db->query("SELECT COUNT(1) AS `count` FROM {$this->_view}")->row()->count;
                $row = array();
                break;
        }
    }

    protected function datatable_custom_query($where = "", $limit = 10, $offset = 0, $order = "") {
        $query = "SELECT r.* FROM {$this->_view} r ";
        switch ($this->session_user->department_id) {
            case '5': //OPERATIONAL MANAGER
                //$query .= "JOIN approval a ON a.request_number = r.request_number ";
                $query .= (!empty($where)) ? " WHERE {$where} AND r.proceed = \"YES\" " : " WHERE r.proceed = \"YES\" "; //Where
                break;
            case '2': //ADMIN
                $query .= "JOIN view_approval a ON a.request_number = r.request_number AND a.approval_department_id=\"5\" "; //JOIN Condition #already approved by OPERATIONAL MANAGER
                $query .= (!empty($where)) ? " WHERE {$where} " : " "; //Where
                break;
            case '6': //DIRECTOR
                $query .= "JOIN view_approval a ON a.request_number = r.request_number AND a.approval_department_id=\"2\" "; //JOIN Condition #already approved by ADMIN
                $query .= (!empty($where)) ? " WHERE {$where} " : " "; //Where
                break;
            default:
                $query .= (!empty($where)) ? " WHERE {$where} " : " "; //Where
                break;
        }
        $query .= (!empty($order)) ? " ORDER BY {$order} " : ""; //Order
        $query .= ($limit == -1) ? "" : " LIMIT {$limit} OFFSET {$offset} "; //limit Offfset

        return $this->db->query($query);
    }

    protected function datatable_customize_columns() {
        //echo "<pre>"; print_r($this->columns); die;
        if ($this->input->get('_view') && $this->input->get('_view') == 'request_detail') {
            $this->columns[0]["visible"] = FALSE;
            $this->columns[1]["label"] = 'Request#';
            $this->columns[2]["visible"] = FALSE;
            $this->columns[3]["label"] = 'Description';
            $this->columns[4]["label"] = 'Unit';
            $this->columns[5]["type"] = 'currency';
            $this->columns[6]["label"] = 'Type';
            $this->columns[7]["visible"] = FALSE;
            $this->columns[8]["label"] = 'COA';
            $this->columns[12]["visible"] = FALSE;
            $this->columns[13]["visible"] = FALSE;
            $this->columns[14]["visible"] = FALSE;
            $this->columns[15]["visible"] = FALSE;
            $this->columns[16]["visible"] = FALSE;
            $this->columns[17]["label"] = 'Requster';
            //echo "<pre>"; print_r($this->columns); die;
        } else {
            $this->load->model('Department_model');
            $this->columns[0]["label"] = 'Request#';
            $this->columns[2]["visible"] = FALSE;
            $this->columns[3]["visible"] = FALSE;
            $this->columns[4]["visible"] = FALSE;
            $this->columns[5]["visible"] = FALSE;
            $this->columns[6]["visible"] = FALSE;
            $this->columns[7]["label"] = 'Requester';
            $this->columns[8]["label"] = 'Department';
            $this->columns[8]["type"] = 'enum';
            $this->columns[8]["enums"] = $this->Department_model->get_options('department_name', 'department_id');
            $this->columns[9]["visible"] = FALSE;
            $this->columns[10]['type'] = 'currency';
        }
        return $this->columns;
    }

    /**
     * 
     * @todo calling from detail(), override from request_form::parent
     */
    public function check_request_id_for_current_user($val) {
        switch ($this->session_user->department_id) {
            case '5': //OPERATIONAL MANAGER
                $row = $this->db->query("SELECT * FROM {$this->_view} WHERE request_number = ? AND proceed = ?", array($val, "YES"))->row();
                //$row = $this->db->query("SELECT * FROM {$this->_view} r JOIN approval a ON a.request_number = r.request_number AND r.request_number = ?", array($val))->row();
                break;
            case '2': //ADMIN
                $row = $this->db->query("SELECT * FROM {$this->_view} JOIN view_approval a ON a.request_number = ? AND a.approval_department_id = ?", array($val, '5'))->row();
                break;
            case '6': //DIRECTOR
                $row = $this->db->query("SELECT * FROM {$this->_view} JOIN view_approval a ON a.request_number = ? AND a.approval_department_id = ?", array($val, '2'))->row();
                break;
            default:
                $row = array();
                break;
        }

        if (empty($row)) {
            $this->form_validation->set_message('check_request_id_for_current_user', '%s is not avaliable');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

/* End of file request_form_all.php */
/* Location: ./application/controllers/backend/request_form_all.php */