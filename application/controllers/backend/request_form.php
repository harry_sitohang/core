<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/datatable.php';

/**
 * @author Harry <if07087@gmail.com>
 * @since 11 Nov 2014 
 * @todo Request Form per/Department
 */
class Request_form extends Datatable {

    private $error_msg = array();

    public function __construct() {
        parent::__construct();
    }

    protected function init() {
        Backend::init();
        $this->model_name = ($this->input->get('_view') && $this->input->get('_view') == 'request_detail') ? 'Material_request_model' : 'Request_form_model'; //ucfirst($this->class_name)."_model"; //set model_name controller attribute
        $this->load->model($this->model_name); //load model controllers
        $this->_view = $this->{$this->model_name}->_view; //get 'view' name from controller model
        $this->_table = $this->{$this->model_name}->_table; //get 'table' name from controller model
        $this->primary_key = $this->{$this->model_name}->primary_key; //get 'primary key' from controller model

        $this->load->helper('text');

        $this->view->set("enable_datatable", $this->enable_datatable);

        if ($this->enable_datatable === TRUE) {
            $this->datatable_set_columns();  //Set Datatable column defenition
        }


        $this->load->helper('form');
        $this->load->model('Material_request_model');
        //echo '<pre>'; print_r($this->session_user); die;
    }

    public function index() {
        $this->view->set('breadcrumb_view', $this->view->load('breadcrumb/request_form_breadcrumb', '', TRUE));
        parent::index();
    }

    protected function set_class_assets() {
        $this->page_js[] = "{$this->_assets_js}request_form.js";
        $this->page_css[] = "{$this->_assets_css}pages/request_form.css";
        $this->view->set("pages_js", $this->page_js);
        $this->view->set("pages_css", $this->page_css);
    }

    protected function datatable_customize_columns() {
        //echo "<pre>"; print_r($this->columns); die;
        if ($this->input->get('_view') && $this->input->get('_view') == 'request_detail') {
            $this->columns[0]["visible"] = FALSE;
            $this->columns[1]["label"] = 'Request#';
            $this->columns[2]["visible"] = FALSE;
            $this->columns[3]["label"] = 'Description';
            $this->columns[4]["label"] = 'Unit';
            $this->columns[5]["type"] = 'currency';
            $this->columns[6]["label"] = 'Type';
            $this->columns[7]["visible"] = FALSE;
            $this->columns[8]["label"] = 'COA';
            $this->columns[11]["visible"] = FALSE;
            $this->columns[12]["visible"] = FALSE;
            $this->columns[13]["visible"] = FALSE;
            $this->columns[14]["visible"] = FALSE;
            $this->columns[15]["visible"] = FALSE;
            $this->columns[16]["visible"] = FALSE;
            $this->columns[17]["label"] = 'Requster';
            //echo "<pre>"; print_r($this->columns); die;
        } else {
            $this->columns[0]["label"] = 'Request#';
            $this->columns[2]["visible"] = FALSE;
            $this->columns[3]["visible"] = FALSE;
            $this->columns[4]["visible"] = FALSE;
            $this->columns[5]["visible"] = FALSE;
            $this->columns[6]["visible"] = FALSE;
            $this->columns[7]["label"] = 'Requester';
            $this->columns[8]["label"] = 'Department';
            $this->columns[8]['searchable'] = FALSE;
            $this->columns[8]['orderable'] = FALSE;
            $this->columns[9]["visible"] = FALSE;
            $this->columns[10]['type'] = 'currency';
        }
        return $this->columns;
    }

    protected function datatable_total_rows() {
        return $this->db->query("SELECT COUNT(1) AS `count` FROM {$this->_view} WHERE user_group_id = {$this->session_user->user_group_id}")->row()->count;
    }

    protected function datatable_custom_query($where = "", $limit = 10, $offset = 0, $order = "") {
        $query = "SELECT * FROM {$this->_view} ";
        $query .= (!empty($where)) ? " WHERE {$where} AND user_group_id = {$this->session_user->user_group_id} " : " WHERE user_group_id = {$this->session_user->user_group_id} "; //Where
        $query .= (!empty($order)) ? " ORDER BY {$order} " : ""; //Order
        $query .= ($limit == -1) ? "" : " LIMIT {$limit} OFFSET {$offset} "; //limit Offfset
        return $this->db->query($query);
    }

    protected function datatable_record_formatter($row) {
        $row = parent::datatable_record_formatter($row);
        if ($this->input->get('_view') && $this->input->get('_view') == 'request_detail') {
            $row->item_qty = !$row->item_qty ? '-' : $row->item_qty;
            $row->item_price = !$row->item_price ? '-' : $row->item_price;
        } else {
            $row->department_id = $row->department_name;
        }
        return $row;
    }

    protected function datatable_customize_actions($row) {
        return sprintf('<a href="%s" class="btn btn-sm btn-default"><i class="fa fa-search"></i> View</a>', "{$this->class_url}detail/{$row->request_number}");
    }

    public function add() {
        if ($this->input->is_ajax_request() && $this->input->post('items')) {//Submit
            $this->error_msg = $this->input->post('items');
            $this->load->library('form_validation');
            foreach ($this->input->post('items') as $item_key => $item) {
                foreach ($item as $field_key => $field) {
                    $this->error_msg[$item_key][$field_key] = "";
                    $label = ucwords(preg_replace("/_/i", ' ', $field_key));
                    $this->form_validation->set_rules("item_{$item_key}_{$field_key}", $field_key, "callback_check_item[{$item_key},{$field_key},{$label}]");
                }
            }
            if ($this->form_validation->run() == FALSE) {
                echo json_encode(array('status' => 'error', 'form_error' => $this->error_msg));
            } else {
                //Db Transaction
                $this->db->trans_start(TRUE);
                //Insert to table 'form_request'
                $this->db->insert('form_request', array(
                    'request_by' => $this->session_user->user_id
                ));
                $request_number = $this->db->insert_id();

                //Insert to table 'material_request'
                $items = $this->input->post('items');
                foreach ($items as $item_key => $item) {
                    $this->db->insert('material_request', array(
                        'request_number' => $request_number,
                        'item_description' => $item['item_description'],
                        'item_qty' => $item['item_qty'],
                        'item_type' => $item['item_type']
                    ));
                }
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    echo json_encode(array('status' => 'error', 'msg' => 'Database error occured, please re-submit requisition'));
                } else {
                    $this->db->trans_commit();
                    echo json_encode(array('status' => 'success', 'msg' => 'Successfully save Requisition', 'request_number' => $request_number));
                }
            }
        } else if ($this->input->is_ajax_request() && !$this->input->post('items')) {//Submit with empty items
            echo json_encode(array('status' => 'error', 'msg' => 'You cannot submit empty requisition, please add at least 1 item.'));
        } else { //View
            $this->set_class_assets();
            $this->view->set(array(
                'first_row_item' => $this->load_row_item(1)
            ));
            $this->view->content("pages/{$this->class_name}_add");
        }
    }

    public function detail($request_number = '') {
        $this->load->library('form_validation');
        if ($this->check_request_id_for_current_user($request_number)) {
            //load model data
            $this->load->model('User_model');
            $this->load->model('Department_model');
            $this->load->model('Quotation_model');
            $this->load->model('Request_order_model');
            $this->load->model('Delivery_order_model');

            //set general data
            $form_request = $this->Request_form_model->get_row_by_primary_key($request_number)->row();
            $lock_position = $this->Request_form_model->get_current_lock_position($request_number);

            //set view variable
            $this->view->set(array(
                'lock_position' => $lock_position,
                'lock_by' => $lock_position['label'],
                'form_request' => $form_request,
                'proceed_user_fullname' => $form_request->proceed == 'YES' ? $this->User_model->get_by_field('user_id', $form_request->proceed_by)->row()->user_fullname : '',
                'items' => $this->Material_request_model->get_result_detail(array('request_number' => $request_number))->result(),
                'unselected_items' => $this->Material_request_model->get_unselected_items($request_number)->result(), //items not found yet, in quotation items
                'requester' => $this->User_model->get_detailed_user($form_request->request_by)->row(),
                'operational_manager_approval' => $this->Request_form_model->get_approval($request_number, 5)->row(),
                'request_state' => $this->Request_form_model->get_request_state($request_number),
                'quots' => $this->Quotation_model->get_result_per_group_number_detailed($request_number),
                'purchase_orders' => $this->Request_order_model->get_result(array('request_number' => $request_number))->result(),
                'delivery_orders' => $this->Delivery_order_model->get_do($request_number),
                'invoices' => array()
            ));

            $this->page_js[] = "{$this->_general_assets}plugins/jquery-tags-input/jquery.tagsinput.min.js";
            $this->page_css[] = "{$this->_general_assets}plugins/jquery-tags-input/jquery.tagsinput.css";
            $this->page_js[] = "{$this->_assets_js}request_form_detail.js";
            $this->view->set("pages_js", $this->page_js);
            $this->view->set("pages_css", $this->page_css);
            $this->view->content("pages/{$this->class_name}_detail");
        } else {
            $this->view->set(array('err_title' => 'Invalid Request Number', 'err_msg' => 'Sorry, Either this request number : #' . $request_number . ' is not valid or you don\'t have access to it.'));
            $this->view->content("error");
        }
    }

    public function load_row_item($item_counter) {
        $this->view->set(array(
            'option_item_type' => form_dropdown('item_type[]', $this->Request_form_model->get_enum_item_type(), '0', 'class="form-control"'),
            'item_counter' => $item_counter
        ));
        if ($this->input->is_ajax_request()) {
            echo json_encode(array('row_item' => $this->view->load('pages/request_form/row_item', '', TRUE)));
        } else {
            return $this->view->load('pages/request_form/row_item', '', TRUE);
        }
    }

    public function load_items_table() {
        if (!$this->input->is_ajax_request() || !$this->input->post('request_number')) {
            echo json_encode(array('status' => 'error', 'msg' => 'Request Number required'));
            die;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules("request_number", "Request", "xss_clean|trim|required|callback_check_request_id_for_current_user");
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            echo json_encode(array('status' => 'success', 'data' => $this->view->load('pages/request_form/modal_proceed_with_items_content', array('items' => $this->Material_request_model->get_result('request_number', $this->input->post('request_number'))->result()), TRUE)));
        }
    }

    /**
     * @todo http://core.net/backend/request_form/detail/#request_number, when button 'save' foreach material/service row clicked
     */
    public function update_per_item() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'item_id',
                'label' => 'Item Id',
                'rules' => 'xss_clean|trim|required|callback_check_item_id'
            ),
            array(
                'field' => 'item_type',
                'label' => 'Type',
                'rules' => 'xss_clean|trim|required|callback_check_item_type'
            ),
            array(
                'field' => 'item_description',
                'label' => 'Description',
                'rules' => 'xss_clean|trim|required|callback_check_item_description'
            ),
            array(
                'field' => 'item_qty',
                'label' => 'Qty',
                'rules' => 'xss_clean|trim|callback_check_item_qty'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $form_error = array();
            foreach ($config as $field) {
                $form_error[$field['field']] = form_error($field['field'], '<span class="text-danger">', '</span>');
            }
            echo json_encode(array('status' => 'error', 'form_error' => $form_error, 'msg' => validation_errors()));
        } else {
            $this->db->update('material_request', array(
                'item_type' => $this->input->post('item_type'),
                'item_description' => $this->input->post('item_description'),
                'item_qty' => $this->input->post('item_qty')
                    ), array('item_id' => $this->input->post('item_id')));

            echo json_encode(array('status' => 'success', 'msg' => 'Successfully updated'));
        }
    }

    /**
     * 
     * @todo Delete per item : http://core.net/backend/request_form/detail/#request_number, when button 'remove' foreach material/service row clicked
     */
    public function delete_per_item() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'item_id',
                'label' => 'Item Id',
                'rules' => 'xss_clean|trim|required|callback_check_item_id'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            $this->db->delete('material_request', array('item_id' => $this->input->post('item_id')));
            echo json_encode(array('status' => 'success', 'msg' => 'Successfully removed'));
        }
    }

    /**
     * 
     * @todo :  http://core.net/backend/request_form/detail/#request_number, when button 'add' foreach material/service row clicked
     */
    public function add_per_item() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_number'
            ),
            array(
                'field' => 'item_type',
                'label' => 'Type',
                'rules' => 'xss_clean|trim|required|callback_check_item_type'
            ),
            array(
                'field' => 'item_description',
                'label' => 'Description',
                'rules' => 'xss_clean|trim|required|callback_check_item_description'
            ),
            array(
                'field' => 'item_qty',
                'label' => 'Qty',
                'rules' => 'xss_clean|trim|callback_check_item_qty'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $form_error = array();
            foreach ($config as $field) {
                $form_error[$field['field']] = form_error($field['field'], '<span class="text-danger">', '</span>');
            }
            echo json_encode(array('status' => 'error', 'form_error' => $form_error, 'msg' => validation_errors()));
        } else {
            $this->db->insert('material_request', array(
                'request_number' => $this->input->post('request_number'),
                'item_type' => $this->input->post('item_type'),
                'item_description' => $this->input->post('item_description'),
                'item_qty' => $this->input->post('item_qty')
            ));
            $item_id = $this->db->insert_id();
            echo json_encode(array('status' => 'success', 'msg' => 'Successfully updated', 'item_id' => $item_id));
        }
    }

    /**
     * @todo on click 'proceed' button on requisition form
     */
    public function approved_requisition_by_department() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_number|callback_check_valid_approved_requisition_by_department'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            $this->db->update('form_request', array(
                'proceed' => 'YES',
                'proceed_date' => date('Y-m-d H:i:s'),
                'proceed_by' => $this->session_user->user_id
                    ), array("request_number" => $this->input->post('request_number')));
            echo json_encode(array('status' => 'success', 'msg' => 'Successfully proceed', 'btn_approved' => '<a href="javascript:;" class="btn btn-sm green default"><i class="fa fa-check"></i> Already proceed by ' . $this->session_user->department_name . '</a> <a href="javascript:;" class="btn yellow btn-sm default"><i class="fa fa-lock"></i> Review by OPERATIONAL MANAGER</a>'));
        }
    }

    /**
     * 
     * @todo check if approval is already exist according to department_id => request_number
     */
    public function check_valid_approved_requisition_by_department($val) {
        if (!$this->Request_form_model->check_valid_proceed_request($val, $this->session_user->department_id)) {
            $this->db->select('u.user_fullname');
            $this->db->from('form_request r');
            $this->db->join('user u', 'u.user_id = r.proceed_by');
            $this->db->where(array('r.request_number' => $val));
            $row = $this->db->get()->row();
            $user_fullname = !empty($row) ? $row->user_fullname : '';
            $this->form_validation->set_message('check_valid_approved_requisition_by_department', 'Requisition already proceeded by <strong>' . $this->session_user->department_name . ' : ' . $user_fullname . '</strong>');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_request_number($val) {
        if (!$this->Request_form_model->check_valid_request_for_current_user_group($val, $this->session_user->user_group_id)) {
            $this->form_validation->set_message('check_request_number', '%s is not valid');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_item_id($val) {
        if (!$this->Material_request_model->check_valid_item_id_for_current_user_group($val, $this->session_user->user_group_id)) {
            $this->form_validation->set_message('check_item_id', '%s is not valid');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_item_type($val) {
        if ($val == '0') {
            $this->form_validation->set_message('check_item_type', 'Please select %s field');
            return FALSE;
        }

        if (preg_match("/^(MATERIAL|SERVICE)$/i", $val)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_item_type', 'The %s field is not valid');
            return FALSE;
        }
    }

    public function check_item_qty($val) {
        if (!$this->form_validation->is_natural_no_zero($val)) {//if ($this->input->post('item_type') == 'MATERIAL' && !$this->form_validation->is_natural_no_zero($val)) {
            $this->form_validation->set_message('check_item_qty', 'The %s field must be a positive number');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_request_id_for_current_user($val) {
        $this->db->select('*');
        $this->db->from('form_request');
        $this->db->where(array('request_number' => $val, 'request_by' => $this->session_user->user_id));
        $row = $this->db->get()->row();

        if (empty($row)) {
            $this->form_validation->set_message('check_request_id_for_current_user', '%s is not avaliable');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_item($val, $params) {
        $params = explode(',', $params);
        $item_key = $params[0];
        $field_key = $params[1];
        $label = $params[2];
        $items = $this->input->post('items');
        $val = $items[$item_key][$field_key];

        if ($field_key == 'item_type') {
            if ($val == '0') {
                $this->error_msg[$item_key][$field_key] = sprintf('Please select %s field', $label);
                $this->form_validation->set_message('check_item', 'Please select %s field');
                return FALSE;
            }

            if (preg_match("/^(MATERIAL|SERVICE)$/i", $val)) {
                return TRUE;
            } else {
                $this->error_msg[$item_key][$field_key] = sprintf('The %s field is not valid', $label);
                $this->form_validation->set_message('check_item', 'The %s field is not valid');
                return FALSE;
            }
        } else if ($field_key == 'item_description') {
            if (!empty($val)) {
                return TRUE;
            } else {
                $this->error_msg[$item_key][$field_key] = sprintf('The %s field is required', $label);
                $this->form_validation->set_message('check_item', 'The %s field is required');
                return FALSE;
            }
        } else if ($field_key == 'item_qty') {
            if (!$this->form_validation->is_natural_no_zero($val)) {//if ($items[$item_key]['item_type'] == 'MATERIAL' && !$this->form_validation->is_natural_no_zero($val)) {
                $this->error_msg[$item_key][$field_key] = sprintf('The %s field must be a positive number', $label);
                $this->form_validation->set_message('check_item', 'The %s field must be a positive number');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->error_msg[$item_key][$field_key] = sprintf('Invalid %s', $label);
            $this->form_validation->set_message('check_item', 'Invalid ' . $field_key);
            return FALSE;
        }
    }

    /**
     * @todo on click btn 'approve-request' : http://core.net/backend/request_form_all/detail/xxx
     */
    public function approved_requisition() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_id_for_current_user|callback_check_valid_approved_requisition'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            switch ($this->session_user->department_id) {
                case '6': //DIRECTOR
                    $this->db->update('request_order', array('process_date' => date('Y-m-d H:i:s')), array('request_number' => $this->input->post('request_number')));
                    break;
                default:
                    break;
            }

            $this->db->insert('approval', array(
                'request_number' => $this->input->post('request_number'),
                'approved_by' => $this->session_user->user_id,
            ));
            $approval_id = $this->db->insert_id();
            $lock_position = $this->Request_form_model->get_current_lock_position($this->input->post('request_number'));
            echo json_encode(array('status' => 'success', 'msg' => 'Successfully Approve/Sign Requisition', 'approval_id' => $approval_id, 'current_lock_position' => $lock_position['label']));
            //echo json_encode(array('status' => 'success', 'msg' => 'Successfully Approve/Sign Requisition', 'approval_id' => $approval_id, 'btn_approved' => '<a href="javascript:;" class="btn btn-warning btn-sm" data-name="unlock-request" data-loading-text="loading..." data-request-number="' . $this->input->post('request_number') . '"><i class="fa fa-lock"></i> Completion Quotation by Admin </a>'));
        }
    }

    /**
     * @todo Callback check is approved/lock requistion action is valid for current user
     */
    public function check_valid_approved_requisition($val) {
        switch ($this->session_user->department_id) {
            case '5': //OPERATIONAL MANAGER
                //No action
                break;
            case '2': //ADMIN
                $this->load->model('Quotation_model');
                if (!$this->Quotation_model->is_complete_quotation_item($val)) {
                    $this->form_validation->set_message('check_valid_approved_requisition', 'Please complete quotation for all items, before doing approval');
                    return FALSE;
                }
                break;
            case '6': //DIRECTOR
                //all item should has been associated to quotation
                $this->load->model('Request_order_model');
                if (!$this->Request_order_model->is_complete_po($val)) {
                    $this->form_validation->set_message('check_valid_approved_requisition', 'Please complete all PO/WO, before doing approval');
                    return FALSE;
                }
                break;
            default:
                break;
        }


        if (!$this->Request_form_model->check_valid_approved_requisition($val, $this->session_user->department_id)) {
            $this->db->select('a.user_fullname');
            $this->db->from('view_approval a');
            $this->db->where(array('a.request_number' => $val, 'a.approval_department_id' => $this->session_user->department_id));
            $user_fullname = $this->db->get()->row()->user_fullname;
            $this->form_validation->set_message('check_valid_approved_requisition', 'Requisition already proceeded by <strong>' . $this->session_user->department_name . ' : ' . $user_fullname . '</strong>');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * @todo on click btn 'unlock-request' : http://core.net/backend/request_form_all/detail/xxx
     */
    public function unlock_requisition() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_id_for_current_user|callback_check_valid_unlock_requisition'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            $lock_position = $this->Request_form_model->get_current_lock_position($this->input->post('request_number'));
            echo json_encode(array('status' => 'success', 'msg' => 'Successfully unlock Requisition', 'current_lock_position' => $lock_position['label']));
        }
    }

    /**
     * @todo Callback check is unclock requistion action is valid for current user
     */
    public function check_valid_unlock_requisition($val) {
        //Check State ex : 5,2,6
        $request_state = $this->Request_form_model->get_request_state($val);
        if (preg_match("/({$this->session_user->department_id},|{$this->session_user->department_id}$)/", $request_state)) {
            //set to empty because already this request number already approved by current user, so it cannot be unlock.
            $this->form_validation->set_message('check_valid_unlock_requisition', 'Request is locked');
            return FALSE;
        }

        switch ($this->session_user->department_id) {
            case '5': //OPERATIONAL MANAGER
                $row = $this->db->query("SELECT * FROM {$this->_view} WHERE request_number = ? AND proceed = ?", array($val, "YES"))->row(); //check is requistion has been proceed
                if (empty($row)) {
                    $this->form_validation->set_message('check_valid_unlock_requisition', 'Requesition is not proceed yet by requester department.');
                    return FALSE;
                } else {
                    $this->db->update('form_request', array('proceed' => 'NO'), array('request_number' => $this->input->post('request_number')));
                    return TRUE;
                }
                break;
            case '2': //ADMIN
                if (!preg_match("/(5,|5$)/", $request_state)) { //If not approved by OPERATINAL MANAGER
                    $this->form_validation->set_message('check_valid_unlock_requisition', 'Requesition is not approved yet by OPERATIONAL MANAGER');
                    return FALSE;
                } else {
                    //Remove all quotation and CAPEX + Approval data
                    $this->db->where('request_number', $val);
                    $this->db->where_in('approved_by', array('5'));
                    $this->db->delete('approval');

                    $this->db->delete('quotation', array('request_number' => $val));
                    $this->db->update('material_request', array('capex_number' => "-"), array('request_number' => $val));
                    return TRUE;
                }
                break;
            case '6': //DIRECTOR
                if (!preg_match("/(2,|2$)/", $request_state)) { //If not approved by ADMIN
                    $this->form_validation->set_message('check_valid_unlock_requisition', 'Requesition is not approved yet by ADMIN');
                    return FALSE;
                } else {
                    //Remove selected quotation + Approval data
                    $this->db->where('request_number', $val);
                    $this->db->where_in('approved_by', array('2'));
                    $this->db->delete('approval');

                    $this->db->delete('request_order', array('request_number' => $val));
                    return TRUE;
                }
                break;
            default:
                return FALSE;
                break;
        }
    }

    /**
     * @todo Search supplier for 'add|update quotation'
     */
    public function search_supplier() {
        $this->load->library('form_validation');
        $this->load->helper('text');
        $config = array(
            array(
                'field' => 'search_supplier_by',
                'label' => 'Search by',
                'rules' => 'xss_clean|trim|required|callback_check_search_supplier_by'
            ),
            array(
                'field' => 'search_supplier_keyword',
                'label' => 'Search Keyword',
                'rules' => 'xss_clean|trim|required'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            //Setting error validation foreach field
            $form_error = array();
            foreach ($config as $field) {
                $form_error[$field['field']] = form_error($field['field'], '<span class="help-inline error block text-danger">', '</span>');
            }
            echo json_encode(array('status' => 'error', 'msg' => validation_errors(), 'form_error' => $form_error));
        } else {
            $this->db->select("*");
            $this->db->from('supplier');
            $this->db->like($this->input->post('search_supplier_by'), $this->input->post('search_supplier_keyword'));
            $result = $this->view->load('pages/request_form/detail/modal_list_supplier_result', array('result' => $this->db->get()->result()), TRUE);
            echo json_encode(array('status' => 'success', 'result' => $result));
        }
    }

    public function check_search_supplier_by($val) {
        if (!$val) {
            $this->form_validation->set_message('check_search_supplier_by', 'Please select %s field');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * @todo Add group quotations with price list of items
     */
    public function add_group_quotation() {
        $suppliers = $this->input->post('supplier_id');
        $items_price = $this->input->post('item_price');
        $request_number = $this->input->post('request_number');

        $this->load->library('form_validation');
        $config = array();


        //Set rules for items price
        foreach ($items_price as $supplier_id => $item_price) {
            foreach ($item_price as $item_id => $price) {
                $_POST["item_price_{$supplier_id}_{$item_id}"] = $price;
                $config[] = array(
                    'field' => "item_price_{$supplier_id}_{$item_id}",
                    'label' => "item_price_{$supplier_id}_{$item_id}",
                    'rules' => "required|is_natural_no_zero"
                );
            }
        }

        //Set rules for quotation file
        foreach ($suppliers as $supplier_id) {
            $_POST["quotation_file_{$supplier_id}"] = "quotation_file_{$supplier_id}";
            $config[] = array(
                'field' => "quotation_file_{$supplier_id}",
                'label' => "quotation_file_{$supplier_id}",
                'rules' => "required|callback_check_upload_quotation_file[quotation_file_{$supplier_id}]"
            );
        }
        //print_r($config);       print_r($_POST); print_r($_FILES);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === true) {
            $this->load->model('Quotation_model');
            $this->load->model('Request_form_model');

            $group_number = $this->Quotation_model->get_next_group_number($request_number); //Get max group number + 1
            //Insert table quotation
            foreach ($suppliers as $supplier_id) {
                $this->db->insert('quotation', array(
                    'request_number' => $request_number,
                    'supplier_id' => $supplier_id,
                    'payment_step' => 'FULL_PAYMENT', //default
                    'quotation_file' => $_POST["quotation_file_{$supplier_id}_upload_data"]['file_name'],
                    'group_number' => $group_number
                ));
                $quotation_id = $this->db->insert_id(); //new quotation id
                //Insert to table quotation item
                foreach ($items_price[$supplier_id] as $item_id => $price) {
                    $this->db->insert('quotation_item', array(
                        'quotation_number' => $quotation_id,
                        'item_id' => $item_id,
                        'price' => $price
                    ));
                }
            }
            echo json_encode(array('status' => 'success', 'msg' => 'Successfully add quotation, for selected items.', 'group_number' => $group_number, 'tr' => $this->view->load("pages/request_form/detail/quotation_detail_box_table_tr_added", array('group_number' => $group_number, 'quotations' => $this->Quotation_model->get_row_per_group_number_detailed($request_number, $group_number), 'form_request' => $this->Request_form_model->get_row_by_primary_key($request_number)->row()), TRUE)));
        } else {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        }
    }

    /**
     * @todo Check upload file quotation status
     */
    public function check_upload_quotation_file($val, $field_name) {
        $this->load->library('upload');
        $this->upload->initialize(array(
            'upload_path' => './uploads/quotation/',
            'allowed_types' => 'gif|jpg|jpeg|png|bmp|pdf',
            'encrypt_name' => TRUE
        ));

        if (!$this->upload->do_upload($field_name)) {
            $this->form_validation->set_message('check_upload_quotation_file', $this->upload->display_errors());
            return FALSE;
        } else {
            $_POST["{$field_name}_upload_data"] = $this->upload->data();
            return TRUE;
        }
    }

    /**
     * @todo get list items for request, called from ajax when 'add group quotation' button clickedd
     */
    public function get_list_request_items() {
        $this->load->model('Material_request_model');
        $result = $this->Material_request_model->get_result(array('request_number' => $this->input->post('request_number')))->result();
        $data = array();
        foreach ($result as $row) {
            $data[$row->item_id]['desc'] = $row->item_description;
            $data[$row->item_id]['type'] = $row->item_type;
        }
        echo json_encode($data);
    }

    /**
     * @todo remove quotation group
     */
    public function remove_group_quotation_row() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_id_for_current_user'
            ),
            array(
                'field' => 'group_number',
                'label' => 'Group Number',
                'rules' => 'xss_clean|trim|required|callback_check_group_number_for_current_user'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => 'Invalid request', 'err' => validation_errors()));
        } else {
            $this->db->delete('quotation', array(
                'request_number' => $this->input->post('request_number'),
                'group_number' => $this->input->post('group_number'),
            ));
            echo json_encode(array('status' => 'success', 'msg' => 'successfully remove your quotation'));
        }
    }

    /**
     * @todo Check is valid group number for current user
     */
    public function check_group_number_for_current_user($group_number) {
        $this->load->model('Quotation_model');
        if ($this->Quotation_model->check_group_number_for_current_user($this->input->post('request_number'), $group_number)) {
            return TRUE;
        }

        $this->form_validation->set_message('check_group_number_for_current_user', '%s is not avaliable');
        return FALSE;
    }

    /**
     * @todo Set item capex, called from modal-capex on change select[name=set-capex]
     */
    public function update_capex_data() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_id_for_current_user'
            ),
            array(
                'field' => 'item_id',
                'label' => 'Item Id',
                'rules' => 'xss_clean|trim|required|callback_check_item_id_for_request_number'
            ),
            array(
                'field' => 'state',
                'label' => 'Capex State',
                'rules' => 'xss_clean|trim|required'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => 'Invalid request', 'err' => validation_errors()));
        } else {
            $this->db->update('material_request', array(
                'capex_number' => $this->input->post('state') == 'YES' ? $this->input->post('item_id') : '-'
                    ), array('item_id' => $this->input->post('item_id')));
            echo json_encode(array('status' => 'success', 'msg' => 'successfully update capex data for selected item'));
        }
    }

    /**
     * @todo check is item_id is exist in selected request number
     */
    public function check_item_id_for_request_number($val) {
        $this->load->model("Request_form_model");
        if ($this->Request_form_model->check_item_id_for_request_number($val, $this->input->post('request_number'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_item_id_for_request_number', '%s is not avaliable for this requesition');
            return FALSE;
        }
    }

    /**
     * @todo Generate PO, by director in po tab, when  clicked 'Select this supplier btn' => [data-name=select-quot]
     */
    public function generate_po() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_id_for_current_user'
            ),
            array(
                'field' => 'quotation_number',
                'label' => 'Quotation Number',
                'rules' => 'xss_clean|trim|required|callback_check_quotation_number_for_request_number'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => 'Invalid request', 'err' => validation_errors()));
        } else {
            $this->load->model("Quotation_model");
            $this->load->model("Request_order_model");

            //get quotation data
            $quot = $this->Quotation_model->get_result(array('quotation_number' => $this->input->post('quotation_number')))->row();
            $request_type = $this->Quotation_model->get_request_type($this->input->post('quotation_number'));

            //delete the old data
            $this->db->delete('request_order', array('request_number' => $quot->request_number, 'group_number' => $quot->group_number));

            //insert new data
            $this->db->insert('request_order', array(
                'request_number' => $quot->request_number,
                'quotation_number' => $quot->quotation_number,
                'group_number' => $quot->group_number,
                'request_type' => $request_type,
                'created_by' => $this->session_user->user_id
            ));
            $request_order_number = $this->db->insert_id();

            //loop quotation : HAS BEEN HANDLED USING TRIGGER
//            $this->load->model('Quotation_item_model');
//            $items = $this->Quotation_item_model->get_result(array('quotation_number' => $quot->quotation_number))->result();
//            foreach($items as $item){
//                $this->db->update('material_request', array('choosed_quotation_item_id' => $item->quotation_item_id), array('item_id' => $item->item_id));
//            }

            echo json_encode(array(
                'status' => 'success',
                'msg' => 'successfully select supplier',
                'request_order_number' => $request_order_number,
                'request_order_link' => '<a data-name="link-po" class="pull-right" style="margin:5px;" href="http://core.net/backend/request_form_all/pdf/po/' . $request_order_number . '" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> ' . $request_type . '</a>',
                'count_po' => $this->Request_order_model->get_count()
            ));
        }
    }

    /**
     * @todo check is quotation number is exist for request number
     */
    public function check_quotation_number_for_request_number($val) {
        $this->load->model("Quotation_model");
        if ($this->Quotation_model->check_quotation_number_for_request_number($val, $this->input->post('request_number'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_quotation_number_for_request_number', '%s is not avaliable for this requisition');
            return FALSE;
        }
    }

    /**
     * @todo Generate 
     */
    public function reload_requisition_table() {
        $request_number = $this->input->post('request_number');
        $this->load->model('Request_form_model');
        $this->load->model('Material_request_model');
        echo json_encode($this->view->load("pages/request_form/detail/requisition_form_detail_table_approved_by_department", array(
                    'form_request' => $this->Request_form_model->get_row_by_primary_key($request_number)->row(),
                    'items' => $this->Material_request_model->get_result_detail(array('request_number' => $request_number))->result()
                        ), TRUE));
    }

    /**
     * @todo Reload Table DO
     */
    public function reload_do_table() {
        $request_number = $this->input->post('request_number');
        $this->load->model('Delivery_order_model');
        $this->load->model('Request_form_model');
        echo json_encode($this->view->load("pages/request_form/detail/do_detail_box_table", array(
                    'delivery_orders' => $this->Delivery_order_model->get_do($request_number),
                    'request_state' => $this->Request_form_model->get_request_state($request_number)
                        ), TRUE));
    }

    /**
     * @todo view do
     */
    public function view_do() {
        $this->load->library('form_validation');
        $this->load->model('Request_order_model');
        $config = array(
            array(
                'field' => 'request_order_number',
                'label' => 'Request Order Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_order_number'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(validation_errors());
        } else {
            $this->load->model('Delivery_order_model');
            echo json_encode($this->view->load("pages/request_form/detail/modal_view_do_content", array(
                        'do' => $this->Delivery_order_model->get_result_detail(array('request_order_number' => $this->input->post('request_order_number')))->row()
                            ), TRUE));
        }
    }

    /**
     * @todo get list item for coa & asset number
     */
    public function get_do_items() {
        $this->load->library('form_validation');
        $this->load->model('Request_order_model');
        $this->load->model('Delivery_order_model');
        $config = array(
            array(
                'field' => 'request_order_number',
                'label' => 'Request Order Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_order_number'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => 'Invalid request', 'err' => validation_errors()));
        } else {
            $this->load->model('Delivery_order_model');
            echo json_encode($this->view->load("pages/request_form/detail/modal_input_do_items", array(
                        'items' => $this->Delivery_order_model->get_do_items($this->input->post('request_order_number'))->result()
                            ), TRUE));
        }
    }

    /**
     * @todo input do
     */
    public function input_do() {
        $this->load->library('form_validation');
        $this->load->model('Request_order_model');
        $this->load->model('Delivery_order_model');
        $this->load->model('Material_request_model');
        $_POST['do_file'] = true;
        $_POST['coa'] = true;
        $_POST['asset'] = true;
        $config = array(
            array(
                'field' => 'request_order_number',
                'label' => 'Request Order Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_order_number'
            ),
            array(
                'field' => 'do_number',
                'label' => 'Delivery Order Number',
                'rules' => 'xss_clean|trim|required|callback_check_do_number'
            ),
            array(
                'field' => 'review_comment',
                'label' => 'Review Comment',
                'rules' => 'xss_clean|trim|required|min_length[5]'
            ),
            array(
                'field' => 'delivery_score',
                'label' => 'Score',
                'rules' => 'xss_clean|trim|required|callback_check_delivery_score'
            ),
            array(
                'field' => 'do_file',
                'label' => 'Delivery Order File',
                'rules' => 'xss_clean|trim|required|callback_check_do_file'
            ),
            array(
                'field' => 'coa',
                'label' => 'COA number',
                'rules' => 'xss_clean|trim|required|callback_check_co'
            ),
            array(
                'field' => 'asset',
                'label' => 'Asset number',
                'rules' => 'xss_clean|trim|required|callback_check_asset'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            $upload_data = $this->upload->data();
            $this->db->insert('delivery_order', array(
                'request_order_number' => $this->input->post('request_order_number'),
                'do_number' => $this->input->post('do_number'),
                'do_file' => $upload_data['file_name'],
                'received_by' => $this->session_user->user_id,
                'delivery_score' => $this->input->post('delivery_score') / 100,
                'review_comment' => $this->input->post('review_comment')
            ));
            $do_id = $this->db->insert_id();

            //loop update asset number
            foreach ($this->input->post('asset_number') as $item_id => $asset_number) {
                $this->db->update('material_request', array('asset_number' => $asset_number), array('item_id' => $item_id));
            }

            //loop update coa number
            foreach ($this->input->post('coa_number') as $item_id => $coa_number) {
                $this->db->update('material_request', array('coa_number' => $coa_number), array('item_id' => $item_id));
            }
            echo json_encode(array('status' => 'success', 'msg' => 'Succesfully Save DO', 'do_id' => $do_id));
        }
    }

    /**
     * @todo Callback check request_order_number : input_do
     */
    public function check_request_order_number($val) {
        if ($this->Request_order_model->get_count(array('request_order_number' => $val))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_request_order_number', 'The %s field is not valid');
            return FALSE;
        }
    }

    /**
     * @todo Callback check do_number : input_do
     */
    public function check_do_number($val) {
        if (!$this->Delivery_order_model->get_count(array('do_number' => $val))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_do_number', 'The %s field is not valid');
            return FALSE;
        }
    }

    /**
     * @todo Callback check delivery_score : input_do
     */
    public function check_delivery_score($val) {
        if ($val >= 0 && $val <= 100) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_delivery_score', 'The %s field is not valid must in range : 0 - 100');
            return FALSE;
        }
    }

    /**
     * @todo Callback check do file : input_do
     */
    public function check_do_file() {
        $this->load->library('upload');
        $this->upload->initialize(array(
            'upload_path' => './uploads/do/',
            'allowed_types' => 'gif|jpg|jpeg|png|bmp|pdf',
            'encrypt_name' => TRUE
        ));

        if (!$this->upload->do_upload('do_file')) {
            $this->form_validation->set_message('check_do_file', $this->upload->display_errors());
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * @todo check coa : input do
     */
    public function check_asset() {
        if (!$this->input->post('asset_number')) {
            $this->form_validation->set_message('check_asset', 'Complete all Asset Number');
            return FALSE;
        }

        $str = '';
        foreach ($this->input->post('asset_number') as $val) {
            if (!is_numeric($val)) {
                $this->form_validation->set_message('check_asset', 'Invalid Asset Number');
                return FALSE;
            }
            if (preg_match("/{$val},/", $str)) {
                $this->form_validation->set_message('check_asset', 'Duplicate Asset Number');
                return FALSE;
            }
            if ($this->Material_request_model->get_count(array('asset_number' => $val))) {
                $this->form_validation->set_message('check_asset', 'Asset Number Already Exist in Database');
                return FALSE;
            }
            $str .= $val . ',';
        }

        return TRUE;
    }

    /**
     * @todo check coa : input do
     */
    public function check_co() {
        if (!$this->input->post('coa_number')) {
            $this->form_validation->set_message('check_co', 'Complete all COA Number');
            return FALSE;
        }

        $str = '';
        foreach ($this->input->post('coa_number') as $val) {
            if (!preg_match("/(\d{1})-(\d{1,})/", $val)) {
                $this->form_validation->set_message('check_co', 'Invalid COA Number');
                return FALSE;
            }
            if (preg_match("/{$val},/", $str)) {
                $this->form_validation->set_message('check_co', 'Duplicate Asset Number');
                return FALSE;
            }
            if ($this->Material_request_model->get_count(array('coa_number' => $val))) {
                $this->form_validation->set_message('check_co', 'COA Number Already Exist in Database');
                return FALSE;
            }
            $str .= $val . ',';
        }

        return TRUE;
    }

    /**
     * @todo Generate pdf
     */
    public function pdf($type = '', $id = 0) {
        //Set $_POST['request_number]
        switch ($type) {
            case 'capex':
                $this->load->model('Material_request_model');
                $item = $this->Material_request_model->get_result(array('item_id' => $id, "capex_number <> " => "-", "item_type" => "MATERIAL"))->row();
                $_POST['request_number'] = empty($item) ? 0 : $item->request_number;
                break;
            case 'material_request':
                $_POST['request_number'] = $id;
                break;
            case 'po':
            case 'wo':
                $this->load->model('Request_order_model');
                $req_order = $this->Request_order_model->get_result(array('request_order_number' => $id))->row();
                $this->view->set('req_order', $req_order);
                $_POST['request_number'] = empty($req_order) ? 0 : $req_order->request_number;
                break;
            default:
                die('Invalid Url Parameter');
                break;
        }

        //Form validation - check request number is valid or not
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'request_number',
                'label' => 'Request Number',
                'rules' => 'xss_clean|trim|required|callback_check_request_id_for_current_user'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            unset($_POST['request_number']);
            die("Requisition not available");
        }

        //Generate Pdf
        $this->load->library('Mypdf');
        switch ($type) {
            case 'po':
            case 'wo':
                $this->mypdf->initialize(array('pdf_header_title' => 'PT. SYSTRAN ELECTRONICS ZONE', 'pdf_header_string' => 'Nongsapura Ferry Terminal, Jl. Hang Leklu, Nongsa, Batam, Indonesia  Tel. : (0778) 761777 Fax : (0778) 761553'));
                break;
            default:
                break;
        }

        //Set General data
        $this->load->model('Request_form_model');
        $form_request = $this->Request_form_model->get_result_detail(array('request_number' => $this->input->post('request_number')))->row();
        $this->view->set('form_request', $form_request);

        switch ($type) {
            case 'capex':
                $this->load->model('Quotation_model');
                $quots = $this->Quotation_model->get_result_detail(array("item_id" => $id))->result();
                $choosed_quot = $this->Quotation_model->get_selected_quotation($id)->row();
                $approval_admin = $this->Request_form_model->get_approval($this->input->post('request_number'), '2')->row(); //Approval by Admin Asst
                $approval_director = $this->Request_form_model->get_approval($this->input->post('request_number'), '6')->row(); //Approval by Director
                $req_order = empty($choosed_quot) ? 0 : $this->Quotation_model->get_po_wo_number($choosed_quot->quotation_number)->row();
                $html = $this->view->load("pages/request_form/detail/pdf/{$type}", array(
                    "item" => $item,
                    "quots" => $quots,
                    "choosed_quot" => $choosed_quot,
                    "approval_admin" => $approval_admin,
                    "approval_director" => $approval_director,
                    "req_order" => $req_order
                        ), TRUE);
                //echo '<pre>'; print_r($choosed_quot); die;
                $this->mypdf->generate($html, "{$type}_{$id}.pdf");
                break;
            case 'material_request':
                $this->load->model('Material_request_model');
                $result = $this->Material_request_model->get_result(array('request_number' => $id, 'item_type' => 'MATERIAL'))->result();
                $approval = $this->Request_form_model->get_approval($id, '5')->row(); //Is already approved by OPERATIONAL MANAGER?
                $html = $this->view->load("pages/request_form/detail/pdf/{$type}", array('result' => $result, 'approval' => $approval), TRUE);
                $this->mypdf->generate($html, "{$type}_{$id}.pdf");
                break;
            case 'po':
            case 'wo':
                $this->view->set('result', $this->Request_order_model->generate_request_order_pdf_result($id)->result());
                $html = $this->view->load("pages/request_form/detail/pdf/{$type}", "", TRUE);
                $this->mypdf->generate($html, "{$type}_{$id}.pdf");
                break;
            default:
                die('Invalid Url Parameter');
                break;
        }
    }

    /**
     * @todo View/Donwload Quotaiton File 
     */
    public function view_quotation($filename = 'xxx.jpg') {
        $file_path = "./uploads/quotation/{$filename}";
        $this->view_file($filename, $file_path);
    }

    /**
     * @todo View/Donwload Do File 
     */
    public function view_delivery_order($filename = 'xxx.jpg') {
        $file_path = "./uploads/do/{$filename}";
        $this->view_file($filename, $file_path);
    }

    /**
     * @todo View/Donwload Quotaiton File 
     */
    public function view_invoice($filename = 'xxx.jpg') {
        $file_path = "./uploads/invoice/{$filename}";
        $this->view_file($filename, $file_path);
    }

    /**
     * @todo Common view file
     */
    public function view_file($filename, $file_path) {
        if (file_exists($file_path)) {
            // Grab the file extension
            $x = explode('.', $filename);
            $extension = end($x);

            $mime = preg_match("/(gif|jpg|jpeg|png|bmp)/i", $extension) ? "image/{$extension}" : "application/octet-stream";
            $mime = preg_match("/(pdf)/i", $extension) ? "application/pdf" : $mime;
        } else {
            echo "File Not found";
            die;
        }


//        $data = file_get_contents("./uploads/quotation/{$filename}"); // Read the file's contents
//        if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") !== FALSE) {
//            header('Content-Type: "application/octet-stream"');
//            header('Content-Disposition: inline; filename="' . $filename . '"'); //header('Content-Disposition: attachment; filename="' . $filename . '"');
//            header('Expires: 0');
//            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
//            header("Content-Transfer-Encoding: binary");
//            header('Pragma: public');
//            header("Content-Length: " . strlen($data));
//        } else {
//            header('Content-Type: "application/octet-stream"');
//            header('Content-Disposition: inline; filename="' . $filename . '"'); //header('Content-Disposition: attachment; filename="' . $filename . '"');
//            header("Content-Transfer-Encoding: binary");
//            header('Expires: 0');
//            header('Pragma: no-cache');
//            header("Content-Length: " . strlen($data));
//        }


        $fp = fopen($file_path, 'rb');

        // send the right headers
        header("Content-Type: " . $mime); //header("Content-Type: image/png");
        header("Content-Length: " . filesize($file_path));


        // dump the picture and stop the script
        fpassthru($fp);
        exit;
    }

    public function json() {
        //echo json_decode('{"status":"success","msg":"Successfully add quotation, for selected items.","group_number":2,"tr":"<td>2.<\/td>\r\n<td data-for=\"list-supplier\">\r\n    <ul class=\"list-unstyled\">\r\n                    <li>PT. Cipta Surya Graha <a href=\"http:\/\/core.net\/backend\/request_form_all\/view_quotation\/1df28d52b000d1b8d0beaaba7b3cc4a4.pdf\" target=\"_blank\"><span class=\"glyphicon glyphicon-paperclip\"><\/span><\/a><\/li>\r\n            <\/ul>\r\n<\/td>\r\n<td data-for=\"items\">\r\n            PT. Cipta Surya Graha <a href=\"http:\/\/core.net\/backend\/request_form_all\/view_quotation\/1df28d52b000d1b8d0beaaba7b3cc4a4.pdf\" target=\"_blank\"><span class=\"glyphicon glyphicon-paperclip\"><\/span><\/a>\r\n        <ul class=\"list-unstyled\">\r\n                            <li><input type=\"checkbox\" value=\"63\" name=\"set-items\" data-item-type=\"<div style=\"border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;\">\n\n<h4>A PHP Error was encountered<\/h4>\n\n<p>Severity: Notice<\/p>\n<p>Message:  Undefined index: item_type<\/p>\n<p>Filename: detail\/quotation_detail_box_table_tr_added.php<\/p>\n<p>Line Number: 14<\/p>\n\n<\/div>\" data-item-description=\"Perbaikan plafon\/drop ceiling & pengecetan ulang(cat warna putih)\" checked=\"CHECKED\" disabled=\"DISABLED\"> Perbaikan plafon\/drop ceiling & pengecetan ulang(cat warna putih) : <div style=\"border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;\">\n\n<h4>A PHP Error was encountered<\/h4>\n\n<p>Severity: Notice<\/p>\n<p>Message:  Undefined index: item_type<\/p>\n<p>Filename: detail\/quotation_detail_box_table_tr_added.php<\/p>\n<p>Line Number: 14<\/p>\n\n<\/div> : 1,000,000 IDR<\/li>\r\n                    <\/ul>\r\n    <\/td>\r\n    <td>\r\n        <a href=\"javascript:;\" data-loading-text=\"Loading...\" name=\"remove-quotation\" class=\"btn btn-danger btn-sm\" data-status=\"added\" data-group-number=\"2\" data-request-number=\"19\"><i class=\"fa fa-trash\"><\/i>&nbsp;&nbsp;remove&nbsp;<\/a>\r\n    <\/td>\r\n<pre>Array\n(\n    [6] => Array\n        (\n            [quotation_number] => 6\n            [request_number] => 19\n            [supplier_id] => 4\n            [payment_step] => FULL_PAYMENT\n            [quotation_file] => 1df28d52b000d1b8d0beaaba7b3cc4a4.pdf\n            [created_date] => 2014-12-07 08:50:01\n            [group_number] => 2\n            [supplier_name] => PT. Cipta Surya Graha\n            [items] => Array\n                (\n                    [0] => Array\n                        (\n                            [quotation_item_id] => 14\n                            [quotation_number] => 6\n                            [item_id] => 63\n                            [price] => 1000000\n                            [item_description] => Perbaikan plafon\/drop ceiling & pengecetan ulang(cat warna putih)\n                        )\n\n                )\n\n        )\n\n)\n"}');
    }

}

/* End of file request_form.php */
/* Location: ./application/controllers/backend/request_form.php */