<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/backend.php';

/**
 * @author Harry <if07087@gmail.com>
 * @since 10 Oct 2014 
 * @todo Department Annual Budget Management
 */
class Annual_budget extends Backend {

    public function __construct() {
        parent::__construct();
    }

    protected function init() {
        parent::init();
        $this->load->model('Annual_budget_model');
        $this->load->model('Department_model');
    }

    public function index() {
        //set asset
        $this->page_js[] = "{$this->_assets_js}annual_budget.js";

        //get min_year data & department list
        $row_min_year = $this->db->query('SELECT MIN(`year`) AS `min_year` FROM annual_budget')->row();
        $min_year = empty($row_min_year) ? date('Y') : $row_min_year->min_year;
        $this->view->set(array(
            'departments' => $this->Department_model->get_result()->result(),
            'min_year' => $min_year
        ));

        parent::index();
    }

    public function load_amount() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('department_id', 'Department', 'xss_clean|trim|required|callback_check_department_id');
        $this->form_validation->set_rules('year', 'Year', 'xss_clean|trim|required|callback_check_budget_year');
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            $amount = $this->input->post('amount_return');
            unset($_POST['amount_return']);
            echo json_encode(array('status' => 'success', 'amount' => $amount));
        }
    }

    public function save_amount() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('department_id', 'Department', 'xss_clean|trim|required|callback_check_department_id');
        $this->form_validation->set_rules('year', 'Year', 'xss_clean|trim|required|callback_check_budget_year');
        $this->form_validation->set_rules('amount', 'Amount', 'xss_clean|trim|required|is_natural|callback_check_budget_amount');
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
        } else {
            $this->db->update('annual_budget', array('amount' => $this->input->post('amount')), array('department_id' => $this->input->post('department_id'), 'year' => $this->input->post('year')));
            echo json_encode(array('status' => 'success', 'msg' => 'Successfully update department budget.'));
        }
    }

    public function check_department_id($val) {
        if ($val == 0) {
            $this->form_validation->set_message('check_department_id', 'Please select %s field');
            return FALSE;
        }

        $row = $this->Department_model->get_row_by_primary_key($val)->row();
        if (empty($row)) {
            $this->form_validation->set_message('check_department_id', 'The %s field is not valid');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_budget_year($val) {
        if ($val == 0) {
            $this->form_validation->set_message('check_budget_year', 'Please select %s field');
            return FALSE;
        }

        $row = $this->Annual_budget_model->get_by_field(array('department_id' => $this->input->post('department_id'), 'year' => $val))->row();
        if (empty($row)) {
            $this->form_validation->set_message('check_budget_year', 'The %s field is not valid');
            return FALSE;
        } else {
            $_POST['amount_return'] = $row->amount;
            return TRUE;
        }
    }

    public function check_budget_amount($val) {
        if (doubleval($val) < 0) {
            $this->form_validation->set_message('check_budget_amount', 'The %s field is not valid');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

/* End of file annual_budget.php */
/* Location: ./application/controllers/backend/annual_budget.php */