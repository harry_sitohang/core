<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Core</title>
        <?php $this->view->load("font"); ?>
        <?php $this->view->load("assets_css"); ?>
        <?php $this->view->load("global_js_var.php"); ?>
    </head>
    <body>
        <?php $this->view->load("header"); ?>

        <div class="container">
            <?php $this->view->load($_pages); ?>
        </div>

        <div class="footer"></div>

        <?php $this->view->load("assets_js"); ?>
    </body>
</html>