<script type="text/javascript">
    var base_url = <?php echo json_encode(base_url()); ?>;
    var template_url = <?php echo json_encode($template_url); ?>;
    var class_name = <?php echo json_encode($class_name); ?>;
    var class_url = <?php echo json_encode($class_url); ?>;
    var current_url = <?php echo json_encode(current_url() . "/"); ?>;

    var _assets = <?php echo json_encode($_assets); ?>;
    var _assets_css = <?php echo json_encode($_assets_css); ?>;
    var _assets_js = <?php echo json_encode($_assets_js); ?>;
</script>
