<!-- BEGIN HEADER -->
<div class="header navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <button class="navbar-toggle btn navbar-btn" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN LOGO (you can use logo image instead of text)-->
            <a class="navbar-brand logo-v1" href="<?php echo base_url(); ?>">
                Raenz<em>.com</em>
            </a>
            <!-- END LOGO -->
        </div>

        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                        Home
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="active"><a href="index.html">Home Default</a></li>
                        <li><a href="page_home_fixed_header.html">Header Fixed</a></li>
                        <li><a href="page_home2.html">Home with Top Bar</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-delay="0" data-close-others="false" data-target="#" href="#">
                        Mega Menu 
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <!-- BEGIN DROPDOWN MENU -->
                    <ul class="dropdown-menu" aria-labelledby="mega-menu">
                        <li>
                            <div class="nav-content">
                                <!-- BEGIN DROPDOWN MENU - COLUMN -->
                                <div class="nav-content-col">
                                    <h3>Footwear</h3>
                                    <ul>
                                        <li><a href="#">Astro Trainers</a></li>
                                        <li><a href="#">Basketball Shoes</a></li>
                                        <li><a href="#">Boots</a></li>
                                        <li><a href="#">Canvas Shoes</a></li>
                                        <li><a href="#">Football Boots</a></li>
                                        <li><a href="#">Golf Shoes</a></li>
                                        <li><a href="#">Hi Tops</a></li>
                                        <li><a href="#">Indoor and Court Trainers</a></li>
                                    </ul>
                                </div>
                                <!-- END DROPDOWN MENU - COLUMN -->
                                <!-- BEGIN DROPDOWN MENU - COLUMN -->
                                <div class="nav-content-col">
                                    <h3>Clothing</h3>
                                    <ul>
                                        <li><a href="#">Base Layer</a></li>
                                        <li><a href="#">Character</a></li>
                                        <li><a href="#">Chinos</a></li>
                                        <li><a href="#">Combats</a></li>
                                        <li><a href="#">Cricket Clothing</a></li>
                                        <li><a href="#">Fleeces</a></li>
                                        <li><a href="#">Gilets</a></li>
                                        <li><a href="#">Golf Tops</a></li>
                                    </ul>
                                </div>
                                <!-- END DROPDOWN MENU - COLUMN -->
                                <!-- BEGIN DROPDOWN MENU - COLUMN -->
                                <div class="nav-content-col">
                                    <h3>Accessories</h3>
                                    <ul>
                                        <li><a href="#">Belts</a></li>
                                        <li><a href="#">Caps</a></li>
                                        <li><a href="#">Gloves, Hats and Scarves</a></li>
                                    </ul>

                                    <h3>Clearance</h3>
                                    <ul>
                                        <li><a href="#">Jackets</a></li>
                                        <li><a href="#">Bottoms</a></li>
                                    </ul>
                                </div>
                                <!-- END DROPDOWN MENU - COLUMN -->
                            </div>
                        </li>
                    </ul>
                    <!-- END DROPDOWN MENU -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                        Pages
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="page_about.html">About Us</a></li>
                        <li><a href="page_services.html">Services</a></li>
                        <li><a href="page_prices.html">Prices</a></li>
                        <li><a href="page_faq.html">FAQ</a></li>
                        <li><a href="page_gallery.html">Gallery</a></li>
                        <li><a href="page_search_result.html">Search Result</a></li>
                        <li><a href="page_404.html">404</a></li>
                        <li><a href="page_500.html">500</a></li>
                        <li><a href="page_login.html">Login Page</a></li>
                        <li><a href="page_signup.html">Signup Page</a></li>
                        <li><a href="page_careers.html">Careers</a></li>
                        <li><a href="page_contacts.html">Contact</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                        Features
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="feature_typography.html">Typography</a></li>
                        <li><a href="feature_buttons.html">Buttons</a></li>
                        <li><a href="feature_forms.html">Forms</a></li>
                        <li><a href="feature_icons.html">Icons</a></li>
                    </ul>
                </li>                        
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                        Portfolio
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="portfolio_4.html">Portfolio 4</a></li>
                        <li><a href="portfolio_3.html">Portfolio 3</a></li>
                        <li><a href="portfolio_2.html">Portfolio 2</a></li>
                        <li><a href="portfolio_item.html">Portfolio Item</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                        Blog
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="blog.html">Blog Page</a></li>
                        <li><a href="blog_item.html">Blog Item</a></li>
                    </ul>
                </li>					
                <li><a href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin&page=index.html" target="_blank">Admin Theme</a></li>
                <li class="menu-search">
                    <span class="sep"></span>
                    <span class="glyphicon glyphicon-search"></span>

                    <div class="search-box">
                        <form action="#">
                            <div class="input-group input-large">
                                <input class="form-control" type="text" placeholder="Search">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn theme-btn">Go</button>
                                </span>
                            </div>
                        </form>
                    </div> 
                </li>
            </ul>                           
        </div>
        <!-- BEGIN TOP NAVIGATION MENU -->
    </div>
</div>
<!-- END HEADER -->

<!-- BEGIN BREADCRUMBS -->   
<div class="row breadcrumbs margin-bottom-40">
    <div class="container">
        <div class="col-md-4 col-sm-4">
            <h1>About Us</h1>
        </div>
        <div class="col-md-8 col-sm-8">
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="">Pages</a></li>
                <li class="active">About Us</li>
            </ul>
        </div>
    </div>
</div>
<!-- END BREADCRUMBS -->