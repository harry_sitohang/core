<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>PT.SEZ | Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/plugins/font-awesome/4.2.0/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/plugins/bootstrap/3.2.0/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/plugins/uniform/2.1.1/css/uniform.default.css"); ?>" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/plugins/select2/select2.css"); ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/plugins/select2/select2-metronic.css"); ?>"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo base_url("assets/css/style-metronic.css"); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/style.css"); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/style-responsive.css"); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/plugins.css"); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/themes/default.css"); ?>" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo base_url("assets/css/pages/login.css"); ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?php echo base_url("assets/img/sez.ico"); ?>"/>
    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="javascript:,">
                PT.SEZ
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="index.html" method="post">
                <h3 class="form-title">Login to your account</h3>
                <div class="error-msg-container">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        <span>
                            Enter any email and password.
                        </span>
                    </div>
                </div>
                <div class="form-group" for="email">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
                    </div>
                </div>
                <div class="form-group" for="password">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
                    </div>
                </div>
                <div class="form-actions">
                    <button name="submit" type="submit" class="btn green pull-right" data-loading-text="loading...">
                        Login <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                </div>
                <div class="forget-password">
                    <h4>Forgot your password ?</h4>
                    <p>
                        no worries, click
                        <a href="javascript:;" id="forget-password">
                            here
                        </a>
                        to reset your password.
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="index.html" method="post">
                <h3>Forget Password ?</h3>
                <p>
                    Enter your e-mail address below to reset your password.
                </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn">
                        <i class="m-icon-swapleft"></i> Back </button>
                    <button type="submit" class="btn green pull-right" data-loading-text="loading...">
                        Submit <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright">
            2014 &copy; PT.SEZ
        </div>
        <!-- END COPYRIGHT -->
        <script type="text/javascript">
            var base_url = <?php echo json_encode(base_url()); ?>;
            var class_name = <?php echo json_encode($class_name); ?>;
            var class_url = <?php echo json_encode($class_url); ?>;
            var current_url = <?php echo json_encode(current_url() . "/"); ?>;
        </script>
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
                <script src="<?php echo base_url("assets/js/respond.min.js"); ?>"></script>
                <script src="<?php echo base_url("assets/js/excanvas.min.js"); ?>"></script> 
                <![endif]-->
        <script src="<?php echo base_url("assets/plugins/jquery/1.11.1/jquery.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/plugins/bootstrap/3.2.0/js/bootstrap.min.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/plugins/bootstrap-hover-dropdown/2.0.1/bootstrap-hover-dropdown.min.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/plugins/jquery-slimscroll/1.3.2/jquery.slimscroll.min.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/plugins/jquery.blockUI/2.66.0/jquery.blockui.min.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/plugins/jquery.cookie/1.3.1/jquery.cookie.min.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/plugins/uniform/2.1.1/jquery.uniform.min.js"); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url("assets/plugins/jquery-validation/1.11.1/dist/jquery.validate.min.js"); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/plugins/select2/select2.min.js"); ?>"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url("assets/scripts/core/app.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/js/site.js"); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/js/pages/login.js"); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function() {
                App.init();
                Login.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>