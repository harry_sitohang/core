<!-- Jquery Ui CSS -->
<link rel="stylesheet" href="<?php echo "{$_general_assets}plugins/jquery-ui/1.11.0/jquery-ui{$_sufiks_min_assets}.css"; ?>">

<!-- Juery Ui Addon CSS-->
<link rel="stylesheet" href="<?php echo "{$_general_assets}plugins/jquery-ui/addon/jquery-ui-timepicker-addon{$_sufiks_min_assets}.css"; ?>">

<!-- Bootstrap -->
<link href="<?php echo "{$_general_assets}plugins/bootstrap/3.2.0/css/bootstrap{$_sufiks_min_assets}.css"; ?>" rel="stylesheet">
<!-- End Bootstrap -->

<!-- Form Sexy forms with jQuery : http://uniformjs.com/-->          
<link href="<?php echo "{$_general_assets}plugins/uniform/2.1.1/css/uniform.default{$_sufiks_min_assets}.css"; ?>" rel="stylesheet" type="text/css"/>
<!-- End Sexy forms with jQuery -->  

<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
<!--A small growl-like notification plugin for jQuery-->
<link href="<?php echo "{$_general_assets}"; ?>plugins/gritter/1.7.4/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<!--Date Range Picker for Bootstrap-->
<link href="<?php echo "{$_general_assets}"; ?>plugins/bootstrap-daterangepicker/1.3/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<!--Calendar feature for jquery-->
<link href="<?php echo "{$_general_assets}"; ?>plugins/fullcalendar/1.6.1/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
<!--JQVMap is a jQuery plugin that renders Vector Maps-->
<!--<link href="<?php //echo "{$_general_assets}"; ?>plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>-->
<!--Lightweight jQuery plugin to render and animate nice pie charts with the HTML5 canvas element-->
<!--<link href="<?php //echo "{$_general_assets}"; ?>plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>-->
<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN THEME STYLES --> 
<link href="<?php echo "{$_general_assets}"; ?>css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$_general_assets}"; ?>css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$_general_assets}"; ?>css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$_general_assets}"; ?>css/plugins.css" rel="stylesheet" type="text/css"/>
<!--<link href="<?php //echo "{$_assets}"; ?>css/pages/tasks.css" rel="stylesheet" type="text/css"/>-->
<link href="<?php echo "{$_general_assets}"; ?>css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo "{$_general_assets}"; ?>css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

<!-- Site CSS -->
<link rel="stylesheet" href="<?php echo "{$_assets}css/site{$_sufiks_min_assets}.css"; ?>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Load Css for current page -->
<?php if (isset($pages_css)): ?>
    <?php if (is_array($pages_css)): ?>
        <?php foreach ($pages_css as $css): ?>
            <link rel="stylesheet" href="<?php echo $css; ?>">
        <?php endforeach; ?>
    <?php else: ?>
        <link rel="stylesheet" href="<?php echo $pages_css; ?>">
    <?php endif; ?>
<?php endif; ?>