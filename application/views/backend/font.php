<!-- Fonts START -->
<!--Load google fonts-->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<!--Font Awesome gives you scalable vector icons that can instantly be customized — size, color, drop shadow, and anything that can be done with the power of CSS.-->
<link rel="stylesheet" href="<?php echo "{$_general_assets}plugins/font-awesome/4.2.0/css/font-awesome{$_sufiks_min_assets}.css" ?>">
<!-- Fonts END -->