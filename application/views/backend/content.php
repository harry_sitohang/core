<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN PAGE -->
    <div class="page-content clearfix" id="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($breadcrumb_view)): ?>
                    <?php echo $breadcrumb_view; ?>
                <?php else: ?>
                    <?php $this->view->load("breadcrumb"); ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <?php $this->view->load($_pages); ?>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>      
    <!-- END PAGE -->
</div>
<!-- END CONTENT -->
