<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>

            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!--            <li class="sidebar-search-wrapper">
                            <form class="sidebar-search" action="extra_search.html" method="POST">
                                <div class="form-container">
                                    <div class="input-box">
                                        <a href="javascript:;" class="remove">
                                        </a>
                                        <input type="text" placeholder="Search..."/>
                                        <input type="button" class="submit" value=" "/>
                                    </div>
                                </div>
                            </form>
                        </li>-->
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
            
            <?php echo $sidebar_menu; ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->