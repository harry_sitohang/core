<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->   
<!--[if lt IE 9]>
<script src="<?php echo "{$_general_assets}"; ?>js/respond.min.js"></script>
<script src="<?php echo "{$_general_assets}"; ?>js/excanvas.min.js"></script> 
<![endif]-->   
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo "{$_general_assets}plugins/jquery/1.11.1/jquery{$_sufiks_min_assets}.js"; ?>"></script>
<script src="<?php echo "{$_general_assets}plugins/jquery-ui/1.11.0/jquery-ui{$_sufiks_min_assets}.js"; ?>"></script>  
<!-- Jquery Ui Addon CSS-->
<script src="<?php echo "{$_general_assets}plugins/jquery-ui/addon/jquery-ui-timepicker-addon{$_sufiks_min_assets}.js"; ?>"></script>  
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo "{$_general_assets}plugins/bootstrap/3.2.0/js/bootstrap{$_sufiks_min_assets}.js"; ?>"></script>
<!--Bootstrap: Dropdown on Hover Plugin Demo-->
<script src="<?php echo "{$_general_assets}"; ?>plugins/bootstrap-hover-dropdown/2.0.1/bootstrap-hover-dropdown.min.js"></script>
<!--SlimScroll is a small jQuery plugin that transforms any div into a scrollable area with a nice scrollbar-->
<script src="<?php echo "{$_general_assets}"; ?>plugins/jquery-slimscroll/1.3.2/jquery.slimscroll.min.js"></script>
<!--JQuery BlockUI Plugin lets you simulate synchronous behavior when using AJAX, without locking the browser[1]. When activated, it will prevent user activity with the page (or part of the page) until it is deactivated-->
<script src="<?php echo "{$_general_assets}"; ?>plugins/jquery.blockUI/2.66.0/jquery.blockui.min.js"></script>
<!--A simple, lightweight jQuery plugin for reading, writing and deleting cookies.-->
<script src="<?php echo "{$_general_assets}"; ?>plugins/jquery.cookie/1.3.1/jquery.cookie.min.js" type="text/javascript"></script>
<!-- Form Sexy forms with jQuery : http://uniformjs.com/-->    
<script src="<?php echo "{$_general_assets}"; ?>plugins/uniform/2.1.1/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--Full featured date library for parsing, validating, manipulating, and formatting dates. http://momentjs.com/-->
<script src="<?php echo "{$_general_assets}"; ?>plugins/momentjs/2.1.0/moment.min.js" type="text/javascript"></script>
<!--Bootstrap-datepicker provides a flexible datepicker widget in the Twitter bootstrap style. http://bootstrap-datepicker.readthedocs.org/en/release/-->
<script src="<?php echo "{$_general_assets}"; ?>plugins/bootstrap-daterangepicker/1.3/daterangepicker.js" type="text/javascript"></script>
<!--A small growl-like notification plugin for jQuery-->
<script src="<?php echo "{$_general_assets}"; ?>plugins/gritter/1.7.4/js/jquery.gritter.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo "{$_general_assets}"; ?>scripts/core/app.js" type="text/javascript"></script>
<script src="<?php echo "{$_general_assets}"; ?>scripts/custom/index.js" type="text/javascript"></script>
<!--<script src="<?php //echo "{$_general_assets_js}";     ?>scripts/custom/tasks.js" type="text/javascript"></script>        -->
<!-- END PAGE LEVEL SCRIPTS --> 

<script>
    jQuery(document).ready(function() {
        App.init(); // initlayout and core plugins
        Index.init();
    });
</script>

<!-- BEGIN My Own JS -->
<!--Site Js-->
<script src="<?php echo "{$_general_assets_js}"; ?>site.js"></script>
<!-- Load Js for current page -->
<?php if (isset($pages_js)): ?>
    <?php if (is_array($pages_js)): ?>
        <?php foreach ($pages_js as $js): ?>
            <script src="<?php echo $js; ?>"></script>
        <?php endforeach; ?>
    <?php else: ?>
        <script src="<?php echo $js; ?>"></script>
    <?php endif; ?>
<?php endif; ?>
<!-- END My Own JS -->
