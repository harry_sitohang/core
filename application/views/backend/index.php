<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>PT.SEZ | Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->view->load("font"); ?>
        <?php $this->view->load("assets_css"); ?>
        <?php $this->view->load("global_js_var.php"); ?>
        <link rel="shortcut icon" href="<?php echo base_url("assets/img/sez.ico"); ?>"/>
    </head>
    <body class="page-header-fixed">
        <?php $this->view->load("header"); ?>

        <div class="clearfix"></div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php $this->view->load("sidebar"); ?>
            <?php $this->view->load("content"); ?>
        </div>
        <!-- END CONTAINER -->

        <?php $this->view->load("footer"); ?>
        <?php $this->view->load("assets_js"); ?>


        <!--tinymce editor-->
        <?php if (isset($enable_crud) && $enable_crud == TRUE): ?>
            <?php $this->view->load("pages/tinymce/media_dialog"); ?>
        <?php endif; ?>
    </body>
</html>