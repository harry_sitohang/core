<h4 class="page-header text-center bold uppercase">Requisition Form</h4>
<div class="clearfix" style="margin-bottom: 15px;">
    <h5 style="font-weight: normal !important;" class="pull-left"><strong>DATE : </strong><?php echo date('d/m/Y'); ?></h5>
    <button class="pull-right" data-name="add-more-item">Add More Item</button>
</div>

<form id="requisition-form" class="form-horizontal" role="form" name="requisition-form" action="#">
    <table class="table table-condensed table-bordered table-striped">
        <thead>
            <tr>
                <th class="text-center uppercase" width="10">NO.</th>
                <th class="text-center uppercase">TYPE</th>
                <th class="text-center uppercase">Description</th>
                <th class="text-center uppercase" width="100">UNIT</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php echo $first_row_item; ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5" class="text-right"><button data-action="add" class="btn green" data-loading-text="Loading..." type="submit" name="submit">Submit</button></th> 
            </tr>
        </tfoot>
    </table>
</form>

<div class="clearfix" style="margin-top: 30px;">
    <button class="pull-right" data-name="add-more-item">Add More Item</button>
</div>

<?php //$this->view->load('pages/request_form/modal_proceed_with_items'); ?>
<?php $this->view->load('pages/request_form/modal_suggest_after_add_request'); ?>