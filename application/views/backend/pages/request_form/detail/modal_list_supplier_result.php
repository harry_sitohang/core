<?php if (!empty($result)): ?>
    <div class="row-fluid clearfix table-scrollable datatable-container no-border">
        <div class="alert alert-info"><?php echo count($result); ?> supplier founds.</div>
        <table class="table table-condensed table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>No</th>
                    <th>Name</th>
                    <th>Business</th>
                    <th>Contact Number</th>
                    <th>Email</th>
                    <th>Website</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($result as $key=>$row): ?>
                    <tr>
                        <td><a href="javascript:;" class="btn btn-sm blue" data-name="set-supplier" data-supplier-id="<?php echo $row->supplier_id; ?>" data-supplier-name="<?php echo $row->supplier_name; ?>">Choose this <i class="fa fa-chevron-right"></i></a></td>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo highlight_phrase($row->supplier_name, $this->input->post('search_supplier_keyword'), '<span style="color:#990000">', '</span>'); ?></td>
                        <td><?php echo highlight_phrase($row->supplier_business, $this->input->post('search_supplier_keyword'), '<span style="color:#990000">', '</span>'); ?></td>
                        <td>Telp : <?php echo $row->supplier_telp; ?> Fax : <?php echo $row->supplier_fax; ?></td>
                        <td><?php echo $row->supplier_email; ?></td>
                        <td><?php echo highlight_phrase($row->supplier_address, $this->input->post('search_supplier_keyword'), '<span style="color:#990000">', '</span>'); ?></td>
                        <td><?php echo $row->supplier_website_url; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <div class="alert alert-danger text-danger">No result found, try another search keyword...</div>
<?php endif; ?>
