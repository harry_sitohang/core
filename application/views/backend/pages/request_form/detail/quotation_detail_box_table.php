<a href="javascript:;" class="btn btn-sm blue pull-right" style="margin-bottom: 20px;" data-name="add-group-quotation" data-request-number="<?php echo $form_request->request_number; ?>"><i class="fa fa-plus-square"></i> Add more quotation</a>
<table class="table table-bordered table-condensed table-striped" id="table-quotation">
    <thead>
        <tr>
            <th>NO</th>
            <th>Supplier</th>
            <th>Items</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($quots)): ?>
            <?php $data_quots = array(); ?>
            <?php foreach ($quots as $group_number => $quotations): ?>
                <tr data-name="quotion-group" data-group-number="<?php echo $group_number; ?>" data-status="added">
                    <?php $this->view->load("pages/request_form/detail/quotation_detail_box_table_tr_added", array('group_number' => $group_number, 'quotations' => $quotations)); ?>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        <tr data-name="quotion-group" data-group-number="">
            <td><?php echo isset($group_number) ? $group_number+1 : 1; ?>.</td>
            <td data-for="list-supplier">
                <div class="box-list-supplier">
                    <div class="form-group" data-for="supplier">
                        <a href="javascript:;" class="btn btn-sm" data-name="select-supplier">Select Supplier</a>
                        <input type="hidden" name="supplier_id[]">
<!--                            <input type="file" class="" name="quotation_file[]" style="display: inline-block;">-->
                        <a href="javascript:;" class="btn btn-sm btn-default" data-name="remove-supplier-from-group-quotation"><i class="fa fa-remove"></i></a>
                    </div>
                </div>
                <a href="javascript:;" class="btn btn-sm btn-default" data-name="add-supplier-to-group-quotation"><i class="fa fa-plus-square-o"></i> add supplier</a>
            </td>
            <td data-for="items">
                <ul class="list-unstyled">
                    <?php foreach ($unselected_items as $key => $item): ?>
                        <li><input type="checkbox" value="<?php echo $item->item_id; ?>" name="set-items" data-item-type="<?php echo $item->item_type; ?>" data-item-description="<?php echo $item->item_description; ?>"> <?php echo $item->item_description; ?> : <?php echo $item->item_type; ?></li>
                    <?php endforeach; ?>
                </ul>
            </td>
            <td>
                <a href="javascript:;" data-loading-text="Loading..." name="add-quotation" class="btn btn-success btn-sm"><i class="fa fa-save"></i>&nbsp;&nbsp;save&nbsp;</a> 
                <a href="javascript:;" data-loading-text="Loading..." name="remove-quotation" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp;&nbsp;remove&nbsp;</a>
            </td>
        </tr>
    </tbody>
</table>
<a href="javascript:;" class="btn btn-sm grey blue pull-right" data-name="add-group-quotation" data-request-number="<?php echo $form_request->request_number; ?>"><i class="fa fa-plus-square"></i> Add more quotation</a>