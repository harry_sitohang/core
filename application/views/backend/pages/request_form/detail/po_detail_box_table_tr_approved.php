<td><?php echo $group_number; ?>.</td>
<td data-for="quots">
    <?php foreach ($quotations as $quotation_number => $quotation): ?>
        <div class="group-quot clearfix">
            <div class="col-md-8">
                <?php echo $quotation['supplier_name']; ?> <a href="<?php echo "{$class_url}view_quotation/{$quotation['quotation_file']}"; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> quotation</a>
                <?php $current_items = $quotation['items']; ?>
                <ul class="">
                    <?php foreach ($quotation['items'] as $item): ?>
                        <li>
                            <?php echo $item['item_qty'] ? $item['item_qty'] . ' unit ' : ''; ?><?php echo $item['item_description']; ?> : <?php echo format_number($item['price'] * $item['item_qty']); ?> IDR 
                            <?php if ($item['capex_number'] != '-'): ?>
                                <a href="<?php echo "{$class_url}pdf/capex/{$item['item_id']}"; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> capex</a>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-4">
                <?php if ($quotation['request_order_number']): ?>
                    <a data-name="link-po" class="pull-right" style="margin:5px;" href="http://core.net/backend/request_form_all/pdf/<?php echo strtolower($quotation['request_type']); ?>/<?php echo $quotation['request_order_number']; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> <?php echo $quotation['request_type']; ?></a>
                    <a href="javascript:;" class="btn btn-primary btn-sm"><span aria-hidden="true" class="glyphicon glyphicon-ok-sign"></span> Selected</a>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</td>
<td>
    <ul class="">
        <?php foreach ($current_items as $item): ?>
            <li>
                <?php echo $item['item_qty'] ? $item['item_qty'] . ' unit ' : ''; ?><?php echo $item['item_description']; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</td>