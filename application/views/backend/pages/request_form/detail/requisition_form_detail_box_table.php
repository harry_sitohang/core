<div class="portlet-body clearfix" id="box-table-requisition">
    <div class="table-responsive container-table">
        <?php $this->view->load("pages/request_form/detail/requisition_form_detail_table"); ?>
    </div>
    <a href="javascript:;" class="btn btn-sm btn-default pull-right" data-name="add-more-item"><i class="fa fa-plus-square"></i> Add More item</a>
</div>