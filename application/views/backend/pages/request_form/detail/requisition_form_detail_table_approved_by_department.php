<table class="table table-hover table-bordered table-striped" id="list-items" data-request-number="<?php echo $form_request->request_number; ?>">
    <thead>
        <tr>
            <th>
                NO
            </th>
            <th>
                TYPE
            </th>
            <th>
                DESCRIPTION
            </th>
            <th>
                UNIT
            </th>
            <th>
                PRICE (IDR)
            </th>
            <th>
                SUB TOTAL
            </th>
        </tr>
    </thead>
    <tbody>
        <?php $grand_total = 0; ?>
        <?php foreach ($items as $key => $item): ?>
            <tr>
                <td data-for="item-counter"><?php echo ($key + 1) . '.'; ?></td>
                <td>
                    <?php echo $item->item_type; ?>
                </td>
                <td>
                    <?php echo $item->item_description; ?>
                </td>
                <td>
                    <?php echo $item->item_qty; ?>
                </td>
                <td>
                    <?php if ($item->item_price): ?>
                        Rp <?php echo format_number($item->item_price); ?>
                    <?php else: ?>
                        <a href="javascript:;">Quotation has not been set</a>
                    <?php endif; ?>
                </td>
                <td>
                    <?php $sub_total = $item->item_price * $item->item_qty; ?>
                    <?php $grand_total += $sub_total; ?>
                    <?php if ($item->item_price): ?>
                        Rp <?php echo format_number($sub_total); ?>
                    <?php else: ?>
                        <a href="javascript:;">Price has not been set</a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="text-right"><strong>Grand Total</strong></td>
            <td><strong>Rp <?php echo format_number($grand_total); ?></strong></td>
        </tr>
    </tfoot>
</table>