<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet grey box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>PO List 
                    <span class="badge badge-success" data-name="count-po">
                        <?php echo count($purchase_orders); ?>
                    </span>
                </div>
                <div class="actions actions-requisition">
                    <?php if ($session_user->department_id == '6'): ?>
                        <?php if (!preg_match('/(6,|6$)/i', $request_state)): ?><!--If not self-approved-->
                            <a href="javascript:;" class="btn btn-success btn-sm" data-name="approve-po" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-send"></i> Approved 
                            </a>
                            <a href="javascript:;" class="btn btn-danger btn-sm" data-name="unlock-po" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-unlock"></i> Unlock 
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="portlet-body clearfix" id="box-table-po">
                <?php if (preg_match("/(2,|2$)/i", $request_state) && !preg_match("/(6,|6$)/i", $request_state) && $session_user->department_id == '6'): ?><!--Approved by ADMIN, LOGIN AS ADMIN, ADMIN NOT APPROVED BY DIRECTOR YET-->
                    <?php $this->view->load('pages/request_form/detail/po_detail_box_table'); ?>
                <?php else: ?>
                    <?php $this->view->load('pages/request_form/detail/po_detail_box_table_approved'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>