<table class="table table-hover table-bordered table-striped" id="list-items" data-request-number="<?php echo $form_request->request_number; ?>">
    <thead>
        <tr>
            <th>
                NO
            </th>
            <th>
                TYPE
            </th>
            <th>
                DESCRIPTION
            </th>
            <th>
                UNIT
            </th>
            <th>
                PRICE (IDR)
            </th>
            <th>
                SUB TOTAL
            </th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $key => $item): ?>
            <tr>
                <td data-for="item-counter"><?php echo ($key + 1) . '.'; ?></td>
                <td>
                    <div class="form-group" for="item_type">
                        <select name="item_type" class="form-control">
                            <option value="0">SELECT</option>
                            <option value="MATERIAL" <?php echo $item->item_type == 'MATERIAL' ? 'selected="TRUE"' : ''; ?>>MATERIAL</option>
                            <option value="SERVICE" <?php echo $item->item_type == 'SERVICE' ? 'selected="TRUE"' : ''; ?>>SERVICE</option>
                        </select>
                    </div>
                </td>
                <td>
                    <div class="form-group" for="item_description">
                        <textarea name="item_description" class="form-control"><?php echo $item->item_description; ?></textarea>
                    </div>
                </td>
                <td>
                    <?php //if ($item->item_type == 'MATERIAL'): ?>
                    <div class="form-group" for="item_qty">
                        <input type="text" class="form-control input-small" name="item_qty" value="<?php echo $item->item_qty; ?>">
                    </div>
                    <?php //else: ?>
                    <?php //endif; ?>
                </td>
                <td><a href="javascript:;">Quotation has not been set</a></td>
                <td><a href="javascript:;">Price has not been set</a></td>
                <td>
                    <a href="javascript:;" class="btn green btn-sm" data-item-id="<?php echo $item->item_id; ?>" data-name="update-item" data-loading-text="loading..."><i class="fa fa-check"></i> Save</a> 
                    <a href="javascript:;" class="btn red btn-sm" data-item-id="<?php echo $item->item_id; ?>" data-name="remove-item" data-loading-text="loading..."><i class="fa fa-remove"></i> Remove</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>