<table class="table table-bordered table-condensed table-striped" id="table-quotation">
    <thead>
        <tr>
            <th>NO</th>
            <th>Supplier</th>
            <th>Items</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($quots)): ?>
            <?php $data_quots = array(); ?>
            <?php foreach ($quots as $group_number => $quotations): ?>
                <tr data-name="quotion-group" data-group-number="<?php echo $group_number; ?>" data-status="added">
                    <?php $this->view->load("pages/request_form/detail/quotation_detail_box_table_tr_added", array('group_number' => $group_number, 'quotations' => $quotations, 'approved_by_admin' => TRUE)); ?>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>