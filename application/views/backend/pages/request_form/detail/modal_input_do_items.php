<table class="table table-condensed table-striped table-bordered"> 
    <tr>
        <th>Item</th>
        <th>Type</th>
        <th>COA</th>
        <th>Asset</th>
    </tr>
    <?php foreach ($items as $item): ?>
        <tr>
            <td><?php echo $item->item_description; ?></td>
            <td><?php echo $item->item_type; ?></td>
            <td>
                <?php echo ($item->capex_number == "-") ? 'EXPENSE' : 'CAPEX'; ?>
                <input type="text" placeholder="COA Number" name="coa_number[<?php echo $item->item_id; ?>]" value=" <?php echo ($item->capex_number == "-") ? '5-' : '1-'; ?>" class="form-control">
            </td>
            <td>
                <?php if ($item->item_type == "MATERIAL"): ?>
                    <input type="text" placeholder="Asset Number" name="asset_number[<?php echo $item->item_id; ?>]" value="" class="form-control">
                <?php else: ?>
                    -
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>