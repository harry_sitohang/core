<div class="modal fade" id="modal-input-do" data-row="1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Input Delivery Order</h4>
            </div>
            <div class="modal-body">
                <form class="clearfix" name="form-input-do" id="form-input-do">
                    <input type="hidden" name="request_order_number" value="">
                    <div class="form-group" for="do_number">
                        <div class="controls">
                            <input type="text" name="do_number" class="form-control" placeholder="DO Number">
                        </div>
                    </div>
                    <div class="form-group" for="do_file">
                        <div class="controls">
                            <input type="file" name="do_file" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" for="review_comment">
                        <div class="controls">
                            <textarea class="form-control" name="review_comment" style="height: 100px;" placeholder="Comment"></textarea>
                        </div>
                    </div>
                    <div class="form-group" for="delivery_score">
                        <div class="controls">
                            <label for="delivery_score">Delivery Score % : </label>
                            <input type="text" name="delivery_score" id="delivery_score" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            <div id="delivery_score_range"></div>
                        </div>
                    </div>
                    <div class="form-group" for="delivery_score">
                        <div class="controls">
                            <label for="delivery_score">Items : </label>
                            <div class="items-container"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-loading-text="loading" data-name="save-input-do">Save</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->