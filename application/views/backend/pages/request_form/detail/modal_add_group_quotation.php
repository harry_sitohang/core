<div class="modal fade" id="modal-add-grooup-quotation">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Set Items Price @Quotation</h4>
            </div>
            <div class="modal-body">
                <form id="form-add-group-quotation">
                    <input type="hidden" name="request_number" value="<?php echo $form_request->request_number; ?>">
                    <table class="table table-bordered table-condensed table-striped" id="table-add-group-quotation">
                        <thead>
                            <tr>
                                <th>Supplier</th>
                                <th>Quotation File</th>
                                <th>Items Price @qty</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </form>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" data-name="save-group-quotation">Save</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->