<div class="modal fade" id="modal-capex" data-row="1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">CAPEX</h4>
            </div>
            <div class="modal-body">
                <table class="table table-condensed table-striped table-bordered"> 
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Item</th>
                            <th>Capex Status</th>
                    </thead>
                    <tbody>
                        <?php $counter = 0; ?>
                        <?php foreach ($items as $key => $item): ?>
                            <?php if ($item->item_type == 'MATERIAL'): ?>
                                <tr>
                                    <td><?php echo ++$counter; ?>.</td>
                                    <td><?php echo $item->item_description; ?></td>
                                    <td>
                                        <?php if ($lock_position['department'] == 'DIRECTOR'): ?>
                                            <?php if ($item->capex_number != '-'): ?>
                                                <a data-name="view-capex-attachment" style="margin-top: 5px; display: inline-block;" href="<?php echo "{$class_url}pdf/capex/{$item->item_id}"; ?>" target="_blank" title="view capex attachment"><span class="glyphicon glyphicon-paperclip"></span> view capex attachment</a>
                                            <?php else: ?>
                                                -
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <select name="set-capex" data-request-number="<?php echo $form_request->request_number; ?>" data-item="<?php echo $item->item_id; ?>" class="form-control">
                                                <option value="YES" <?php echo $item->capex_number != '-' ? 'selected="selected"' : ''; ?>>YES</option>
                                                <option value="NO" <?php echo $item->capex_number == '-' ? 'selected="selected"' : ''; ?>>NO</option>
                                            </select>
                                            <?php if ($item->capex_number != '-'): ?>
                                                <a data-name="view-capex-attachment" style="margin-top: 5px; display: inline-block;" href="<?php echo "{$class_url}pdf/capex/{$item->item_id}"; ?>" target="_blank" title="view capex attachment"><span class="glyphicon glyphicon-paperclip"></span> view capex attachment</a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->