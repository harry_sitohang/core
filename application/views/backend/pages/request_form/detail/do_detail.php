<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet grey box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Delivery Order List 
                    <span class="badge badge-success" data-name="count-do">
                        0<?php //echo count($delivery_orders); ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body clearfix" id="box-table-do">
                <?php $this->view->load('pages/request_form/detail/do_detail_box_table'); ?>
            </div>
        </div>
    </div>
</div>