<table class="table table-bordered table-condensed table-striped" id="table-do">
    <thead>
        <tr>
            <th style="width: 50px;">NO</th>
            <th>Ref No.</th>
            <th>Items</th>
            <th>Supplier</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($delivery_orders as $key => $delivery_order): ?>
            <tr>
                <td><?php echo $key + 1; ?></td>
                <td><a href="<?php echo "{$class_url}pdf/po/{$delivery_order['request_order_number']}"; ?>" target="_blank" title="view po"><?php echo generate_delivery_order_number($delivery_order['request_order_number'], $delivery_order['created_date']); ?></a></td>
                <td>
                    <ul>
                        <?php foreach ($delivery_order['items'] as $item): ?>
                            <li>
                                <?php echo $item['item_description']; ?><br/><?php echo $item['item_qty']; ?> unit &times; <?php echo format_number($item['item_price']); ?> = <?php echo format_number($item['item_qty'] * $item['item_price']); ?><br/>TYPE : <?php echo $item['item_type']; ?>
                                <?php if ($item['capex_number'] != '-'): ?>
                                    <a href="<?php echo "{$class_url}pdf/capex/{$item['item_id']}"; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> capex</a>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </td>
                <td><?php echo $delivery_order['supplier_name']; ?><br/><?php echo $delivery_order['supplier_address']; ?><br/><a href="<?php echo "{$class_url}/view_quotation/{$delivery_order['quotation_file']}"; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> quotation</a></td>
                <td>

                    <?php if (preg_match("/(6,|6$)/i", $request_state) && $session_user->department_id == '2'): ?><!--Approved BY DIRECTOR-->
                        <?php if ($delivery_order['do_number']): ?>
                            <a href="javascript:;" class="btn btn-primary btn-sm pull-right" data-name="view-do" data-loading-text="Loading..." data-do-id="<?php echo $delivery_order['do_id']; ?>" data-request-order-number="<?php echo $delivery_order['request_order_number']; ?>"><span aria-hidden="true" class="glyphicon glyphicon-ok-sign"></span> View DO : <?php echo $delivery_order['do_number']; ?></a>
                        <?php else: ?>
                            <a href="javascript:;" class="btn btn-primary btn-sm pull-right" data-name="input-do" data-loading-text="Loading..." data-request-order-number="<?php echo $delivery_order['request_order_number']; ?>">Input DO</a>
                        <?php endif; ?>
                    <?php elseif (!preg_match("/(6,|6$)/i", $request_state)): ?>
                        <span class="text-danger">Waiting Director Approval</span>
                    <?php else: ?>
                        <?php if ($delivery_order['do_number']): ?>
                            <a href="javascript:;" class="btn btn-primary btn-sm pull-right" data-name="view-do"data-loading-text="Loading..." data-do-number="<?php echo $delivery_order['do_number']; ?>"><span aria-hidden="true" class="glyphicon glyphicon-ok-sign"></span> View DO : <?php echo $delivery_order['do_number']; ?></a>
                        <?php else: ?>
                            <span class="text-danger">Waiting Receive DO</span>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php $this->view->load("pages/request_form/detail/modal_input_do"); ?>
<?php $this->view->load("pages/request_form/detail/modal_view_do"); ?>
<?php
//echo '<pre>'; print_r($delivery_orders); ?>