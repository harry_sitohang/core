<div class="modal fade" id="modal-list-supplier" data-row="1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Select Supplier</h4>
            </div>
            <div class="modal-body">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-lg">
                        <li class="active">
                            <a href="#tab_search_supplier" data-toggle="tab">
                                Search
                            </a>
                        </li>
                        <li>
                            <a href="#tab_add_new_supplier" data-toggle="tab">
                                New
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_search_supplier">
                            <form class="clearfix" name="form-search-supplier" id="form-search-supplier">
                                <div class="form-group" for="search_supplier_by">
                                    <div class="controls">
                                        <select name="search_supplier_by" class="form-control">
                                            <option value="0">Search By...</option>
                                            <option value="supplier_name">Name</option>
                                            <option value="supplier_business">Business</option>
                                            <option value="supplier_address">Address</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" for="search_supplier_keyword">
                                    <div class="controls">
                                        <input type="text" name="search_supplier_keyword" class="form-control" placeholder="Type Your Search Here">
                                    </div>
                                </div>
                            </form>
                            <div id="search-supplier-result"></div>
                        </div>
                        <div class="tab-pane" id="tab_add_new_supplier">
                            <form action="#" name="add-supplier-form" id="add-supplier-form" class="form-horizontal" role="form">
                                <div class="form-group" for="supplier_name">
                                    <label for="supplier_name" class="col-sm-2 control-label">Supplier Name</label>
                                    <div class="col-sm-10 controls">
                                        <input type="text" name="supplier_name" value="" id="supplier_name" class="form-control" max_length="250" placeholder="Supplier Name">                </div>
                                </div>
                                <div class="form-group" for="supplier_business">
                                    <label for="supplier_business" class="col-sm-2 control-label">Supplier Business</label>
                                    <div class="col-sm-10 controls">
                                        <input id="supplier_business" name="supplier_business" type="text" class="form-control tags" value="" placeholder="Supplier Business">                </div>
                                </div>
                                <div class="form-group" for="supplier_telp">
                                    <label for="supplier_telp" class="col-sm-2 control-label">Supplier Telp</label>
                                    <div class="col-sm-10 controls">
                                        <input type="text" name="supplier_telp" value="" id="supplier_telp" class="form-control" max_length="50" placeholder="Supplier Telp">                </div>
                                </div>
                                <div class="form-group" for="supplier_fax">
                                    <label for="supplier_fax" class="col-sm-2 control-label">Supplier Fax</label>
                                    <div class="col-sm-10 controls">
                                        <input type="text" name="supplier_fax" value="" id="supplier_fax" class="form-control" max_length="50" placeholder="Supplier Fax">                </div>
                                </div>
                                <div class="form-group" for="supplier_email">
                                    <label for="supplier_email" class="col-sm-2 control-label">Supplier Email</label>
                                    <div class="col-sm-10 controls">
                                        <input type="text" name="supplier_email" value="" id="supplier_email" class="form-control" max_length="250" placeholder="Supplier Email">                </div>
                                </div>
                                <div class="form-group" for="supplier_address">
                                    <label for="supplier_address" class="col-sm-2 control-label">Supplier Address</label>
                                    <div class="col-sm-10 controls">
                                        <textarea name="supplier_address" cols="40" rows="10" id="supplier_address" value="" class="form-control" placeholder="Supplier Address"></textarea>                </div>
                                </div>
                                <div class="form-group" for="supplier_website_url">
                                    <label for="supplier_website_url" class="col-sm-2 control-label">Supplier Website Url</label>
                                    <div class="col-sm-10 controls">
                                        <input type="text" name="supplier_website_url" value="" id="supplier_website_url" class="form-control" max_length="250" placeholder="Supplier Website Url">                </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <input type="hidden" name="submit"><!--fix bug in firefox-->
                                        <button name="submit" type="submit" data-loading-text="Loading..." class="btn green" data-action="add">Submit</button>
                                        <!--            <button type="button" class="btn default">Cancel</button>-->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->