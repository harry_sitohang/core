<table class="table table-bordered table-condensed table-striped" id="table-po">
    <thead>
        <tr>
            <th style="width: 50px;">NO</th>
            <th>Select Quotation</th>
            <th>Items</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($quots)): ?>
            <?php $data_quots = array(); ?>
            <?php foreach ($quots as $group_number => $quotations): ?>
                <tr data-name="quotion-group" data-group-number="<?php echo $group_number; ?>" data-status="added">
                    <?php $this->view->load("pages/request_form/detail/po_detail_box_table_tr_approved", array('group_number' => $group_number, 'quotations' => $quotations)); ?>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>
<style>
    #tab_po .group-quot:nth-child(2n){
        border-top: dashed #999 1px;
        padding-top: 5px;
    }
    #tab_po .group-quot:hover{
        background-color: #ddd;
    }
</style>
<?php //echo '<pre>'; print_r($quots); ?>
<?php //echo '<pre>'; print_r($items); ?>