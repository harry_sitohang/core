<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="portlet yellow box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Request Details
                </div>
            </div>
            <div class="portlet-body">
                <div class="row static-info">
                    <div class="col-md-5 name">
                        Request #:
                    </div>
                    <div class="col-md-7 value">
                        <?php echo $form_request->request_number; ?>
<!--                                    <span class="label label-info label-sm">
                            Email confirmation was sent
                        </span>-->
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name">
                        Request Date & Time:
                    </div>
                    <div class="col-md-7 value">
                        <?php echo format_date($form_request->request_date); ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name">
                        Request Status:
                    </div>
                    <div class="col-md-7 value" data-name="box-current-lock-position">
                        <?php echo $lock_by; ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name">
                        Grand Total:
                    </div>
                    <div class="col-md-7 value">
                        <a href="javascript:;">Price has not been set</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="portlet blue box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Requester Information
                </div>
            </div>
            <div class="portlet-body">
                <div class="row static-info">
                    <div class="col-md-5 name">
                        Customer Name:
                    </div>
                    <div class="col-md-7 value">
                        <?php echo $requester->user_fullname; ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name">
                        Email:
                    </div>
                    <div class="col-md-7 value">
                        <a href="mailto:<?php echo $requester->user_email; ?>"><?php echo $requester->user_email; ?></a>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name">
                        Department:
                    </div>
                    <div class="col-md-7 value">
                        <?php echo $requester->department_name; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet grey box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Material/Service List 
                    <span class="badge badge-success" data-name="count-items">
                        <?php echo count($items); ?>
                    </span>
                </div>
                <div class="actions actions-requisition">
                    <?php if (preg_match("/^request_form$/i", $class_name)): ?>
                        <?php if ($form_request->proceed == 'NO'): ?>
                            <a href="javascript:;" class="btn btn-success btn-sm" data-name="proceed-request" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                Proceed <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        <?php else: ?><!--Approved Cycle-->
                            <a href="javascript:;" class="btn btn-sm green default"><i class="fa fa-check"></i> Already proceed by <?php echo $proceed_user_fullname; ?></a> 
                            <?php if ($request_state): ?>
                                <?php $request_state_arr = explode(',', $request_state) ?>
                                <?php foreach ($request_state_arr as $state_department_id): ?>
                                    <a href="javascript:;" class="btn green btn-sm default">
                                        <i class="fa fa-check"></i> 
                                        <?php echo $this->db->query("SELECT department_name FROM department WHERE department_id = ?", array($state_department_id))->row()->department_name; ?>
                                    </a>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <a href="javascript:;" class="btn yellow btn-sm default"><i class="fa fa-lock"></i> Review by OPERATIONAL MANAGER</a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if (empty($operational_manager_approval)): ?>
                            <a href="javascript:;" class="btn btn-success btn-sm" data-name="approve-request" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-check"></i> Approve requisition 
                            </a>
                            <a href="javascript:;" class="btn btn-danger btn-sm" data-name="unlock-request" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-unlock"></i> Unlock 
                            </a>
                            <?php //else: ?>
        <!--                            <a href="javascript:;" class="btn btn-warning btn-sm" data-name="unlock-request" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-lock"></i> Completion Quotation by Admin 
                            </a>-->
                        <?php endif; ?>
                    <?php endif; ?>
                                
                    <!--Show view pdf "Material Requisition Form"-->
                    <?php if (preg_match("/(5,|5$)/", $request_state)): ?>
                        <a href="<?php echo $class_url; ?>pdf/material_request/<?php echo $form_request->request_number; ?>" class="btn btn-info btn-sm" target="_blank">View Requisition Form</a>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (preg_match("/^request_form$/i", $class_name)): ?>
                <?php if ($form_request->proceed == 'NO'): ?>
                    <?php $this->view->load('pages/request_form/detail/requisition_form_detail_box_table'); ?>
                <?php else: ?>
                    <?php $this->view->load('pages/request_form/detail/requisition_form_detail_box_table_approved_by_department'); ?>
                <?php endif; ?>
            <?php else: ?>
                <?php $this->view->load('pages/request_form/detail/requisition_form_detail_box_table_approved_by_department'); ?>
            <?php endif; ?>
        </div>
    </div>
</div>