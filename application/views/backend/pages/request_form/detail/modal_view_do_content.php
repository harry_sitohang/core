<table class="table table-condensed table-striped">
    <tr>
        <td>Received Date </td>
        <td><?php echo format_date($do->received_date); ?></td>
    </tr>
    <tr>
        <td>Received By </td>
        <td><?php echo $do->user_fullname; ?></td>
    </tr>
    <tr>
        <td>DO Number </td>
        <td><?php echo $do->do_number; ?></td>
    </tr>
    <tr>
        <td>DO File </td>
        <td><a href="<?php echo "{$class_url}view_delivery_order/{$do->do_file}"; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> <?php echo $do->do_file; ?></a></td>
    </tr>
    <tr>
        <td>Review </td>
        <td><?php echo $do->review_comment; ?></td>
    </tr>
    <tr>
        <td>Score </td>
        <td><?php echo ($do->delivery_score * 100) . ' %'; ?></td>
    </tr>
</table>