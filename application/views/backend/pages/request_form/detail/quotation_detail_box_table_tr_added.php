<td><?php echo $group_number; ?>.</td>
<td data-for="list-supplier">
    <ul class="list-unstyled">
        <?php foreach ($quotations as $quotation_number => $quotation): ?>
            <li><?php echo $quotation['supplier_name']; ?> <a href="<?php echo "{$class_url}view_quotation/{$quotation['quotation_file']}"; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span></a></li>
        <?php endforeach; ?>
    </ul>
</td>
<td data-for="items">
    <?php foreach ($quotations as $quotation_number => $quotation): ?>
        <?php echo $quotation['supplier_name']; ?> <a href="<?php echo "{$class_url}view_quotation/{$quotation['quotation_file']}"; ?>" target="_blank"><span class="glyphicon glyphicon-paperclip"></span></a>
        <ul class="list-unstyled">
            <?php foreach ($quotation['items'] as $item): ?>
                <li><input type="checkbox" value="<?php echo $item['item_id']; ?>" name="set-items" data-item-type="<?php echo $item['item_type']; ?>" data-item-description="<?php echo $item['item_description']; ?>" checked="CHECKED" disabled="DISABLED"> <?php echo $item['item_description']; ?> : <?php echo $item['item_type']; ?> : <?php echo format_number($item['price']); ?> IDR</li>
            <?php endforeach; ?>
        </ul>
    <?php endforeach; ?>
</td>
<?php if (!isset($approved_by_admin)): ?>
    <td>
        <a href="javascript:;" data-loading-text="Loading..." name="remove-quotation" class="btn btn-danger btn-sm" data-status="added" data-group-number="<?php echo $group_number; ?>" data-request-number="<?php echo $form_request->request_number; ?>"><i class="fa fa-trash"></i>&nbsp;&nbsp;remove&nbsp;</a>
    </td>
<?php endif; ?>
<?php //echo '<pre>'; print_r($quotations);  ?>