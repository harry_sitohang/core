<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet grey box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Quotation List 
                    <span class="badge badge-success" data-name="count-quots">
                        <?php echo count($quots); ?>
                    </span>
                </div>
                <div class="actions actions-requisition">
                    <!--view capex button-->
                    <a href="javascript:;" class="btn btn-info btn-sm" data-name="view-capex" data-request-number="<?php echo $form_request->request_number; ?>"><?php echo !preg_match('/(2|2$)/i', $request_state) ? 'Set Capex' : 'View Capex'; ?></a>
                    <?php if ($session_user->department_id == '2'): ?>
                        <?php if (!preg_match('/(2|2$)/i', $request_state)): ?><!--If not self-approved-->
                            <a href="javascript:;" class="btn btn-success btn-sm" data-name="approve-quotation" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-send"></i> Proceed/Send Quotation 
                            </a>
                            <a href="javascript:;" class="btn btn-danger btn-sm" data-name="unlock-quotation" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-unlock"></i> Unlock 
                            </a>
                        <?php else: ?>
<!--                            <a href="javascript:;" class="btn btn-warning btn-sm" data-name="unlock-po" data-loading-text="loading..." data-request-number="<?php echo $form_request->request_number; ?>">
                                <i class="fa fa-lock"></i> Review Quotation by Director 
                            </a>-->
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="portlet-body clearfix" id="box-table-quotation">
                <?php if (preg_match("/(5,|5$)/i", $request_state) && !preg_match("/(2,|2$)/i", $request_state) && $session_user->department_id == '2'): ?><!--Approved by OPERATIONAL MANAGER, LOGIN AS ADMIN, ADMIN NOT APPROVED BY ADMIN YET-->
                    <?php $this->view->load('pages/request_form/detail/quotation_detail_box_table'); ?>
                    <?php $this->view->load('pages/request_form/detail/modal_add_group_quotation'); ?>
                <?php else: ?>
                    <?php $this->view->load('pages/request_form/detail/quotation_detail_box_table_approved'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php $this->view->load('pages/request_form/detail/modal_list_supplier'); ?>
<?php $this->view->load('pages/request_form/detail/modal_capex'); ?>