<div style="text-align: center;"><h4>CITRAMAS GROUP<br/>CAPITAL EXPENDITURE APPROVAL FORM</h4></div>
<table style="border: solid #000 1px;" border="1">
    <tr>
        <th style="text-align: center;">COMPANY<br/>PT.SEZ</th>
        <th style="text-align: center;">AMOUNT<br/><?php echo $item->item_price ? 'IDR ' . format_number($item->item_price * $item->item_qty) : '<span style="color:red;">Price has not been set</span>'; ?></th>
        <th style="text-align: center;">DEPARMENT<br/><?php echo $form_request->department_name; ?></th>
        <th style="text-align: center;">DATE REQUIRED<br/><?php echo format_date($form_request->request_date, 'd/m/Y'); ?></th>
        <th style="text-align: center;">REF. NO</th>
    </tr>
    <tr>
        <td colspan="2" rowspan="2">DETAIL OF EXPENDITURE <p><?php echo $item->item_type == 'MATERIAL' ? "{$item->item_qty}&nbsp;Unit&nbsp;" : ''; ?><?php echo $item->item_description; ?></p></td>
        <td colspan="3">RECOMMENDATIONS <p>&nbsp;</p></td>
    </tr>
    <tr>
        <td>RECOMMENDED BY<br/>&nbsp;</td>
        <td>SIGNATURE<br/>&nbsp;</td>
        <td>DATE<br/>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" rowspan="2">REASON/PURPOSE <p>&nbsp;</p></td>
        <td colspan="3">EVALUATION <p>&nbsp;</p></td>
    </tr>
    <tr>
        <td>EVALUATED BY<br/>&nbsp;</td>
        <td>SIGNATURE<br/>&nbsp;</td>
        <td>DATE<br/>&nbsp;</td>
    </tr>
    <tr>
        <td>QUOTATION</td>
        <td>AMOUNT</td>
        <td colspan="3">APPROVED SUPPLIER</td>
    </tr>
    <?php foreach ($quots as $key => $quot): ?>
        <tr>
            <td>SUPPLIER <?php echo $quot->supplier_name; ?></td>
            <td><?php echo format_number($quot->price * $quot->item_qty); ?></td>
            <?php if ($key + 1 == 1): ?><!--1 row quotation-->
                <td colspan="3">NAME : <?php echo !empty($choosed_quot) ? $choosed_quot->supplier_name : '<span style="color:red;">Quotation has not been set</span>'; ?> </td>
            <?php elseif ($key + 1 == 2): ?><!-- 2nd and next row quotation-->
                <td colspan="3">AMOUNT : <?php echo !empty($choosed_quot) ? 'IDR ' . format_number($choosed_quot->item_qty * $choosed_quot->price) : '<span style="color:red;">Quotation has not been set</span>'; ?> </td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
    <?php if (count($quots) == 1): ?>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3">AMOUNT : <?php echo!empty($choosed_quot) ? 'IDR ' . format_number($choosed_quot->item_qty * $choosed_quot->price) : '<span style="color:red;">Quotation has not been set</span>'; ?></td>
        </tr>
    <?php endif; ?>
    <tr>
        <td></td>
        <td></td>
        <td rowspan="2" colspan="3">REMARKS : <p>&nbsp;</p></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>PROPOSED BY <p><?php echo $form_request->user_fullname; ?></p><p>DATE : <?php echo format_date($form_request->request_date, 'd/m/Y'); ?></p></td>
        <td colspan="2">Admin Asst. 
            <?php if (empty($approval_admin)): ?>
                <p><span style="color:red;">Not approved yet</span></p>
            <?php else: ?>
                <p>DATE : <?php echo format_date($approval_admin->approved_date, 'd/m/Y'); ?></p>
            <?php endif; ?>
        </td>
        <td colspan="2">DIRECTOR APPROVAL 
            <?php if (empty($approval_director)): ?>
                <p><span style="color:red;">Not yet approved</span></p>
            <?php else: ?>
                <p>DATE : <?php echo format_date($approval_director->approved_date, 'd/m/Y'); ?></p>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3">REMARKS : <p>&nbsp;</p></td>
        <?php if (empty($choosed_quot)): ?>
            <td colspan="2"><span style="color:red;">PO has not been set</span></td>
        <?php else: ?>
            <td>PO NO. <strong><?php echo generate_delivery_order_number($req_order->request_order_number, $req_order->created_date); ?></strong></td>
            <td>PO DATE. <?php echo !empty($approval_director) ? format_date($approval_director->approved_date, 'd/m/Y') : '<span style="color:red;">Not yet approved</span>'; ?></td>
        <?php endif; ?>
    </tr>
</table>