<div style="text-align: center;"><h1>MATERIAL REQUISITION FORM </h1></div>
<div><h4>DATE : <?php echo format_date($form_request->request_date, 'd/m/Y'); ?></h4></div>
<table style="border: solid #000 1px;" border="1" width="100%">
    <tr>
        <th style="text-align: center;"><strong>NO</strong></th>
        <th style="text-align: center;"><strong>DESCRIPTION</strong></th>
        <th style="text-align: center;"><strong>UNIT</strong></th>
        <th style="text-align: center;"><strong>PRICE</strong></th>
        <th style="text-align: center;"><strong>TOTAL</strong></th>
    </tr>
    <?php $sub_total = 0; ?>
    <?php foreach ($result as $key => $row): ?>
        <tr>
            <td><?php echo $key + 1; ?></td>
            <td><?php echo $row->item_description; ?></td>
            <td><?php echo $row->item_qty; ?></td>
            <td><?php echo $row->item_price ? 'IDR ' . format_number($row->item_price) : '<span style="color:red;">Price has not been set</span>'; ?></td>
            <?php $total = $row->item_qty * $row->item_price; ?>
            <?php $sub_total += $total; ?>
            <td><?php echo $row->item_price ? 'IDR ' . format_number($total) : '<span style="color:red;">Price has not been set</span>'; ?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="3"></td>
        <td><strong>SUB TOTAL</strong></td>
        <td><?php echo $sub_total ? format_number($sub_total) : '<span style="color:red;">Price has not been set</span>'; ?></td>
    </tr>
</table>
<div>&nbsp;</div>
<table>
    <tr>
        <td><strong>Requested by,</strong></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><strong>Approved by,</strong></td>
    </tr>
    <tr>
        <td><p>xxx</p><hr></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><p>xxx</p><hr></td>
    </tr>
    <tr>
        <td>
            <p><strong>Name : </strong><?php echo $form_request->user_fullname; ?></p><p><strong>Department : </strong><?php echo $form_request->department_name; ?></p>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>
            <?php if(!empty($approval)): ?>
            <p><strong>Name : </strong><?php echo $approval->user_fullname; ?></p><p><strong>Department : </strong><?php echo $approval->department_name; ?></p>
            <?php else: ?>
                Not Approved Yet
            <?php endif;?>
        </td>
    </tr>
</table>