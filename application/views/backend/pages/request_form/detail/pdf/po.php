<?php $current = current($result); ?>
<div style="text-align: center;"><h4>PURCHASE ORDER</h4></div>
<table border="0">
    <tr>
        <td>To : <?php echo $current->supplier_name; ?><br/><?php echo $current->supplier_address; ?></td>
        <td colspan="1">&nbsp;</td>
        <td>Ref No. <?php echo generate_delivery_order_number($current->request_order_number, $current->created_date); ?><p>Date : <?php echo format_date($current->created_date); ?></p></td>
    </tr>
</table>
<p>&nbsp;</p>
<table style="border: solid #000 1px;" border="1">
    <tr>
        <td>No</td>
        <td>Description</td>
        <td>Unit</td>
        <td>Unit Price</td>
        <td>Amount</td>
    </tr>
    <?php $total = 0; ?>
    <?php foreach ($result as $key => $row): ?>
        <tr>
            <td><?php echo $key + 1; ?></td>
            <td><?php echo $row->item_description; ?></td>
            <td><?php echo $row->item_qty; ?></td>
            <td>Rp <?php echo format_number($row->item_price); ?></td>
            <?php $amount = $row->item_qty * $row->item_price; ?>
            <?php $total += $amount; ?>
            <td>Rp <?php echo format_number($amount); ?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="3"></td>
        <td><p><strong>Total</strong></p></td>
        <td><p>Rp <?php echo format_number($total); ?></p></td>
    </tr>
</table>
<p>&nbsp;</p>
<div style="text-align: right"><p>xxx</p><p><?php echo $current->user_fullname; ?></p></div>