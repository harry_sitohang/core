<table class="table table-bordered table-striped table-condensed table-bordered">
    <thead>
        <tr>
            <td>NO.</td>
            <td>TYPE</td>
            <td>DESCRIPTION</td>
            <td>UNIT</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $key => $item): ?>
            <tr>
                <td><?php echo $key+1; ?>.</td>
                <td><?php echo $item->item_type; ?></td>
                <td><?php echo $item->item_description; ?></td>
                <td><?php echo ($item->item_type == 'MATERIAL') ? $item->item_qty : '-'; ?></td>
            </tr>  
        <?php endforeach; ?>
    </tbody>
</table>
