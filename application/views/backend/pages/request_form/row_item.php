<tr>
    <td data-for="item-counter"><?php echo $item_counter; ?></td>
    <td data-for="item_type">
        <div class="form-group" for="item_type">
            <?php echo $option_item_type; ?>
        </div>
    </td>
    <td data-for="item_description">
        <div class="form-group" for="item_description">
            <textarea name="item_description[]" class="form-control item_description"></textarea>
        </div>
    </td>
    <td data-for="item_qty">
        <div class="form-group" for="item_qty">
            <input type="text" class="form-control item_qty" name="item_qty[]">
        </div>
    </td>
    <td class="text-center" data-for="remove_item">
        <a href="#" data-name="remove-item" class="remove-item"><span class="glyphicon glyphicon-remove text-danger" data></span></a>
    </td>
</tr>