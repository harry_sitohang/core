<div class="modal fade" id="modal_suggest_after_add_request">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="text-center"><div class="alert alert-success">Successfully submit Requisition, <a href="javascript:;" class="btn btn-sm btn-primary" data-name="link-submitted-requisition">view <i class="fa fa-arrow-circle-right"></i></a> submitted requisition.</div></h5>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn grey" data-dismiss="modal" style="width: 100%;">Close, then Add New Requisition <i class="fa fa-plus-square-o"></i></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->