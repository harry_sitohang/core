<div class="modal fade" id="modal-proceed-with-items">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Proceed Your Requisition</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon glyphicon-pencil"></span> EDIT</button>
                <button type="button" class="btn btn-success">PROCEED <span class="glyphicon glyphicon-chevron-right"></span></button>
                <div class="clearfix"></div>
                <div class="bs-callout bs-callout-danger text-left">
                    <h4>By click the <strong class="text-success bg-success">PROCEED</strong> button.</h4>
                    <p>Your requisition will be <span class="text-warning"><span class="glyphicon glyphicon glyphicon-lock"></span> LOCKED</span> then SEND to <span class="text-info"><span class="glyphicon glyphicon glyphicon-user"></span> OPERATIONAL MANAGER</span>.<br/>Please make sure your requisition item's is correct.<br/>Because when your requistion is LOCKED, you can't <strong class="text-primary bg-primary">EDIT</strong> your requisition item's anymore.</p>
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->