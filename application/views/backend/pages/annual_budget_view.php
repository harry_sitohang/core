<!-- CONTENT BODY GOES HERE -->
<form action="#" name="budget-form" id="budget-form" class="form-horizontal" role="form">
    <div class="form-group" for="department_id">
        <label for="department_id" class="col-sm-2 control-label">Department</label>
        <div class="col-sm-10 controls">
            <select class="form-control" name="department_id" id="department_id">
                <option value="0">Please Select</option>
                <?php foreach ($departments as $department): ?>
                    <option value="<?php echo $department->department_id; ?>"><?php echo $department->department_name; ?></option>
                <?php endforeach; ?>
            </select>          
        </div>
    </div>
    <div class="form-group" for="year">
        <label for="year" class="col-sm-2 control-label">Year</label>
        <div class="col-sm-10 controls">
            <select class="form-control" name="year" id="year">
                <option value="0">Please Select</option>
                <?php for ($i = $min_year; $i <= date('Y'); $i++): ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>          
        </div>
    </div>
    <div class="form-group" for="amount">
        <label for="amount" class="col-sm-2 control-label">Amount</label>
        <div class="col-sm-10 controls">
            <input type="text" name="amount" id="amount" value="" placeholder="Budget Amount" class="form-control">
        </div>
    </div>
    <div class="form-actions fluid">
        <div class="col-sm-offset-2 col-sm-10">
            <button name="submit" type="submit" data-loading-text="Loading..." class="btn green" data-action="add">Submit</button>
        </div>
    </div>
</form>
<!-- END CONTENT BODY GOES HERE -->