<div class="tabbable">
    <ul class="nav nav-tabs nav-tabs-lg">
        <li class="active">
            <a href="#tab_requisition" data-toggle="tab">
                Requisition Form
                <span class="badge badge-success" data-name="count-items">
                    <?php echo count($items); ?>
                </span>
            </a>
        </li>
        <li>
            <a href="#tab_quot" data-toggle="tab">
                Quotations
                <span class="badge badge-success" data-name="count-quots">
                    <?php echo count($quots); ?>
                </span>
            </a>
        </li>
        <li>
            <a href="#tab_po" data-toggle="tab">
                Purchase Orders
                <span class="badge badge-success" data-name="count-po">
                    <?php echo count($purchase_orders); ?>
                </span>
            </a>
        </li>
        <li>
            <a href="#tab_do" data-toggle="tab">
                Delivery Orders
                <span class="badge badge-success" data-name="count-do">
                    0<?php //echo count($delivery_orders); ?>
                </span>
            </a>
        </li>
<!--        <li>
            <a href="#tab_invoice" data-toggle="tab">
                Invoices
                <span class="badge badge-default">
                    0<?php //echo count($invoices); ?>
                </span>
            </a>
        </li>-->
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_requisition">
            <?php $this->view->load('pages/request_form/detail/requisition_form_detail'); ?>
        </div>
        <div class="tab-pane" id="tab_quot">
            <?php $this->view->load('pages/request_form/detail/quotation_detail'); ?>
        </div>
        <div class="tab-pane" id="tab_po">
            <?php $this->view->load('pages/request_form/detail/po_detail'); ?>
        </div>
        <div class="tab-pane" id="tab_do">
            <?php $this->view->load('pages/request_form/detail/do_detail'); ?>
        </div>
    </div>
</div>