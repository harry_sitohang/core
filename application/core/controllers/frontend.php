<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/core.php';

/**
 * @author : Harry Osmar Sitohang
 * @date : 30 Jun 2014
 * @desc : This class used as core, all frontend controller should be extend this class
 */
class Frontend extends Core {

    protected $template_path = 'frontend';

    public function __construct() {
        parent::__construct();
    }

}

/* End of file frontend.php */
/* Location: ./application/core/controllers/frontend.php */