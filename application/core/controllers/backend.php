<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'core/controllers/core.php';

/**
 * @author : Harry Osmar Sitohang
 * @date : 16 Sept 2014
 * @desc : Backend themes, , all backend controller should be extend this class
 */
class Backend extends Core {

    protected $template_path = 'backend';
    protected $session_user;
    protected $privilege_action;

    public function __construct() {
        parent::__construct();
    }

    protected function init() {
        parent::init();
        $this->check_authenticate();  //check_authenticate(); must be placed on top of all called function
        $this->load->model("Privilege_model");
        $this->load->model("Menu_model");
        $this->set_sidebar_menu();
        $this->set_breadcrumb();
        $this->check_privilege(); //check privilege for current menu > action
        $this->set_backend_asset();
    }

    /**
     * @since 26 Oct 2014 
     * @todo check login status
     */
    protected function check_authenticate() {
        if (!$this->session->userdata('user')) {
            if ($this->input->is_ajax_request()) {//if ajax request => session not exist, reload current page
                echo json_encode(array('error', 'action' => 'reload'));
                die;
            } else {//if not ajax request => redirect page to login
                $urlredirect = empty($_SERVER['QUERY_STRING']) ? current_url() : sprintf("%s?%s", current_url(), $_SERVER['QUERY_STRING']);
                redirect(sprintf("%s?urlredirect=%s", base_url(), $urlredirect));
            }
        } else {
            $this->session_user = $this->session->userdata('user');
            $this->view->set('session_user', $this->session_user);
        }
    }

    /**
     * @since 27 Oct 2014 
     * @todo check privilege user asccociated to menu access
     */
    protected function check_privilege() {
        //set variable $action & $menu_segment
        $action = $this->uri->segment(3) ? $this->uri->segment(3) : 'view'; //if action not exist set default to view
        $menu = $this->Menu_model->get_by_field('menu_segment', $this->class_name)->row();

        //set $this->privilege_action
        if (!empty($menu)) {
            $privilege = $this->Privilege_model->get_by_field(array('menu_id' => $menu->menu_id, 'user_group_id' => $this->session_user->user_group_id))->row();
            $this->privilege_action = $privilege->privilege_action;
            $this->view->set('privilege_action', $this->privilege_action); //'action list' for this current menu ex: view,edit,add,remove

            if (!preg_match("/({$action},|{$action}$)/i", $menu->menu_action)) {//for all action that's not defined in menu_action set $action to 'view'
                $action = 'view';
            }
        } else {
            $this->view->set('privilege_action', $this->privilege_action); //leave 'action list' blank
        }

        //check privilege access
        if (!empty($menu) && !$this->Privilege_model->check_user_privilege($this->class_name, $action)) { //FORBIDDEN ACCESS FOR THIS MENU + ACTION
            if ($this->input->is_ajax_request()) {//if ajax request => session not exist, reload current page
                echo json_encode(array('error', 'action' => 'forbidden_access'));
                die;
            } else {//if not ajax request, set view to error page
                $this->view->set(array('err_title' => 'Access forbidden', 'err_msg' => 'Sorry, but you have no access for this page.'));
                $this->view->content("error");
            }
        } else if (empty($menu)) { //MENU DOESNT EXIST IN DATABASE
            $this->view->set('privilege_action', $action);
        }
    }

    /**
     * @author Harry <if07087@gmail.com>
     * @since 9 Oct 2014 
     * @todo set index backend
     */
    public function index() {
        parent::index();
    }

    /**
     * @since 26 Oct 2014 
     * @todo logout url
     */
    public function logout() {
        $this->session->unset_userdata(array('user' => '', 'sidebar_menu' => ''));
        redirect(sprintf("%s", base_url()));
    }

    /**
     * @author Harry <if07087@gmail.com>
     * @since 9 Oct 2014 
     * @todo set assets backend
     */
    protected function set_backend_asset() {
        $this->page_js[] = "{$this->_assets_js}backend.js";
        $this->page_css[] = "{$this->_assets_css}backend.css";
        $this->view->set("pages_css", $this->page_css);
        $this->view->set("pages_js", $this->page_js);
    }

    /**
     * @author Harry <if07087@gmail.com>
     * @since 9 Oct 2014 
     * @todo generate sidebar menu for backend template, then set value to view
     */
    protected function set_sidebar_menu() {
        if (!$this->session->userdata('sidebar_menu')) { //set side bar menu html to session, to reduce database load
            $this->session->set_userdata('sidebar_menu', $this->Menu_model->generate_sidebar_menu());
        }
        //$this->view->set("sidebar_menu", $this->Menu_model->generate_sidebar_menu());
        $this->view->set("sidebar_menu", $this->session->userdata('sidebar_menu'));
    }

    /**
     * @author Harry <if07087@gmail.com>
     * @since 9 Oct 2014 
     * @todo set breadcrumb for current pages
     */
    protected function set_breadcrumb() {
        $this->view->set("breadcrumb", $this->Menu_model->get_breadcrumb_data());
        $this->view->set('breadcrumb_parent', $this->Menu_model->get_breadcrumb_parent($this->uri->segment(2)));
    }

}

/* End of file backend.php */
/* Location: ./application/core/controllers/backend.php */